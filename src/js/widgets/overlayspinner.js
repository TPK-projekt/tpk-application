/* eslint-env browser, amd, es6 */
define(["jquery"], (jQuery) => {
    "use strict";

    function OverlaySpinner (options) {
        this.element = options.element;

        this.overlay = jQuery('<div><span class="fa fa-refresh fa-spin fa-2x" /></div>');

        this.active = false;
    }

    OverlaySpinner.prototype.show = function show () {
        if (!this.active) {
            this.overlay.css({
                "position": "absolute",
                "width": String(this.element.width()) + "px",
                "line-height": String(this.element.height()) + "px",
                "top": this.element.position().top,
                "left": this.element.position().left,
                "opacity": "0.5",
                "background-color": "#fff",
                "vertical-align": "middle",
                "text-align": "center"
            });

            this.element.offsetParent().append(this.overlay);
            this.active = true;
        }
    };

    OverlaySpinner.prototype.hide = function hide () {
        if (this.active) {
            this.overlay.detach();
            this.active = false;
        }
    };

    return OverlaySpinner;

});

