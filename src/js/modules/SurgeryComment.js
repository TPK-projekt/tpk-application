/* eslint-env browser, amd, es6 */
define([
    "jquery",
    "AjaxEdit"
], (jQuery, AjaxEdit) => {
    "use strict";

    const EDIT_TOP_LEVEL_PRICE_COMMENT =
          "$services.localization.render('tvpris.edit_top_level_price_comment')";
    const EDIT_AREA_PRICE_COMMENT =
          "$services.localization.render('tvpris.edit_area_price_comment')";

    const EDIT_MEASURE_PRICE_COMMENT =
          "$services.localization.render('tvpris.edit_measure_price_comment')";
    const EDIT_PACKAGE_PRICE_COMMENT =
          "$services.localization.render('tvpris.edit_package_price_comment')";

    const listeners = {};

    function SurgeryComment (element, sid) {
        this.element = jQuery(element);
        this.code = this.element.attr("data-code");
        this.codeText = this.element.
            attr("data-code-text");
        this.isMeasure = this.element.
            attr("data-is-measure") === "true";
        this.level = this.element.attr("data-level");
        this.sid = sid;
        this.parent = this.element.attr("data-parent");
        this.version = this.element.attr("data-version");
        this["public"] = this.element.
            attr("data-public") === "true";

        this.commentElement = this.element.
            find(".tpk-price-comment-text");
        this.checkbox = this.element.
            find(".tpk-comment-level-checkbox");
        this.checkbox = this.checkbox.size() > 0
            ? this.checkbox.get(0)
            : null;
        this.editLink = this.element.find(".edit-comment-link, .tpk-price-comment-text");
        this.url = new XWiki.Document(XWiki.Model.resolve(
            "TVPris.Services.SurgeryComment",
            XWiki.EntityType.DOCUMENT,
            XWiki.Document.currentDocumentReference
        )).getURL();

        let dt = null;

        const TOP_LEVEL = 0;
        const AREA_LEVEL = 1;

        if (Number(this.level) === TOP_LEVEL) {
            dt = EDIT_TOP_LEVEL_PRICE_COMMENT;
        } else if (Number(this.level) === AREA_LEVEL) {
            dt = EDIT_AREA_PRICE_COMMENT + " " +
                this.codeText + ".";
        } else if (this.isMeasure) {
            dt = EDIT_MEASURE_PRICE_COMMENT + " " +
                this.codeText + ".";
        } else {
            dt = EDIT_PACKAGE_PRICE_COMMENT + " " +
                this.codeText + ".";
        }

        this.data = {
            "comment_text": this.commentElement.
                attr("data-comment-text"),
            "include_additional":
            this.checkbox !== null &&
                this.checkbox.checked
        };

        if (typeof this.parent !== "undefined") {
            const i = this.instanceId(
                Number(this.level) - 1,
                this.parent
            );
            if (typeof listeners[i] === "undefined") {
                listeners[i] = [];
            }
            listeners[i].push(this);
        }

        this.ajaxEdit = new AjaxEdit({
            "url": this.url,
            "element": this.element,
            "descriptionText": dt,
            "comment_text": this.data.comment_text,
            "startOpened": false,
            "overlayElement": this.commentElement.parent()
        });
        jQuery(this.ajaxEdit).
            on(
                "content-changed",
                (event, content, data) => {
                    console.
                        log("content-changed " +
                            JSON.stringify(data));
                    this.data = data;
                    this.refresh();
                    this.notifyListeners(data.rendered_text, data);
                }
            );

        this.editLink.click((event) => {
            event.stopPropagation();
            event.preventDefault();
            this.ajaxEdit.open();
        });

        const commonData = () => ({
            "xpage": "plain",
            "outputSyntax": "plain",
            "sid": this.sid,
            "l": this.level,
            "v": this.version,
            "iid": this.code,
            "ism": this.isMeasure ? 1 : 0,
            "public": this["public"]
        });

        if (this.checkbox !== null) {
            this.checkbox.onclick = () => {
                const data = commonData();
                data.ia = this.checkbox.checked;
                data.form_token = jQuery("html").attr("data-xwiki-form-token");
                const note = new XWiki.widgets.Notification(
                    "$services.localization.render('tvpris.save_include_flag')",
                    "inprogress"
                );

                jQuery.ajax({
                    "url": this.url,
                    "method": "POST",
                    "datatype": "json",
                    "data": data
                }).always(() => note.hide()).fail(() => {
                    new XWiki.widgets.Notification(
                        "$services.localization.render('tvpris.save_include_flag_failed')",
                        "error"
                    );
                }).done((data_) => {
                    this.data = data_;
                    this.notifyListeners(data_.rendered_text, data_);
                    this.refresh();
                });
            };
        }

        this.activate = () => {
            const d = jQuery.ajax({
                "url": this.url,
                "data": {
                    "xpage": "plain",
                    "outputSyntax": "plain",
                    "iid": this.code,
                    "ism": this.isMeasure ? 1 : 0,
                    "l": this.level,
                    "sid": this.sid,
                    "v": this.version
                }
            });

            d.done((data) => {
                this.data = data;
                this.refresh();
                this.notifyListeners(data.rendered_text, data);
            });

            return d.promise();
        };

        this.refresh = () => {
            this.commentElement.html(this.data.rendered_text);
            if (this.checkbox !== null) {
                this.checkbox.checked = this.data.include_additional;
            }
        };
    }

    SurgeryComment.prototype.instanceId = function instanceId (l, iid) {
        if (Number(l) === 0) {
            return "0";
        }
        return String(l) + "-" + iid + "-" + this.isMeasure;
    };

    SurgeryComment.prototype.notifyListeners =
        function notifyListeners (renderedText, data) {
            const ls = listeners[
                this.instanceId(
                    this.level,
                    this.code
                )
            ];

            if (typeof ls !== "undefined") {
                for (let i = 0; i < ls.length; i += 1) {
                    ls[i].dependencyUpdate(renderedText, data);
                }
            }
        };

    SurgeryComment.prototype.dependencyUpdate =
        function dependencyUpdate (renderedText, data) {
            if (this.data.include_additional) {
                let isDep = false;
                if (typeof data.deps === "object") {
                    for (let i = 0; i < data.deps.length; i += 1) {
                        const d = data.deps[i];
                        if (Number(d.level) === Number(this.level) &&
                            Boolean(d.isMeasure) === this.isMeasure &&
                            d.itemId === this.code) {
                            this.data.rendered_text = d.rendered_text;
                            isDep = true;
                            break;
                        }
                    }
                }
                if (!isDep) {
                    this.data.rendered_text = renderedText;
                }
                this.refresh();
                this.notifyListeners(this.data.rendered_text, data);
            }
        };

    return SurgeryComment;
});
