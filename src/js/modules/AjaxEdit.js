/* eslint-env browser, amd, es6 */
define([
    "jquery",
    "OverlaySpinner",
    "$services.webjars.url('jquery-ui', 'jquery-ui.js')".toString()
], (jQuery, OverlaySpinner) => {
    "use strict";

    const defaultOptions = {
        "containerSelector": "#ajax-edit-container",
        "title": "$services.localization.render('tvpris.edit_property')",
        "rows": 8,
        "cols": 80,
        "debug": false,
        "attributeName": "data-ajaxedit",
        "element": null,
        "startOpened": true,
        "overlayElement": null
    };

    function AjaxEdit (options) {

        this.options = jQuery.extend({}, defaultOptions, options);

        const elem = jQuery(this.options.element);

        this.data = {
            "url": options.url,
            "parameters": {
                "iid": elem.attr("data-code"),
                "ism": elem.attr("data-is-measure") === "true" ? 1 : 0,
                "l": elem.attr("data-level"),
                "sid": elem.attr("data-sid"),
                "v": elem.attr("data-version"),
                "public": elem.attr("data-public") === "true" ? 1 : 0,
                "outputSyntax": "plain",
                "xpage": "plain"
            }
        };


        if (typeof options.comment_text !== "undefined") {
            this.comment_text = options.comment_text;
            if (this.startOpened) {
                this.openEditor();
            }
        } else {
            this.getProperty(this.startOpened);
        }
    }

    AjaxEdit.prototype.saveProperty = function saveProperty (event) {

        event.preventDefault();
        event.stopPropagation();
        jQuery(document).trigger("xwiki:actions:beforeSave");
        this.input = this.container.find('[name="ajaxedit-input"]');

        const content = this.input.val();
        const notification = new XWiki.widgets.Notification(
            "$services.localization.render('tvpris.saving_property')",
            "inprogress"
        );

        this.buttonContainer.find("button").attr("disabled", "disabled");

        jQuery.ajax({
            "url": this.data.url,
            "method": "POST",
            "datatype": "json",
            "data": jQuery.extend({}, this.data.parameters, {
                "text": content,
                "form_token": jQuery("html").attr("data-xwiki-form-token")
            })
        }).always(() => notification.hide()).done((data) => {
            this.container.dialog("close");
            jQuery(document).trigger("xwiki:actions:afterSave");
            this.ckeditor.instances["ajaxedit-input"].resetDirty();
            this.comment_text = content;
            jQuery(this).trigger("content-saved", [content, data]);
            jQuery(this).trigger("content-changed", [content, data]);
            this.buttonContainer.find("button").attr("disabled", null);
        }).fail((jqXHR, textStatus, errorThrown) => {
            if (typeof jqXHR.responseJSON !== "undefined" &&
                typeof jqXHR.responseJSON.error !== "undefined") {
                new XWiki.widgets.Notification(jqXHR.responseJSON.error, "error");
            } else {
                new XWiki.widgets.Notification("$services.localization.render('tvpris.failed_saving_property'): " +
                                               textStatus + " " + errorThrown, "error");
            }
        });
    };

    AjaxEdit.prototype.openEditor = function openEditor () {
        this.text = jQuery('<div class="ajaxedit-text"/>');
        this.text.text(this.options.descriptionText);
        this.form = jQuery('<form action="#" />');

        this.input = jQuery('<textarea name="ajaxedit-input" data-ckeditor-config="ajaxedit" />');

        this.input.val(this.comment_text);
        this.input.attr({
            "rows": this.options.rows,
            "cols": this.options.cols
        });

        this.form.append(this.input);
        this.buttonContainer = jQuery('<div class="ajax-edit-buttons" />');

        this.saveBtn = jQuery('<span class="button-wrapper"><button class="btn btn-primary" name="ajax-edit-save" >$services.localization.render("save")</button></span>"');

        this.buttonContainer.append(this.saveBtn);

        this.cancelBtn = jQuery('<span class="button-wrapper"><button class="btn btn-default" name="ajax-edit-cancel" >$services.localization.render("cancel")</button></span>"');
        this.buttonContainer.append(this.cancelBtn);
        this.form.append(this.buttonContainer);
        this.form.append(jQuery('<div class="clearfloats"/>'));

        const container = jQuery(this.options.containerSelector);
        this.container = container;
        this.container.append(this.text);
        this.container.append(this.form);

        container.dialog({
            "position": {
                "my": "right top",
                "at": "right top",
                "of": this.options.element.get(0)
            },
            "width": "704",
            "height": "442",
            "title": this.options.title,
            "modal": true,
            "autoOpen": false,
            "close": () => {
                this.text.remove();
                this.text = null;
                this.form.remove();
                this.form = null;
            }
        });

        const r = (ref) => {
            if (typeof ref === "string") {
                return XWiki.Model.resolve(ref, XWiki.EntityType.DOCUMENT);
            }
            return ref;
        };

        this.options.docReference = r(this.options.docReference);
        this.options.classReference = r(this.options.classReference);

        this.saveBtn.click((event) => this.saveProperty(event));
        this.form.submit((event) => this.saveProperty(event));

        this.cancelBtn.click((event) => {
            event.stopPropagation();
            event.preventDefault();
            this.container.dialog("close");
        });

        require(["xwiki-ckeditor", "autosave"], (ckeditorPromise) => {
            ckeditorPromise.done((ckeditor) => {
                this.container.find("textarea").each((index, element) => {
                    ckeditor.replace(element);
                    this.ckeditor = ckeditor;
                });
            });
        });

        this.container.dialog("open");
    };


    AjaxEdit.prototype.open = function open () {
        this.openEditor();
    };

    AjaxEdit.prototype.debug = function debug (msg) {
        if (this.debug) {
            console.log(msg);
        }
    };

    AjaxEdit.prototype.progress = function progress (message) {
        if (this.options.overlayElement === null) {
            const notification = new XWiki.widgets.Notification(
                message,
                "inprogress"
            );
            return () => notification.hide();
        }
        const spinner =
              new OverlaySpinner({"element": this.options.overlayElement});
        spinner.show();
        return () => spinner.hide();
    };

    AjaxEdit.prototype.getProperty = function getProperty (open) {
        const stopProgress = this.progress("$services.localization.render('tvpris.fetching_property')");

        jQuery.ajax({
            "url": this.data.url,
            "method": "GET",
            "dataType": "json",
            "data": this.data.parameters
        }).always(stopProgress).done((data, _) => {
            this.comment_text = data.comment_text;
            jQuery(this).trigger("content-changed", [this.comment_text, data]);
            if (open) {
                this.openEditor();
            }
        }).fail((jqXHR, textStatus, errorThrown) => {
            new XWiki.widgets.Notification(
                "$services.localization.render('tvpris.failed_fetching_property'): " +
                    textStatus + " " + errorThrown,
                "error"
            );
        });

    };

    return AjaxEdit;
});
