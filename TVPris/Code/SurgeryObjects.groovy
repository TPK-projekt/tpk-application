{{include reference="TVPris.Code.RunQuery" /}}{{groovy output="none"}}
import org.xwiki.velocity.tools.JSONTool

class SurgeryObjects {

  private final List measureAreas
  private final List measureAreasSpecial
  private final Map packageMap
  private final Map measureMap
  private final Map measureMapSpecial
  private final Boolean withComments
  private final int sid

  private final def xwiki
  private final def xcontext
  private final def services

  private Map _allMeasureAreas
  private Map _allSpecialMeasureAreas
  private Map _allPackageAreas

  private final Map REPLACEMENTS =
      [
       921: true,
       922: true,
       925: true,
       926: true,
       928: true,
       929: true,
       940: true,
       941: true
      ]
  
  private String topLevelComment

  public final String TOPLEVEL_COMMENT = 'tpk_toplevel_comment'
  
  boolean addObject(map, key, object) {
    boolean ret = false
    def l = map.get(key)
    if (l == null) {
      l = new ArrayList()
      map.put(key, l)
      ret = true
    }
    l.add(object)
    return ret
  }

  public SurgeryObjects(xwiki, xcontext, services, Integer sid, Boolean error, Boolean withComments, Boolean includeMissingPrices) {
    this.xwiki = xwiki
    this.xcontext = xcontext
    this.services = services
    this.withComments = withComments
    this.sid = sid

    if (!error) {
      QueryRunner queryRunner = new QueryRunner('native', xwiki, xcontext)
      def q = """
WITH version (version) AS (SELECT tpkv_version  FROM tvpris_versionclass JOIN xwikiobjects USING (xwo_id) WHERE xwo_name = ?)
SELECT
"""
      queryRunner.addParameterSetter(services.tpkCompare.VERSION_DOCUMENT)
      def columns = """
tvpi_code, is_measure, tvpi_price, tvpi_price_special, tvpi_title, tvpi_description
, tvpp_text_code, tvpp_measures  
, tvpi_area, tvpm_area_special 
, tvpip_price 
"""
      if (withComments) {
	columns += ", tvppc_text, tvppc_include_area_comment"
      }
      q += columns
      if (withComments) {
	q += ", (tvppc_version < version) as is_old "
	columns += ", is_old"
      }
      columns += ", tvpip_type"
      q += " FROM version JOIN tvpris_item AS i ON tvpi_version = version"

      if (withComments || includeMissingPrices) {
	q += " LEFT OUTER "
      }
      q += """
JOIN

 tpk_itemprice ON tvpip_surgery_id = ? AND tvpip_version = version AND tvpip_item_id = tvpi_code AND is_measure = tvpip_is_measure AND (tvpip_type = 0 OR tvpip_type = 1)
LEFT OUTER JOIN tvpris_package AS p ON i.xwo_id = p.xwo_id 
LEFT OUTER JOIN tvpris_measure AS m ON i.xwo_id = m.xwo_id
"""
      queryRunner.addParameterSetter(sid)

      if (withComments) {
	q += " LEFT OUTER JOIN tvpris_publicpricecomment ON tvppc_surgery_id = ? AND tvppc_item_id = tvpi_code AND  (tvppc_is_measure != 0) = tvpip_is_measure AND (tvpip_type = 0 OR tvpip_type = 1)"
	queryRunner.addParameterSetter(sid)

      }
      q += " GROUP BY " + columns
      q += " ORDER BY "
      if (withComments) {
	q += " count(tvppc_text) DESC, "
      }
      q += " is_measure, tvpi_code"
      def result = queryRunner.runQuery(q, null, null)
      packageMap = new HashMap()
      measureMap = new HashMap()
      measureMapSpecial = new HashMap()
      measureAreas = new ArrayList()
      measureAreasSpecial = new ArrayList()
      for (row in result) {
	def tvpi_code = row[0]
	def is_measure = row[1]
	def tvpi_price = row[2]
	def tvpi_price_special = row[3]
	def tvpi_title = row[4]
	def tvpi_description = row[5]
	def tvpp_text_code = row[6]
	def tvpp_measures = row[7]
        def tvpi_area = row[8]
	def tvpm_area_special = row[9]
	def tvpip_price = row[10]
	def publicpricecomment
	def include_area_comment
	def comment_is_old
	if (withComments) {
	  publicpricecomment = row[11] == null ? '' : row[11]
	  include_area_comment = row[12] == null ? true : row[12]
	  comment_is_old = row[13] == null ? false : row[13]
	}
	def obj
	if (is_measure) {
	  def marea = tvpi_area
	  if (marea == '') {
	    marea = 'Åtgärder som ej faller under något behandlingsområde'
	  }
	  def marea_special = tvpm_area_special
	  if (marea_special == '') {
	    marea_special = 'Åtgärder som ej faller under något behandlingsområde'
	  }
	  obj = [
	    'code': tvpi_code,
		     'reference_price': tvpi_price,
		     'reference_price_special': tvpi_price_special,
		     'title': tvpi_title,
		     'description': tvpi_description,
		     'area': marea,
		     'area_special': tvpm_area_special,
		     'price': tvpip_price,
		     'is_replacement': REPLACEMENTS.get(tvpi_code) != null ? REPLACEMENTS.get(tvpi_code) : false
	  ]
	    
	  boolean addedArea = false
	  if (marea != null) {
	    addedArea = addObject(measureMap, marea, obj)
	    if (addedArea) {
	      measureAreas.add(marea)
	    }
	  }
	  if (marea_special != null) {
	    addedArea = addObject(measureMapSpecial, marea, obj)
	    if (addedArea) {
	      measureAreasSpecial.add(marea)
	    }
	  }
	} else {
	  obj = [
	   'reference_price': tvpi_price,
	   'reference_price_special': tvpi_price_special,
	   'title': tvpi_title,
	   'description': tvpi_description,
	   'area': tvpi_area,
           'code': tvpi_code,
	   'text_code': tvpp_text_code,
	   'price': tvpip_price,
	   'measures': tvpp_measures
	  ]
	  addObject(packageMap, tvpi_area, obj)
	}
	if (withComments) {
	  obj.put('publicpricecomment', publicpricecomment)
	  obj.put('include_area_comment', include_area_comment)
	  obj.put('comment_is_old', comment_is_old)
	}
      }
    }
  }

  private Map allAreas(boolean is_measure, boolean is_special) {
      def areaField = (is_special ? "tvpm_area_special" : "tvpi_area")
      QueryRunner queryRunner = new QueryRunner('native', xwiki, xcontext)
      def q = "WITH version (version) AS (SELECT tpkv_version  FROM tvpris_versionclass JOIN xwikiobjects USING (xwo_id) WHERE xwo_name = ?) SELECT " + areaField + " , min(tvpi_code) as min_code"
      queryRunner.addParameterSetter(services.tpkCompare.VERSION_DOCUMENT)
      if (withComments) {
	q += ", tpkac_text, tpkac_include_overall_comment"
      }
      q += " FROM version JOIN tvpris_item ON " + (is_measure ? '' : 'NOT ') + "is_measure AND tvpi_version = version JOIN " + (is_measure ? 'tvpris_measure' : 'tvpris_package') +  " USING (xwo_id)"
      if (withComments) {
	q += " LEFT OUTER JOIN tpk_areacommentclass ON tpkac_surgery_id = ? AND tpkac_area = " + areaField + " AND tpkac_version = version"
	queryRunner.addParameterSetter(sid)
      }
      q += " WHERE " + areaField + " IS NOT NULL GROUP BY " + areaField
      if (withComments) {
	q += ", tpkac_text, tpkac_include_overall_comment"
      }
      q += " ORDER BY min_code ASC"
      def result = queryRunner.runQuery(q, null, null)
      def all = new ArrayList()
      def ret = new HashMap()
      def comments = new HashMap()
      for (row in result) {
	all.add(row[0])
	if (withComments) {
	  comments.put(row[0], [
			 text: row[2] == null ? '' : row[2],
			 includeOverall: (row[3] == null ? true : row[3])
		       ])
	}
      }
      ret.put('areas', all)
      if (withComments) {
	ret.put('comments', comments)
      }
      return ret
  }

  public Map allMeasureAreas() {
    if (_allMeasureAreas == null) {
      _allMeasureAreas = allAreas(true, false)
    }
    return _allMeasureAreas
  }

  public Map allSpecialMeasureAreas() {
    if (_allSpecialMeasureAreas == null) {
      _allSpecialMeasureAreas = allAreas(true, true)
    }
    return _allSpecialMeasureAreas
  }

  public Map allPackageAreas() {
    if (_allPackageAreas == null) {
      _allPackageAreas = allAreas(false, false)
    }
    return _allPackageAreas
  }

  public List getMeasureAreas() {
    return measureAreas
  }

  public Map getMeasureMap() {
    return measureMap
  }

  public Map getPackageMap() {
    return packageMap
  }

  public String getTopLevelComment() {
    if (topLevelComment == null) {
      def q = "SELECT tpkac_text FROM tpk_areacommentclass WHERE tpkac_surgery_id = ? AND tpkac_area = '" + TOPLEVEL_COMMENT + "'"
      QueryRunner queryRunner = new QueryRunner('native', xwiki, xcontext)
      queryRunner.addParameterSetter(sid)
      def result = queryRunner.runQuery(q, null, null)
      if (result.size() == 0) {
	topLevelComment = ''
      } else {
	topLevelComment = result.get(0)
      }
    }
    return topLevelComment != null ? topLevelComment : ''
  }

  private String _getRenderedText(boolean isSpecial, boolean isMeasure, int level, String itemId, obj)
  {
    String prefix = ''
    String text
    Boolean nextLevel
    switch (level) {
    case 0:
       return getTopLevelComment()
    case 1:
       Map allAreas = isMeasure ? (isSpecial ? allSpecialMeasureAreas() : allMeasureAreas()) : allPackageAreas()
       def x = allAreas.comments.get(itemId)
       if (x != null)  {
	 text = x.text
	 nextLevel = x.includeOverall
       }
       text = text == null ? '' : text
       if (nextLevel) {
	 prefix = getTopLevelComment()
       }
       break
    case 2:
       Map map = isMeasure ? (isSpecial ? measureMapSpecial : measureMap) : packageMap
       def c = obj.get('publicpricecomment')
       text = c == null ? '' : c
       def ia = obj.get('include_area_comment')
       if (ia == null) {
	 ia = true
       }
       if (ia) {
	 prefix = this._getRenderedText(isSpecial, isMeasure, level - 1, obj.get('area'), null)
       }
    }

    return prefix + (prefix.length() > 0 && text.length() > 0 ? "\n\n" : "") + text
  }

  public String getRenderedText(boolean isSpecial, boolean isMeasure, int level, String itemId, obj) {
    String renderedText = _getRenderedText(isSpecial, isMeasure, level, itemId, obj)
    def xdom = services.rendering.parse(renderedText, 'xwiki/2.1')
    return services.rendering.render(xdom, 'xhtml/1.0')
  }
}

surgeryObjects = new SurgeryObjects(xwiki, xcontext, services, sid, error, include_comments, true)

{{/groovy}}
