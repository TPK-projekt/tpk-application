/* eslint-env browser, amd, es6 */
define([], () => {
    "use strict";

    const ign = "[^a-zA-ZåäöÅÄÖ0-9]";
    const ignRe1 = new RegExp(ign + "+", "mg");
    const allIgnRe = new RegExp("^" + ign + "*$", "m");
    const isSpace = new RegExp("^\\s+$");
    const TOPWEIGHT = 3;

    function HighlightSegment (textNode, offset, opts) {
        this.opts = opts;
        this.textNode = textNode;
        this.text = textNode.textContent;
        this.offset = offset;
        this.end = this.text.length + offset;
        this.extraNodes = [];
        this.indicies = [];
    }

    const h = HighlightSegment.prototype;

    h.after = function after (ind) {
        return this.end <= ind[0];
    };

    h.before = function before (ind) {
        return ind[1] <= this.offset;
    };

    h.add = function add (ind) {
        this.indicies.push([ind[0] - this.offset, ind[1] - this.offset]);
    };

    h.sort = function sort () {
        this.indicies.sort((a, b) => a[0] - b[0]);
    };

    const overlaps = (a, b) => a[0] < b[1] && b[0] < a[1];

    h.removeOverlap = function removeOverlap () {
        for (let i = 0; i < this.indicies.length - 1; i += 1) {
            if (overlaps(this.indicies[i], this.indicies[i + 1])) {
                this.indicies.splice(i + 1, 1);
            }
        }
    };

    h.render = function render () {
        this.sort();
        this.removeOverlap();

        const ind = this.indicies;

        if (ind.length > 0) {
            const t = this.text;

            if (ind[0][0] > 0) {
                this.textNode.textContent = t.substring(0, ind[0][0]);
            } else {
                this.textNode.textContent = "";
            }

            let n = this.textNode;

            const ins = (en) => {
                n.parentNode.insertBefore(en, n.nextSibling);
                this.extraNodes.push(en);
                n = en;
            };

            const highlight = (o, l) => {
                const e = document.createElement("span");
                e.setAttribute("class", this.opts.classname);
                e.textContent = t.substring(o, l);
                ins(e);
            };

            const normal = (o, l) => {
                const tn = document.createTextNode(t.substring(o, l));
                ins(tn);
            };

            for (let j = 0; j < ind.length; j += 1) {
                highlight(ind[j][0], ind[j][1]);
                if (j < ind.length - 1) {
                    if (ind[j][1] < ind[j + 1][0]) {
                        normal(ind[j][1], ind[j + 1][0]);
                    }
                } else if (ind[j][1] < t.length) {
                    normal(ind[j][1], t.length);
                }
            }
        }
    };

    h.weight = function weight (start) {
        let w = 0;
        for (let i = 0; i < this.indicies.length; i += 1) {
            w = 1;
            const s = this.indicies[i];
            if (s[0] === 0 && start) {
                return TOPWEIGHT;
            } else if (s[0] > 0 && this.text.charAt(s[0] - 1).match(isSpace)) {
                return 2;
            }
        }
        return w;
    };

    h.toend = function toend () {
        const l = this.indicies.length;
        if (l > 0) {
            return this.indicies[l - 1][1] === this.text.length;
        }
        return false;
    };

    h.reset = function reset () {
        for (let i = 0; i < this.extraNodes.length; i += 1) {
            const n = this.extraNodes[i];
            n.parentNode.removeChild(n);
        }
        this.extraNodes = [];
        this.textNode.textContent = this.text;
        this.indicies = [];
    };


    function Highlight (opts) {
        this.opts = opts;

        const textNodes =
              document.createNodeIterator(
                  opts.root,
                  NodeFilter.SHOW_TEXT,
                  null,

                  /* IE mandatory parameter: fEntityReferenceExpansion */
                  true
              );
        const segments = [];
        let s = "";
        let node = textNodes.nextNode();
        while (node) {
            if (!node.textContent.match(allIgnRe)) {
                segments.push(new HighlightSegment(node, s.length, opts));
                s += node.textContent;
            }
            node = textNodes.nextNode();
        }

        this.text = s;
        this.segments = segments;
    }

    const p = Highlight.prototype;

    p.weight = function weight () {
        let start = true;
        for (let i = 0; i < this.segments.length; i += 1) {
            const w = this.segments[i].weight(start);
            if (w > 0) {
                return i === 0 && w === TOPWEIGHT ? w + 1 : w;
            }
            start = !this.segments[i].toend();
        }
        return 0;
    };

    p.reset = function reset () {
        for (let i = 0; i < this.segments.length; i += 1) {
            this.segments[i].reset();
        }
    };

    p.render = function render () {
        for (let i = 0; i < this.segments.length; i += 1) {
            this.segments[i].render();
        }
    };

    p.pattern = function pattern (s) {
        return s.replace(ignRe1, "");
    };

    p.match_ = function match_ (s) {
        const pattern0 = this.pattern(s);
        if (pattern0.length === 0) {
            return true;
        }

        let pattern = pattern0.charAt(0);

        for (let i = 1; i < pattern0.length; i += 1) {
            pattern += ign + "*" + pattern0.charAt(i);
        }

        const re = RegExp(pattern, "mgi");

        const indicies = [];

        let result = re.exec(this.text);
        if (!result) {
            return false;
        }

        while (result) {
            indicies.push([result.index, re.lastIndex]);
            result = re.exec(this.text);
        }

        let i = 0;
        for (let j = 0; j < this.segments.length; j += 1) {
            const segment = this.segments[j];
            for (;i < indicies.length; i += 1) {
                const ind = indicies[i];

                if (segment.after(ind)) {
                    if (i > 0) {

                        /*
                         * The next segment may also overlap with
                         * previous piece.
                         */
                        i -= 1;
                    }
                    break;
                }
                if (!segment.before(ind)) {
                    segment.add(ind);
                }
            }
        }

        return true;
    };

    p.match = function match (s) {

        this.reset();

        const parts =
              isJust(this.opts.delimiter) ? s.split(this.opts.delimiter) : [s];

        let m = true;

        for (let i = 0; i < parts.length; i += 1) {
            m = m && this.match_(parts[i]);
        }

        this.render();

        return m;
    };

    p.firstMatchPos = function firstMatchPos () {
        if (this.segments.length > 0 && this.segments[0].indicies.length > 0) {
            return this.segments[0].indicies[0][0];
        }
        return -1;
    };

    return Highlight;
});
