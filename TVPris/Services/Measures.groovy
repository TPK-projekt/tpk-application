{{groovy}}
import org.xwiki.velocity.tools.JSONTool

def plainMode = request.get('outputSyntax') == 'plain' && request.get('xpage') == 'plain'
def debug = false

if (plainMode) {
  response.setContentType("application/json")
}

Map result = new HashMap()

try {
  Map measureMap = new TreeMap()

  for (row in services.query.xwql("SELECT o.area, o.code, o.title FROM Document doc, Document vdoc, doc.object(TVPris.Classes.MeasureClass) AS o, vdoc.object(TVPris.Classes.VersionClass) as v WHERE o.area IS NOT NULL AND vdoc.fullName = :versiondoc AND o.version=v.version ORDER BY o.code").bindValue('versiondoc', services.tpkCompare.VERSION_DOCUMENT).execute()) {

    mcategory = measureMap.get(row[0])
    if (mcategory == null) {
      mcategory = new ArrayList<>()
      measureMap.put(row[0] == '' ? 'Åtgärder som ej faller under någotb ehandlingsområde' : row[0], mcategory)
    }
    mcategory.add(["code": row[1], "title": row[2]])
  }
  Map measureMap0 = new TreeMap(new Comparator() {
				  @Override
				  public int compare(o1, o2) {
				    def a1 = measureMap.get(o1)
				    def a2 = measureMap.get(o2)
				    if (a1.size() == 0 && a2.size() == 0) {
				      return 0
				    }
				    if (a1.size() == 0) {
				      return -1
				    }
				    if (a2.size() == 0) {
				      return 1
				    }
				    return a1[0].code - a2[0].code
				  }
				})
  for (m in measureMap.entrySet()) {
    measureMap0.put(m.getKey(), m.getValue())
  }
  result.put('measureMap', measureMap0)
} catch (Exception e) {
  if (result.get('error') == null) {
    result.put('error', "Uncaught exception: " + e)
    response.setStatus(response.SC_INTERNAL_SERVER_ERROR)
  }
}


println('{{{')
println(new JSONTool().serialize(result))
println('}}}')


{{/groovy}}
