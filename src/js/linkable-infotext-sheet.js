/* eslint-env browser, amd, es6 */
require([
    "jquery",
    "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"
], (jQuery) => {
    "use strict";

    jQuery(document).ready(() => {
        const areaInput = jQuery("#linkable-infotext-areas");
        const packageInput = jQuery("#linkable-infotext-packages");
        const measureInput = jQuery("#linkable-infotext-measures");
        const form_token = jQuery("html").attr("data-xwiki-form-token");
        const docname = jQuery("html").attr("data-xwiki-document");

        let {docisnew} = XWiki;

        areaInput.select2({});
        packageInput.select2({});
        measureInput.select2({});

        const save = () => {
            if (docisnew) {
                document.on("xwiki:document:saved", () => {
                    docisnew = false;
                    save0();
                });
                jQuery('input[name="action_saveandcontinue"]').trigger("click");
            } else {
                save0();
            }
        };

        const save0 = () => {
            const note = new XWiki.widgets.Notification("Sparar associationer", "inprogress");
            const ref = XWiki.Model.resolve("TVPris.Services.ItemRef", XWiki.EntityType.DOCUMENT);
            const serviceDoc = new XWiki.Document(ref);
            jQuery.ajax({
                "url": serviceDoc.getURL(),
                "method": "POST",
                "data": {
                    "mareas": areaInput.val(),
                    "packages": packageInput.val(),
                    "measures": measureInput.val(),
                    "docname": docname,
                    "form_token": form_token,
                    "xpage": "plain",
                    "outputSyntax": "plain"
                }
            }).always(() => note.hide()).
                fail(() => new XWiki.widgets.Notification("Misslyckades med att spara associationer!", "error"));
        };

        areaInput.change(save);
        packageInput.change(save);
        measureInput.change(save);
    });
});
