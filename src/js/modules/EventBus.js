/* eslint-env browser, amd, es6 */
define(["jquery"], (jQuery) => {
    "use strict";

    const EXCLUSIVE_HANDLER_DELAY = 10;

    const newEvent = jQuery.Event;

    const isDefined = (x) => typeof x !== "undefined";
    const isUndefined = (x) => !isDefined(x);

    function EventBus () {

        const exclusiveEvents = new Map();
        const exclusiveEventsInProgress = new Set();

        let eventTransaction = false;

        this.initEvents = {};

        const eb = jQuery(this);

        this.onInit = (eventname, handler) => {
            if (isDefined(this.initEvents[eventname])) {
                const e = this.initEvents[eventname];
                if (e.fired) {
                    handler(newEvent(eventname), e.args);
                } else {
                    e.handlers.push(handler);
                }
            } else {
                this.initEvents[eventname] = {
                    "fired": false,
                    "handlers": [handler]
                };
                jQuery(this).one(eventname, (...args) => {
                    const hs = this.initEvents[eventname].handlers;
                    for (let i = 0; i < hs.length; i += 1) {
                        hs[i].apply(null, args);
                    }
                    this.initEvents[eventname].fired = true;
                });
            }
        };

        this.on = eb.on.bind(eb);
        this.off = eb.off.bind(eb);
        this.one = eb.one.bind(eb);

        this.trigger = (eventname, args) => {
            const isEventTransaction = !eventTransaction;
            try {
                if (isEventTransaction) {
                    eventTransaction = true;
                }
                const exclusive = exclusiveEvents.get(eventname);
                if (isDefined(exclusive) && exclusive > 0) {
                    if (exclusiveEventsInProgress.has(eventname)) {
                        return;
                    }
                    exclusiveEventsInProgress.add(eventname);
                    eb.trigger(eventname, args);
                } else {
                    eb.trigger(eventname, args);
                }
            } finally {
                if (isEventTransaction) {
                    setTimeout(
                        () => {
                            exclusiveEventsInProgress.clear();
                            eventTransaction = false;
                        },
                        EXCLUSIVE_HANDLER_DELAY
                    );
                }
            }
        };

        this.onExclusive = (eventname, handler) => {
            let c = exclusiveEvents.get(eventname);
            if (isUndefined(c)) {
                c = 0;
            }
            exclusiveEvents.set(eventname, c + 1);
            eb.on(eventname, handler);
        };

        this.triggerInit = (eventname, args) => {
            if (isUndefined(this.initEvents[eventname])) {
                const e = {
                    "fired": true,
                    "args": []
                };
                if (isDefined(args)) {
                    e.args = args;
                }
                this.initEvents[eventname] = e;
            }
            jQuery(this).trigger(eventname, args);
        };
    }

    if (isUndefined(EventBus.prototype.instance)) {
        EventBus.prototype.instance = new EventBus();
    }

    return EventBus.prototype.instance;
});
