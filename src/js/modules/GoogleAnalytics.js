/* eslint-env browser, amd, es6 */
define(["EventBus"], (EventBus) => {
    "use strict";

    /* eslint-disable indent, wrap-iife, func-names, comma-spacing,
       space-before-blocks, max-len, space-before-function-paren,
       brace-style, block-spacing, max-statements-per-line,
       dot-notation, quotes, space-infix-ops, semi-spacing,
       no-unused-expressions, prefer-rest-params, semi,
       no-sequences, no-implicit-coercion, no-param-reassign,
       prefer-destructuring, no-undef
    */
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga("create", "UA-148379091-1", "auto");
    ga("send", "pageview");

    EventBus.on("transition-to-results", () => ga("send", "pageview"));

});
