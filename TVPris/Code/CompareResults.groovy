{{include reference="TVPris.Code.Mustache" /}}{{groovy}}

import java.text.NumberFormat
import org.xwiki.velocity.VelocityManager

VelocityManager velocityManager = services.component.getInstance(VelocityManager.class)
def vcontext = velocityManager.getVelocityContext()



def checkMissing(int price) {
  def missing_p = new TreeSet()
  def missing_m = new TreeSet()
  missing_p.addAll(tpkComparePackages)
  missing_m.addAll(tpkCompareMeasures)
  def items = price.get('items')
  for (int j = 0; j < items.size(); j++) {
    def item = items.get(j)
    if (item == null) {
      continue;
    }
    def ma = price.get('is_m').get(j) ? missing_m : missing_p;
    if (ma.contains(item)) {
      ma.remove(item)
    }
  }
  price.errors = [];
  def join = { set ->
    def s = ""
    def first = true
    for (i in set) {
      if (first) {
	first = false
      } else {
	s += ", "
      }
      s += i
    }
    return s
  };
  if (missing_p.length > 0) {
    price.errors.add(["text": 'Saknar prisuppgift för ' + (missing_p.size() > 1 ? 'paketen ' : 'paket ') + join(missing_p)]);
  }
  if (missing_m.length > 0) {
    price.errors.add(["text": 'Saknar prisuppgift för: ' + (missing_m.size() > 1 ? 'åtgärderna ' : 'åtgärden ') + join(missing_m)]);
  }
}

 
def addComments(price) {
  price.put('comment_texts', [])
  price.put('has_comments', false)
  def comment = price.get('comment')
  if (comment != null) {
    for (int i = 0; i < comment.size(); i++) {
      if (comment.get(i) != null) {
	var c = (comment.size() > 1 ?
		 ( price.get('is_m').get(i) ? 'åtgärd' : 'paket' ) + ' ' + price.get('items').get(i) + ': ' :
		 '') + comment.get(i)
	price.put('has_comments', true)
	price.get('comment_texts').add([ 'text': c ])
      }
    }
  }
}

Map<String, Object> processData(Map<String, Object> data, int offset) {
  def surgeryView = xwiki.getDocument(services.model.resolveDocument('TVPris.View.Mottagning'))
  Locale sv = new Locale('sv', 'SE')
  NumberFormat percentNF = NumberFormat.getPercentInstance(sv)
  NumberFormat currencyNF = NumberFormat.getCurrencyInstance(sv)
  currencyNF.setMaximumFractionDigits(0)
  if (data.get('r') != null) {
    List a = data.get('r');
    for (int i = 0; i < a.size(); i++) {
      Map<String, Object> p = a.get(i);
      try {
	p.put('rank', i + 1 + offset)
	p.put('surgery_view', surgeryView.getURL('view', 'sid=' + p.get('surgery_id')))
	if (p.get('n') < data.get('n')) {
	  p.put('success', false)
	  checkMissing(p);
	} else {
	  p.put('success', true)
	  if (data.get('reference_price') != null) {
	    def c = p.get('price_total') /  data.get('reference_price')
	    p.put('comp_text', percentNF.format(c))
	  }
	  p.put('price_text', currencyNF.format(p.get('price_total')))
	  addComments(p);
	}
      } catch (Exception e) {
	p.put('success', false)
	p.put('errors', [[ 'text': "Fel uppstod! " + e ]])
      }
      p.put('error', !p.get('success'))
    }
    data.put('r', a)
    if (data.get('reference_price') != null) {
      data.put('reference_price_text', currencyNF.format(data.get('reference_price')))
    }
    if (data.get('reference_price_special') != null) {
      data.put('reference_price_special_text', currencyNF.format(data.get('reference_price_special')))
    }
    data.put('has_header', true)
  }
  return data
}

if (vcontext.get('doCompare')) {
  def compiler = new MustacheCompiler(xwiki)
  def mustache = compiler.compileTemplate(services.model.resolveDocument('TVPris.Templates.CompareResults'))

  Writer writer = new StringWriter()
  Integer offset = vcontext.get('tpkOffset')
  mustache.execute(writer, processData(vcontext.get('tpkCompareResult'), offset == null  ? 0 : offset))
  println('((({{html}}' + writer.toString() + '{{/html}})))')
}

{{/groovy}}
