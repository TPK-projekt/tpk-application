/* eslint-env browser, amd, es6 */
/* global $modelDeclarationJSON:false */

/**
 * @license
 * #includeInContext('TVPris.Model.Declaration')
 * #set ($compareServiceURL = '/rest/tpk-compare')
 * #set ($modelDeclarationJSON = $jsontool.serialize($tpk_model))
 */

require([
    "babel-polyfill",
    "EventBus",
    "jquery",
    "behavior",
    "Notifyer",
    "Position",
    "SelectionPresenter",
    "History",
    "StateTransition",
    "MapInterface",
    "Inputs",
    "Result",
    "Header",
    "GoogleAnalytics"
], (
    _pf,
    EventBus,
    jQuery,
    behavior,
    notifyer,
    Position,
    SelectionPresenter,
    history,
    StateTransition,
    MapInterface,
    inputsRegister,
    resultRegister
) => {
    "use strict";

    const ARRAY_DELIM = "$services.tpkCompare.ARRAY_DELIM";
    const compareButton = jQuery("#compare-button");
    const mapInterface = new MapInterface({"container_id": "tvpris-map"});
    const deferred = jQuery.Deferred.bind(jQuery);
    let n_blocks = 0;

    const isDefined = (x) => typeof x !== "undefined";
    const isUndefined = (x) => !isDefined(x);
    const isNothing = (x) => isUndefined(x) || x === null;
    const isEmpty = (x) => isNothing(x) || x === "";
    const isJust = (x) => !isNothing(x);
    const isFunc = (x) => isJust(x) && typeof x === "function";
    const ID = (x) => x;

    const QUERY_WAIT_TIMEOUT = 50;
    const UPDATE_EVENT_TIMEOUT = 10;

    window.isDefined = isDefined;
    window.isUndefined = isUndefined;
    window.isNothing = isNothing;
    window.isJust = isJust;
    window.isEmpty = isEmpty;
    window.isFunc = isFunc;
    window.ID = ID;
    window.ARRAY_DELIM = ARRAY_DELIM;

    if (!isFunc(Number.isInteger)) {
        Number.isInteger = (value) => typeof value === "number" &&
            isFinite(value) && Math.floor(value) === value;
    }

    const packagesCodeMap = new Map();
    const measuresCodeMap = new Map();
    const validate = () => validatePosition();

    let tpkMode = null;
    let formToResultTransition = null;
    let validatePosition = ID;

    let countyInput = null;
    let municipalityInput = null;
    let sublocalityInput = null;
    let packagesInput = null;
    let measuresInput = null;
    let filterFlagInputs = null;
    let _surgeryCategoryInput = null;
    let sortOrderInput = null;
    let sortFieldInput = null;

    const block = () => {
        n_blocks += 1;
        compareButton.attr("disabled", "disabled");
        if (n_blocks === 1) {
            EventBus.trigger("block");
        }
    };

    const unblock = () => {
        n_blocks -= 1;
        if (n_blocks === 0) {
            compareButton.attr("disabled", null);
            EventBus.trigger("unblock");
        }
    };

    const objEquals = (a, b) => {
        let equals = true;

        if (a === null || b === null) {
            return a === b;
        }

        let la = 0;
        let lb = 0;

        jQuery.each(a, (key, value) => {
            if (typeof value !== typeof b[key] || value !== b[key]) {
                equals = false;
            }
            la += 1;
        });
        if (!equals) {
            return equals;
        }
        jQuery.each(b, () => {
            lb += 1;
        });
        return la === lb;
    };

    const inputVal = (input, newVal, options) => {
        const opts = isDefined(options) ? options : {};
        const jq = jQuery(isJust(input.selector)
            ? input.selector
            : "#" + input.id);
        let val = isFunc(input.val) ? input.val(jq) : jq.val();

        if (isDefined(newVal)) {
            const arrayDiffers = Array.isArray(newVal) &&
                  !objEquals(val, newVal);
            const scalarDiffers = !Array.isArray(newVal) &&
                  val !== newVal;
            if (arrayDiffers || scalarDiffers) {
                if (isFunc(input.val)) {
                    input.val(jq, newVal);
                } else {
                    jq.val(newVal);
                }
                val = newVal;
                if (!opts.nopush) {
                    if (typeof input.follower === "object") {
                        inputVal(
                            input.follower,
                            inputVal(input),
                            {"nopush": true}
                        );
                    }
                    if (typeof input.duplicate === "object") {
                        inputVal(
                            input.duplicate,
                            inputVal(input),
                            {"nopush": true}
                        );
                    }
                }
                if (!opts.notrigger) {
                    jq.trigger("change");
                }
                if (isFunc(input.refresh)) {
                    input.refresh();
                }
            }
        }
        return val;
    };

    const contains = (s, s0) => s.indexOf(s0) >= 0;

    const checkboxListVal = (e, newVal) => {
        if (isUndefined(newVal)) {
            const val = [];
            e.find("input[type=checkbox]:checked,input[type=radio]:checked,option:selected").
                each((index, element) => val.push(jQuery(element).val()));
            return val;
        }
        e.find('input[type="checkbox"]').each((index, element) => {
            element.checked = newVal !== null && contains(newVal, element.getAttribute("value"));
        });
        e.find('input[type="radio"]').each((index, element) => {
            element.checked = newVal !== null && contains(newVal, element.getAttribute("value"));
        });
        e.find("option").each((index, element) => {
            element.selected = newVal !== null && contains(newVal, element.getAttribute("value"));
        });
        return newVal;
    };

    const checkboxVal = (e, newVal) => {
        if (isDefined(newVal)) {
            if (newVal) {
                e.attr({"checked": "checked"});
            } else {
                e.removeAttr("checked");
            }
            return newVal;
        }
        if (e.length > 0 &&
            e.get(0).checked) {
            return true;
        }
        return null;
    };

    const tpk_model = $modelDeclarationJSON;

    const modelInterface = new ModelInterface(tpk_model);

    const arrayParse = (s) => {
        if (isJust(s)) {
            if (s === "") {
                return [];
            }
            return s.split(ARRAY_DELIM);
        }
        return null;
    };

    const arraySerialize = (a) => {
        if (isJust(a)) {
            return a.join(ARRAY_DELIM);
        }
        return null;
    };

    const initInputs = () => {
        const _extend = (id, object) => {
            let obj = object;
            obj.id = id;
            if (typeof tpk_model[id] === "object") {
                obj = jQuery.extend(tpk_model[id], obj);
            }
            tpk_model[id] = obj;
            if (isNothing(object.duplicate) && !object.nohistory) {
                const isArray = isJust(object.isArray) && object.isArray;
                const parse = isArray ? arrayParse : ID;
                const serialize = isArray ? arraySerialize : ID;
                history.register({
                    "load": (v) => inputVal(obj, v),
                    "save": () => inputVal(obj),
                    "parse": parse,
                    "serialize": serialize,
                    "name": tpk_model[id].name
                });
            }
            return obj;
        };

        sublocalityInput = _extend("tvpris-sublocality-input", {
            "count": true,
            "view": false,
            "link": true,
            "isArray": true,
            "nohistory": true,
            "val": modelInterface.sublocality.bind(modelInterface)
        });
        municipalityInput = _extend("tvpris-municipality-input", {
            "count": true,
            "view": false,
            "link": true,
            "isArray": true,
            "nohistory": true,
            "val": modelInterface.municipality.bind(modelInterface)
        });
        countyInput = _extend("tvpris-county-input", {
            "count": true,
            "view": false,
            "link": true,
            "isArray": true,
            "nohistory": true,
            "val": modelInterface.county.bind(modelInterface)
        });
        packagesInput = _extend("package-input-select", {
            "count": false,
            "view": false,
            "link": true,
            "refresh": () => jQuery("#" + packagesInput.id).trigger("tpk-input-refresh"),
            "isArray": true,
            "val": checkboxListVal,
            "defaultValue": () => {
                const ms = inputVal(measuresInput);
                if (isJust(ms) && ms.length > 0) {
                    return null;
                }
                return [1];
            }
        });
        measuresInput = _extend("measures-input", {
            "count": false,
            "view": false,
            "link": true,
            "refresh": () => jQuery("#" + measuresInput.id).trigger("tpk-input-refresh"),
            "isArray": true,
            "val": checkboxListVal
        });
        const eveningHoursInput = _extend("tvpris-evening-hours", {
            "count": true,
            "view": false,
            "link": true,
            "val": checkboxVal
        });
        const weekendHoursInput = _extend("tvpris-weekend-hours", {
            "count": true,
            "view": false,
            "link": true,
            "val": checkboxVal
        });
        const haveParkingInput = _extend("tvpris-have-parking", {
            "count": true,
            "view": false,
            "link": true,
            "val": checkboxVal
        });
        const publicTransportInput = _extend("tvpris-public-transport", {
            "count": true,
            "view": false,
            "link": true,
            "val": checkboxVal
        });
        const dentalFearInput = _extend("tvpris-dental-fear", {
            "count": true,
            "view": false,
            "link": true,
            "val": checkboxVal
        });
        filterFlagInputs = [
            eveningHoursInput,
            weekendHoursInput,
            haveParkingInput,
            publicTransportInput,
            dentalFearInput
        ];
        _surgeryCategoryInput = _extend("surgery-category-input", {
            "count": true,
            "view": false,
            "link": true,
            "val": checkboxListVal,
            "isArray": true
        });
        sortOrderInput = _extend("tvpris-sort-order", {
            "count": false,
            "view": true,
            "link": true
        });
        sortFieldInput = _extend("tvpris-sort-field", {
            "count": false,
            "view": true,
            "link": true
        });
        _extend("surgeries-input", {
            "count": true,
            "view": false,
            "link": true,
            "isArray": true
        });
        _extend("tvpris-limit", {
            "count": false,
            "view": true,
            "link": false
        });

        inputsRegister(
            modelInterface,
            tpk_model,
            _extend,
            inputVal
        );

        jQuery("#" + packagesInput.id + " option").each((_index, element) => {
            packagesCodeMap.set(element.getAttribute("value"), {
                "text": jQuery(element).text(),
                "more": jQuery(element).attr("data-more")
            });

        });
        jQuery("#" + measuresInput.id + " option").each((_index, element) => {
            measuresCodeMap.set(element.getAttribute("value"), {
                "text": jQuery(element).text(),
                "more": jQuery(element).attr("data-more")
            });
        });

        EventBus.on("change:" + measuresInput.id, () => {
            const v = inputVal(measuresInput);
            if (isJust(v) && v.length > 0) {
                inputVal(packagesInput, null);
                EventBus.trigger("change:" + packagesInput.id);
            }
        });

        EventBus.on("change:" + packagesInput.id, () => {
            const v = inputVal(packagesInput);
            if (isJust(v) && v.length > 0) {
                inputVal(measuresInput, null);
                EventBus.trigger("change:" + measuresInput.id);
            }
        });
    };

    const inputJQuery = (input) => {
        const elements = [];
        jQuery(isJust(input.selector)
            ? input.selector
            : "#" + input.id).
            each((index, element) => {
                const e = contains(["input", "select"], element.tagName.toLowerCase())
                    ? jQuery(element)
                    : jQuery(element).find("input, select");
                const je = jQuery(e);
                for (let i = 0; i < je.length; i += 1) {
                    elements.push(je.get(i));
                }
            });
        return jQuery(elements);
    };

    const synchTransition = new StateTransition(
        () => EventBus.trigger("becomes-synchronized"),
        () => EventBus.trigger("becomes-unsynchronized"),
        true
    );

    const filterPresenterSource = () => {
        const v = [];
        for (let i = 0; i < filterFlagInputs.length; i += 1) {
            const input = filterFlagInputs[i];
            jQuery("#" + input.id + ":checked").each((_index, element) => {
                v.push({
                    "text": jQuery(element).parent().find("label").text(),
                    "remove": () => inputVal(input, false)
                });
            });
        }
        return v;
    };

    const initSurgery = () => {
        mapInterface.show();
        const sinfo = JSON.parse(jQuery("#tvpris-map").attr("data-surgery-info"));
        mapInterface.addSurgery(sinfo);
        EventBus.trigger("surgery-highlighted", [sinfo.s_id]);
        behavior.accordion(jQuery("#surgery-package-prices .collapse"));
        behavior.accordion(jQuery("#tpk-surgery-measure-prices .collapse"));
    };

    const initLandingpage = () => {
        jQuery(".tpk-landing-page-image").each((_index, element) => {
            let wide = [];
            let small = [];
            try {
                const s = JSON.parse(element.getAttribute("data-small"));
                if (s !== null) {
                    small = s;
                }
            } catch (e) {
                // Ignore
            }
            try {
                const w = JSON.parse(element.getAttribute("data-wide"));
                if (w !== null) {
                    wide = w;
                }
            } catch (e) {
                // Ignore
            }

            const v = [[small, "small"], [wide, "wide"]];
            for (let i = 0; i < v.length; i += 1) {
                const imgs = v[i];
                if (imgs[0].length > 0) {
                    const ns = Math.floor(Math.random() * imgs[0].length);
                    const img = jQuery(`<img class="${imgs[1]}-image" alt="" />`);
                    img.attr("src", imgs[0][ns]);
                    jQuery(element).append(img);
                }
            }

        });
    };

    const initTpk = () => {
        EventBus.on("synchronize", () => synchTransition.to());

        mapInterface.setTabindex = true;

        formToResultTransition = new StateTransition(() => {
            jQuery("#tvpris-result-section").show();
            jQuery("#price-query-main-input-section").hide();
            jQuery("#price-query-results-selection-presenter").show();
            mapInterface.show();
            EventBus.trigger("view-transition");
            EventBus.trigger("transition-to-results");
            window.scrollTo(0, 0);
        }, () => {
            jQuery("#tvpris-result-section").hide();
            jQuery("#price-query-main-input-section").show();
            jQuery("#price-query-results-selection-presenter").hide();
            mapInterface.hide();
            EventBus.trigger("view-transition");
            EventBus.trigger("transition-from-results");
            window.scrollTo(0, 0);
        }, false, validate);

        initInputs();
        displayResults = resultRegister(
            modelInterface,
            inputVal,
            doCompare,
            formToResultTransition,
            packagesCodeMap,
            measuresCodeMap,
            sortFieldInput,
            sortOrderInput,
            block,
            unblock
        );

        const formToResultHistory = formToResultTransition.history(true);
        formToResultHistory.name = "res";
        formToResultHistory.order = 110;
        formToResultHistory.manualScroll = true;
        const formTransLoad = formToResultHistory.load;
        formToResultHistory.load = (v) => {
            if (synchTransition.atInitial) {
                displayResults();
            }
            return formTransLoad(v);
        };
        history.register(formToResultHistory);

        history.addReplaceEvents("parameters-updated");
        history.addPushEvents("view-transition");

        const inputChange = (inp) => {
            let input = inp;
            if (typeof input.follower === "object") {
                inputVal(
                    input.follower,
                    inputVal(input),
                    {
                        "nopush": true,
                        "notrigger": true
                    }
                );
            }
            if (typeof input.duplicate === "object") {
                inputVal(
                    input.duplicate,
                    inputVal(input),
                    {
                        "nopush": true,
                        "notrigger": true
                    }
                );
                input = input.duplicate;
            }
            EventBus.trigger("change:" + input.id);
            const eventName = input.view ? "view-updated" : "parameters-updated";
            setTimeout(
                () => EventBus.trigger(eventName, [input]),
                UPDATE_EVENT_TIMEOUT
            );
        };

        jQuery.each(tpk_model, (id, input) => {
            const ji = inputJQuery(input);
            if (typeof input.duplicate === "object") {
                input.duplicate.follower = input;
            }
            ji.change(inputChange.bind(null, input));
            ji.each((index, element) => {
                const je = jQuery(element);
                if (je.hasClass("tvpris-checkbox") || je.hasClass("tvpris-radio")) {
                    const tr = je.parents(".item-selector, .selectable-item-row, tr");
                    if (tr.length > 0) {
                        tr.click((event) => {
                            if (!jQuery(event.target).is(je)) {
                                je.trigger("click", event);
                            }
                        });
                    }
                }
            });
        });

        const itemSource = (input, map, withRemove = false) => () => {
            const v = inputVal(input);
            if (withRemove) {
                if (Array.isArray(v)) {
                    return v.map((v0) => ({
                        "text": "<span class=\"item-wrapper\"><span class=\"item-code\">" + v0 +
                            "</span><span class=\"item-text\">" + map.get(v0).text + "</span></span>",
                        "remove": () => {
                            const w = inputVal(input);
                            const i = w.indexOf(v0);
                            if (i >= 0) {
                                w.splice(i, 1);
                                inputVal(input, w);
                            }
                            return false;
                        },
                        "removeAriaLabel": "Ta bort val: " + map.get(v0).text
                    }));
                } else if (isJust(v)) {
                    return [
                        {
                            "text": map.get(v),
                            "remove": () => {
                                inputVal(input, null);
                                return false;
                            },
                            "removeAriaLabel": "Ta bort val: " + map.get(v)
                        }
                    ];
                }
            } else if (Array.isArray(v)) {
                return v.map(map.get.bind(map));
            } else if (isJust(v)) {
                return [map.get(v)];
            }
            return [];
        };

        new SelectionPresenter({
            "container": jQuery("#selected-measures-presenter"),
            "events": "change:" + measuresInput.id,
            "htmlSource": true,
            "source": itemSource(measuresInput, measuresCodeMap, true),
            "placeHolder": null
        });

        new SelectionPresenter({
            "container": jQuery(".filter-presenter"),
            "events": filterFlagInputs.map((inp) => "change:" + inp.id).join(" "),
            "source": filterPresenterSource
        });

        new SelectionPresenter({
            "container": jQuery(".filter-count-presenter"),
            "events": filterFlagInputs.map((inp) => "change:" + inp.id).join(" "),
            "source": () => [filterPresenterSource().length]
        });

        new SelectionPresenter({
            "container": jQuery(".tpk-measure-vs-package"),
            "events": "change:" + packagesInput.id + " change:" + measuresInput.id,
            "source": () => {
                const pv = inputVal(packagesInput);
                const mv = inputVal(measuresInput);
                if (isJust(pv) && pv.length > 0) {
                    return ["detta behandlingspaket"];
                } else if (isJust(mv) && mv.length === 1) {
                    return ["denna åtgärd"];
                }
                return ["dessa åtgärder"];
            }
        });

        const cycleSortOrder = () => {
            const so0 = modelInterface.sortOrder();
            const sf0 = modelInterface.sortField();
            let so = isEmpty(so0) ? "asc" : so0;
            let sf = isEmpty(sf0) ? "price" : sf0;

            if (so === "asc") {
                so = "desc";
            } else {
                so = "asc";
                if (sf === "price") {
                    sf = "s_name";
                } else {
                    sf = "price";
                }
            }
            inputVal(sortOrderInput, so);
            inputVal(sortFieldInput, sf);
        };

        jQuery("#sort-order-cycler").click(cycleSortOrder);

        new SelectionPresenter({
            "container": jQuery(".sort-order-presenter"),
            "events": "change:" + sortOrderInput.id + " " +
                "change:" + sortFieldInput.id,
            "htmlSource": true,
            "source": () => {
                switch (modelInterface.sortField()) {
                case "price":
                    switch (modelInterface.sortOrder()) {
                    case "asc":
                        return ["Lägst pris först"];
                    case "desc":
                        return ["Högst pris först"];
                    default:
                        return ["Lägst pris först"];
                    }
                case "s_name":
                    switch (modelInterface.sortOrder()) {
                    case "asc":
                        return ["Från A till Ö"];
                    case "desc":
                        return ["Från Ö till A"];
                    default:
                        return ["Från A till Ö"];
                    }
                default:
                    return ["Lägst pris först"];
                }
            }
        });

        const position = new Position({
            "block": block,
            "unblock": unblock,
            "sublocalityInput": sublocalityInput,
            "municipalityInput": municipalityInput,
            "countyInput": countyInput,
            "inputVal": inputVal
        });

        validatePosition = () => position.validate();

        EventBus.on("sort-order-change", (event, field, order) => {
            const sf = jQuery("#" + sortFieldInput.id);
            const of = jQuery("#" + sortOrderInput.id);
            let change = false;
            if (sf.val() !== field) {
                sf.val(field);
                change = true;
            }
            if (of.val() !== order) {
                of.val(order);
                change = true;
            }
            if (change) {
                sf.trigger("change");
            }
        });

    };

    let queryModelData = null;
    let queryPromise = null;
    let scheduledQueryData = null;

    const _doQuery = (data) => {
        scheduledQueryData = data;
        const d = deferred();
        if (!validate()) {
            d.reject().promise();
        }
        setTimeout(() => _doQuery(scheduledQueryData).done(d.resolve.bind(d)).
            fail(d.reject.bind(d)), QUERY_WAIT_TIMEOUT);
        return d.promise();
    };

    const doQuery = (data) => {
        if (objEquals(queryModelData, data)) {
            return queryPromise;
        }
        const clonedData = jQuery.extend({}, data);
        queryModelData = data;
        const promise = jQuery.ajax({
            "url": "$compareServiceURL",
            "data": clonedData
        }).done((data0) => {
            if (typeof data0.count === "number") {
                modelInterface.setCount(data0.count);
            }
        });

        promise.always(notifyer.progress("$services.localization.render('tvpris.comparing')"));
        promise.fail((jqXHR, textStatus, errorThrown) => {
            notifyer.error("$services.localization.render('tvpris.comparison_failed'): " + textStatus + " " + errorThrown);
        });

        queryPromise = promise;

        EventBus.trigger("query-initiated", [promise]);

        return promise;
    };

    const clearView = () => {
        EventBus.trigger("clear-view");
        EventBus.trigger("surgeries-cleared");
        jQuery("#tvpris-search-result-table").find("div").remove();
    };

    let displayResults = ID;

    jQuery("#price-query-input-form").submit((e) => {
        e.stopPropagation();
        e.preventDefault();
        displayResults();
    });

    const doCompare = (offset, limit) => {
        const COMPARE_DELAY = 10;
        const d = deferred();
        block();
        d.always(unblock);
        setTimeout(
            () => _doCompare(offset, limit).
                done(d.resolve.bind(d)).
                fail(d.reject.bind(d)),
            COMPARE_DELAY
        );
        return d.promise();
    };

    const _doCompare = (offset, limit) => {
        const {data} = modelInterface.data(false);
        data["$services.tpkCompare.OFFSET_PARAMETER"] = offset;
        data["$services.tpkCompare.LIMIT_PARAMETER"] = limit;
        const promise = doQuery(data);

        return promise;
    };

    compareButton.click((e) => {
        block();
        try {
            e.stopPropagation();
            e.preventDefault();
            displayResults();
        } finally {
            unblock();
        }
    });
    jQuery(["sublocality", "municipality", "county"]).
        each((region) => EventBus.on(region + "-selected", (e, reg) => {
            const elem = jQuery("#tvpris-" + region + "-input");
            if (elem.val() !== reg) {
                elem.val(reg);
                elem.trigger("change");
            }
        }));

    let highlighting = false;
    EventBus.on(
        "surgery-highlighted",
        (event, surgeryId) => {
            if (highlighting) {
                return;
            }
            highlighting = true;
            try {
                const restab = jQuery("#tvpris-search-result-table");
                const item = restab.find('[data-surgery-id="' + surgeryId + '"]');
                item.collapse("show");
            } finally {
                highlighting = false;
            }

        }
    );

    function ModelInterface (tpkModel) {
        const DEFAULT_LIMIT = 20000;

        this.numSurgeries = null;
        this.pageNumber = 1;
        this.numPages = null;
        this.special = false;
        this.tpkModel = tpkModel;
        this.trigger = true;
        this.triggering = false;

        EventBus.onExclusive("parameters-updated", (_event, _input) => {
            this.setCount(null);
            this.reset();
            synchTransition.from();
            if (!formToResultTransition.atInitial) {
                modelInterface.reset();
                displayResults();
            }
        });

        EventBus.on("set-page", (_, page) => {
            if (this.triggering) {
                return;
            }
            this.trigger = false;
            try {
                this.setPage(page);
            } finally {
                this.trigger = true;
            }
        });

        EventBus.onExclusive("view-updated", () => {
            this.reset();
            displayResults();
        });

        this.getModel = () => this.tpkModel;

        this.inputVal = inputVal;

        this.reset = function reset () {
            this.numPages = null;
            this.pageNumber = 1;
            clearView();
        };

        this.count = function count () {
            return this.numSurgeries;
        };

        this.setCount = function setCount (count) {
            this.numSurgeries = count;
            EventBus.trigger("count-updated", [count]);
        };

        this.page = function page () {
            return this.pageNumber;
        };

        this.setPage = function setPage (pageNumber) {
            if (this.pageNumber !== pageNumber) {
                this.pageNumber = isNothing(pageNumber) ? 1 : pageNumber;
                if (this.trigger) {
                    this.triggering = true;
                    try {
                        EventBus.trigger("set-page", [pageNumber]);
                    } finally {
                        this.triggering = false;
                    }
                }
            }
        };

        this.limit = function limit () {
            const limit0 = parseInt(jQuery("#tvpris-limit").val(), 10);
            if (isNaN(limit0)) {
                return DEFAULT_LIMIT;
            }
            return limit0;
        };

        this.sortOrder = function sortOrder () {
            return jQuery("#tvpris-sort-order").val();
        };

        this.sortField = function sortField () {
            return jQuery("#tvpris-sort-field").val();
        };

        this.county = function county (jq, newVal) {
            if (isDefined(newVal)) {
                jq.val(arraySerialize(newVal));
            }

            const county0 = jq.val();
            if (county0) {
                return arrayParse(county0);
            }

            return null;
        };

        this.municipality = function municipality (jq, newVal) {
            if (isDefined(newVal)) {
                jq.val(arraySerialize(newVal));
            }

            const m = jq.val();
            if (m) {
                return arrayParse(m);
            }

            return null;
        };

        this.sublocality = function sublocality (jq, newVal) {
            if (isDefined(newVal)) {
                jq.val(arraySerialize(newVal));
            }

            const s = jq.val();
            if (s) {
                return arrayParse(s);
            }

            return null;
        };

        this.data = function data (forCount, forLinkIn) {
            let forLink = forLinkIn;
            if (isNothing(forLink)) {
                forLink = false;
            }
            const data0 = {};
            let addedSomething = false;
            if (this.numSurgeries !== null && !forCount) {
                data0.count = this.numSurgeries;
                addedSomething = true;
            }
            if (this.isSpecial()) {
                data0.special = true;
                addedSomething = true;
            }
            jQuery.each(tpk_model, (id, input) => {
                const countButNotView = !input.view && input.count;
                const notForLink = !forLink || input.link;
                const whenCount = !forCount || countButNotView;
                if (whenCount && notForLink &&
                    isNothing(input.duplicate)) {
                    let val = inputVal(input);
                    let empty = isUndefined(val);
                    if (!empty) {
                        empty = Array.isArray(val) && val.length === 0;
                    }
                    empty = empty || isEmpty(val);
                    if (empty && isFunc(input.defaultValue)) {
                        val = input.defaultValue();
                    }
                    if (isDefined(val)) {
                        if (Array.isArray(val)) {
                            if (val.length > 0) {
                                data0[input.name] = val.join(ARRAY_DELIM);
                                addedSomething = true;
                            }
                        } else if (val !== null && val !== "") {
                            data0[input.name] = val;
                            addedSomething = true;
                        }
                    }
                }
            });
            if (!forCount) {
                data0.count = this.count();
                data0[tpk_model["tvpris-limit"].name] = this.limit();
                data0[tpk_model["tvpris-offset"].name] = (this.page() - 1) * this.limit();
                addedSomething = true;
            }
            return {
                "data": data0,
                "addedSomething": addedSomething
            };
        };

        /*
         * Identifier for the currently matched result array
         * (limit and page size not included, although sort-order is.).
         */
        this.key = function key () {
            const {data} = this.data();
            let s = "";
            jQuery.each(tpk_model, (id, inp) => {
                if (!(inp.view || id === "tvpris-offset")) {
                    const {name} = inp;
                    const obj = {};
                    obj[name] = data[name];
                    s += JSON.stringify(obj);
                }
            });
            return s;
        };

        this.isSynchronized = function isSynchronized () {
            return queryModelData !== null &&
                objEquals(this.data(false).data, queryModelData);
        };

        this.setSpecial = function setSpecial (special) {
            this.special = special;
        };

        this.isSpecial = function isSpecial () {
            return this.special;
        };
    }

    EventBus.onInit("application-init-complete", () => {
        history.init();
        history.replace();
    });

    function init () {
        history.preserveParameters("minify");
        behavior.initInputClickedFocus();
        modelInterface.setSpecial(jQuery("#price-query-input-form").attr("data-tpk-special") === "true");
        tpkMode = jQuery("#tpk-application-container").attr("data-tpk-mode");
        if (tpkMode === "main") {
            initTpk();
        } else if (tpkMode === "surgery") {
            initSurgery();
        } else if (tpkMode === "landingpage") {
            initLandingpage();
        }

        EventBus.triggerInit("application-init", [modelInterface]);
        EventBus.triggerInit("application-init-complete", [tpkMode]);
    }

    jQuery(document).ready(init);
});
