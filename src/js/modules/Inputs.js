/* eslint-env browser, amd, es6 */
define([
    "jquery",
    "EventBus",
    "History",
    "StateTransition",
    "Notifyer",
    "StructuredCombobox",
    "Infobox"
], (
    jQuery,
    EventBus,
    history,
    StateTransition,
    notifyer,
    StructuredCombobox,
    infoBox
) => {
    "use strict";

    const register = (
        model,
        tpk_model,
        extend,
        inputVal
    ) => {
        const packageComboToggle = jQuery("#structured-combobox-button-p");
        if (!model.isSpecial()) {
            const packageInput = tpk_model["package-input-select"];
            const packageCombo = new StructuredCombobox(packageComboToggle);
            let pblock = false;
            packageCombo.relabel(packageInput.id, "structured-combobox-button-p");
            packageCombo.element.on("change", () => {
                if (!pblock) {
                    try {
                        pblock = true;
                        inputVal(packageInput, packageCombo.val());
                    } finally {
                        pblock = false;
                    }
                }
            });
            packageCombo.element.on("scb-done", () => jQuery("#position-input").click());
            const psync = () => {
                if (!pblock) {
                    try {
                        pblock = true;
                        packageCombo.val(inputVal(packageInput));
                    } finally {
                        pblock = false;
                    }
                }
            };
            EventBus.on("change:" + packageInput.id, psync);
            EventBus.onInit("history-loaded", psync);

            jQuery("#tpk-example-packages a").click((event) => {
                const p = event.target.getAttribute("data-package");
                try {
                    inputVal(packageInput, [p]);
                } catch (e) {
                    console.log("Caught exception: " + e);
                }
                return false;
            });
        }

        const measuresInput = tpk_model["measures-input"];
        const measuresComboToggle = jQuery("#structured-combobox-button-m");
        const measuresCombo =
              new StructuredCombobox(measuresComboToggle, {
                  "multi": true,
                  "multiSelectTextTemplate": "{} åtgärder valda"
              });

        measuresCombo.relabel(measuresInput.id, "structured-combobox-button-m");

        let mblock = false;
        measuresCombo.element.on("change", () => {
            if (!mblock) {
                try {
                    mblock = true;
                    inputVal(measuresInput, measuresCombo.val());
                } finally {
                    mblock = false;
                }
            }
        });
        measuresCombo.element.on("scb-done", () => jQuery("#position-input").click());
        const msync = () => {
            if (!mblock) {
                try {
                    mblock = true;
                    measuresCombo.val(inputVal(measuresInput));
                } finally {
                    mblock = false;
                }
            }
        };
        EventBus.on("change:" + measuresInput.id, msync);
        EventBus.onInit("history-loaded", msync);
        jQuery("#tpk-example-measures a").click((event) => {
            const m = event.target.getAttribute("data-measure");
            try {
                let v = inputVal(measuresInput);
                if (isJust(v)) {
                    v.push(m);
                } else {
                    v = [m];
                }
                inputVal(measuresInput, v);
            } catch (e) {
                console.log("Caught exception: " + e);
            }
            return false;
        });
    };

    EventBus.onInit("application-init", (_, model) => {
        infoBox(jQuery(document).find("[data-infobox]"));
        if (!model.isSpecial()) {
            const packageMeasureTransition = new StateTransition(() => {
                EventBus.trigger("selecting-measures");
                jQuery("#package-selection-section, #surgery-package-prices").hide();
                jQuery("#item-selection-section, #tpk-surgery-measure-prices").show();
                jQuery("#tpk-measure-package-select").addClass("measures");
                jQuery("#tpk-select-packages").parent().addClass("active");
                jQuery("#tpk-select-measures").parent().removeClass("active");
            }, () => {
                EventBus.trigger("selecting-packages");
                jQuery("#package-selection-section, #surgery-package-prices").show();
                jQuery("#item-selection-section, #tpk-surgery-measure-prices").hide();
                jQuery("#tpk-select-measures").parent().addClass("active");
                jQuery("#tpk-select-packages").parent().removeClass("active");
                jQuery("#tpk-measure-package-select").removeClass("measures");
            });
            const pmHistory = packageMeasureTransition.history(true);
            pmHistory.name = "pm";
            history.register(pmHistory);
            history.addPushEvents("selecting-measures", "selecting-packages");
            jQuery("#tpk-select-packages").click(() => {
                packageMeasureTransition.from();
                return false;
            });
            jQuery("#tpk-select-measures").click(() => {
                packageMeasureTransition.to();
                return false;
            });
        }

        jQuery("#tpk-filters").modal({"show": false});
    });

    return register;
});
