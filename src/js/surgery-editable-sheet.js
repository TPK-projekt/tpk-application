/* eslint-env browser, amd, es6 */
/* global shortcut:false */
require(
    [
        "StateTransition",
        "History",
        "jquery",
        "behavior",
        "EventBus",
        "Notifyer",
        "Upload",
        "MapInterface"
    ],
    (
        StateTransition,
        history,
        jQuery,
        behavior,
        EventBus,
        notifyer,
        Upload,
        MapInterface
    ) => {
        "use strict";

        const ARRAY_DELIM = "$services.tpkCompare.ARRAY_DELIM";
        const isDefined = (x) => typeof x !== "undefined";
        const isUndefined = (x) => !isDefined(x);
        const isNothing = (x) => isUndefined(x) || x === null;
        const isEmpty = (x) => isNothing(x) || x === "";
        const isJust = (x) => !isNothing(x);
        const isFunc = (x) => typeof x === "function";
        const ID = (x) => x;

        window.isDefined = isDefined;
        window.isUndefined = isUndefined;
        window.isNothing = isNothing;
        window.isJust = isJust;
        window.isEmpty = isEmpty;
        window.isFunc = isFunc;
        window.ID = ID;
        window.ARRAY_DELIM = ARRAY_DELIM;

        const SHORT_TIMEOUT = 100;
        const MAX_FILE_SIZE = 1000000;
        const editMode = XWiki.contextaction === "edit" ||
            XWiki.contextaction === "inline";
        const mapInterface =
            new MapInterface({"container_id": "tvpris-edit-map"});

        history.preserveParameters("minify");

        jQuery(document).ready(EventBus.triggerInit.bind(
            EventBus,
            "application-init"
        ));

        let surgeryInfo = jQuery("#tvpris-edit-map").
            attr("data-surgery-info");
        if (surgeryInfo) {
            surgeryInfo = JSON.parse(surgeryInfo);
            if (editMode) {
                surgeryInfo.edit_callback = (surgery) => {
                    const latIn = jQuery("#tvpris-surgery-latitude input");
                    const lonIn = jQuery("#tvpris-surgery-longitude input");
                    latIn.val(surgery.data.lat);
                    latIn.trigger("change");
                    lonIn.val(surgery.data.lon);
                    lonIn.trigger("change");
                };
            }
            mapInterface.addSurgery(surgeryInfo);
            mapInterface.show();
        }

        const initFileUploader = () => {
            new Upload("image", {"uploadInput": jQuery("#surgery-file-input")});
        };

        const checkboxObserver = (element, check, uncheck) => {
            const e = jQuery(element);
            const flag = e.find("input");
            if (flag.size() > 0) {
                const f = flag.get(0);
                const t = () => {
                    if (f.checked) {
                        check();
                    } else {
                        uncheck();
                    }
                };

                t();
                flag.change(t);
            }
        };

        const initEditableFlags = () => {
            jQuery(".editable-flag").each((index, element) => {
                const e = jQuery(element);
                if (element.id === "warranty-editable-flag") {
                    checkboxObserver(
                        element,
                        () => {
                            e.addClass("checked");
                            jQuery("#warranty-description").addClass("has-warranty");
                        },
                        () => {
                            e.removeClass("checked");
                            jQuery("#warranty-description").removeClass("has-warranty");
                        }
                    );
                } else {
                    checkboxObserver(element, () => e.addClass("checked"), () => e.removeClass("checked"));
                }
            });
        };

        jQuery("dd[data-placeholder]").each((index, element) => {
            const ph = element.getAttribute("data-placeholder");
            jQuery(element).find("input").attr("placeholder", ph);
        });

        const allShortcuts = shortcut.all_shortcuts();
        for (let i = 0; i < allShortcuts.length; i += 1) {
            const keys = allShortcuts[i].keys.join(" ");
            if (keys !== "alt shift s") {
                shortcut.remove(keys);
            }
        }

        const nochanges = () => jQuery(".surgery-action-buttons").addClass("nochanges");

        jQuery(".surgery-action-buttons").addClass("nochanges");
        document.observe("xwiki:document:saved", nochanges);

        initEditableFlags();
        initFileUploader();

        const packageMeasureTransition = new StateTransition(() => {
            EventBus.trigger("selecting-measures");
            jQuery("#package-selection-section, #surgery-package-prices").hide();
            jQuery("#item-selection-section, #tpk-surgery-measure-prices").show();
            jQuery("#tpk-measure-package-select").addClass("measures");
            jQuery("#tpk-select-packages").parent().addClass("active");
            jQuery("#tpk-select-measures").parent().removeClass("active");
        }, () => {
            EventBus.trigger("selecting-packages");
            jQuery("#package-selection-section, #surgery-package-prices").show();
            jQuery("#item-selection-section, #tpk-surgery-measure-prices").hide();
            jQuery("#tpk-select-measures").parent().addClass("active");
            jQuery("#tpk-select-packages").parent().removeClass("active");
            jQuery("#tpk-measure-package-select").removeClass("measures");
        });
        const pmHistory = packageMeasureTransition.history(true);
        pmHistory.name = "pm";
        history.register(pmHistory);
        history.addPushEvents("selecting-measures", "selecting-packages");
        jQuery("#tpk-select-packages").click(() => {
            packageMeasureTransition.from();
            return false;
        });
        jQuery("#tpk-select-measures").click(() => {
            packageMeasureTransition.to();
            return false;
        });

        jQuery("#use-company-name-checkbox input").each((_index, element) => {
            const toggle = () => {
                if (element.checked) {
                    jQuery("#company-name").addClass("used");
                } else {
                    jQuery("#company-name").removeClass("used");
                }
            };
            toggle();
            jQuery(element).click(toggle);
        });

        history.preserveParameters("minify");
        history.init();

        jQuery("input[type=\"text\"]").each((_, e) => {
            if (e.id.endsWith("_zipcode")) {
                const reformat = () => {
                    if (e.validity.valid) {
                        const group1length = 3;
                        const s = e.value.replace(/\s/g, "");
                        e.value = s.substring(0, group1length) + " " + s.substring(group1length, group1length + 2);
                    }
                };
                jQuery(e).parents("form").on("submit", reformat);
                e.addEventListener("blur", reformat);
            }
        });

    }
);
