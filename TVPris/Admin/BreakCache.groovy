{{groovy}}

import java.util.regex.Pattern
import java.util.regex.Matcher
import org.xwiki.velocity.tools.JSONTool
import com.xpn.xwiki.internal.cache.rendering.RenderingCache

final String [] SKIN_DOCUMENTS = ["TVPris.Skin.EmptyViewSkin", "TVPris.Skin.DefaultSkin"]
final boolean plainMode = request.get('outputSyntax') == 'plain' && request.get('xpage') == 'plain'
response.setHeader('Cache-Control', 'no-cache')

Long parseLong(String x) {
  try {
    return Long.parseLong(x)
  } catch (NumberFormatException e) {
    return null
  }
}

if (plainMode) {
  response.setContentType("application/json")
}
final Map retval = new TreeMap();
if (services.csrf.getToken() != request.get('form_token')) {
    retval.put('error', 'CSRF validation failure')
    response.setStatus(403)
} else if (request.get('cache_version') == null) {
    retval.put('error', 'Missing parameter cache_version.')
    response.setStatus(400)
} else if (parseLong(request.get('cache_version')) == null) {
    retval.put('error', 'Parameter cache_version must be numerical.')
    response.setStatus(400)
} else {
    final String PATTERN = "\\\$xcontext\\.put\\(\\s*'tpk_cache_version'\\s*,\\s*(\\d+)\\s*\\)"
    final reqver = parseLong(request.get('cache_version'))
    for (skindocname in SKIN_DOCUMENTS) {
	final skindoc = xwiki.getDocument(skindocname)
	final startpage = skindoc.getObject('XWiki.XWikiSkinFileOverrideClass', 'path', 'startpage.vm')
	final pattern = Pattern.compile(PATTERN)
	final content = startpage.getProperty('content').getValue()
	final matcher = pattern.matcher(content)

	if (!matcher.find()) {
	    retval.put('error', "startpage.vm doesn't containe cache_version declaration.")
	    response.setStatus(502)
	} else {
	    final curverstr = matcher.group(1)
	    final curver = parseLong(curverstr)
	    if (curver != reqver) {
		retval.put('error', """Parameter cache_version (${reqver}) does not match current cache version (${curver}).""".toString())
		discard = response.setStatus(400)
	    } else {
		newver = curver + 1
		repl = """\\\$xcontext.put('tpk_cache_version', ${newver})""".toString()
		updated = content.replaceFirst(PATTERN,  repl)
		startpage.set('content', updated)
		skindoc.saveAsAuthor("""Updated cache version to ${newver}""".toString(), true)
		retval.put("new_cache_version", newver)
	    }
	}
    }
    services.component.getInstance(RenderingCache.class).flushWholeCache()
}
println("{{{")
print(new JSONTool().serialize(retval))
println("}}}")
{{/groovy}}
