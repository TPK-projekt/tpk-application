{{include reference="TVPris.Code.RunQuery" /}}

{{groovy}}
import org.xwiki.velocity.tools.JSONTool
import org.xwiki.model.reference.DocumentReference
import org.apache.commons.lang.exception.ExceptionUtils

import org.xwiki.rendering.block.Block
import org.xwiki.rendering.block.EmptyLinesBlock
import org.xwiki.rendering.block.SpaceBlock
import org.xwiki.rendering.block.NewLineBlock
import org.xwiki.rendering.block.ParagraphBlock
import org.xwiki.rendering.block.match.BlockMatcher

import groovy.transform.Field

def plainMode = request.get('outputSyntax') == 'plain' && request.get('xpage') == 'plain'
def debug = false

OVERALL_LEVEL = 0;
AREA_LEVEL = 1;
ITEM_LEVEL = 2;

TOPLEVEL_COMMENT = 'tpk_toplevel_comment'

def standardValidator = { xdom ->
  lengthValidator(xdom, 2048);
};

validators = [
  standardValidator,
  standardValidator,
  standardValidator
  ]

@Field
def commentDoc = null

String getStringParam(String name) {
    String s = request.getParameter(name)
    if (s == null) {
        throw new IllegalArgumentException("Missing parameter: '" + name + "'")
    }
    return s;
}

int getIntParam(String name) {
    String s = request.getParameter(name)
    if (s == null) {
        throw new IllegalArgumentException("Missing parameter: '" + name + "'")
    }
    try {
      return Integer.parseInt(s)
    } catch (Exception e) {
      throw new IllegalArgumentException("Not integer value of parameter: '" + name + "', value was '" + s + "'")
    }
}

Long getVersionParam() {
    String s = request.getParameter('v');
    if (s == null || s == '') {
	throw new IllegalArgumentException("Version parameter is mandatory!")
    }
    return Long.parseLong(s)
}

void handleGet(final Map retVal, final ctx) {
    if (commentDoc.isNew()) {
      retVal.put('rendered_text', '')
      retVal.put('comment_text', '')
      retVal.put('include_additional', true)
      return
    }

    def obj = ctx.getObj()
    String text = ''
    boolean include_additional = true;
    if (obj != null) {
        def prop = obj.getProperty('text')
        if (prop != null) {
            text = prop.getValue()
        }
	prop = obj.getProperty(ctx.includePropName)
	if (prop != null) {
	  include_additional = prop.getValue();
	}
    }
    retVal.put('rendered_text', getRenderedText(ctx, include_additional))
    retVal.put('comment_text', toHtml(text))
    retVal.put('include_additional', include_additional)
}

String toHtml(final String text) {
    def xdom = services.rendering.parse(text, 'xwiki/2.1')
    return services.rendering.render(xdom, 'xhtml/1.0')
}

void lengthValidator(xdom, int maxLength) {
  int l = services.rendering.render(xdom, 'plain/1.0').length()
  if (l > maxLength) {
    throw new Exception("Kommenteare är för lång, längden är " + l + " tecken, men maximalt tillåtna är " + maxLength + " tecken")
  }
}

void whitelistValidator(xdom, Set whiteList) {
}

boolean onlySpace(xdom) {
  return null == xdom.getFirstBlock(
    new BlockMatcher() {
      @Override
      public boolean match(Block block) {
	if (block instanceof EmptyLinesBlock) {
	  return false
	}
	if (block instanceof SpaceBlock) {
	  return false
	}
	if (block instanceof NewLineBlock) {
	  return false
	}
	if (block instanceof ParagraphBlock) {
	  return false
	}
	return true
      }
    }, Block.Axes.DESCENDANT
  )
}

String validateAndConvertFromHtml(final String text, final int level) {
  text_ = text.replace('&nbsp;', '&#160;')
  def xdom = services.rendering.parse(text_, 'xhtml/1.0')
  validators[level](xdom)
  if (onlySpace(xdom)) {
    return null
  }
  return services.rendering.render(xdom, 'xwiki/2.1')
}

dependentPriceComments =
"""
SELECT tvpi_code, tvppc_is_measure FROM tvpris_item AS item
 LEFT OUTER JOIN tvpris_measure AS measure USING (xwo_id)
 LEFT OUTER JOIN tvpris_package AS package USING (xwo_id)
 JOIN tvpris_publicpricecomment AS ppc ON tvpi_version = tvppc_version
                                      AND is_measure = (tvppc_is_measure != 0)
                                      AND tvpi_code = tvppc_item_id
WHERE
   tvppc_surgery_id = ? AND
   is_measure = ? AND
   tvpi_version = ? AND
   CASE WHEN ?
        THEN tvpm_area_special
        ELSE tvpi_area
        END = ?
"""

String rerenderDependent(ctx, retval)
{

    def obj = ctx.getObj()
    def ia = obj == null ? true : _pv(obj, ctx.includePropName)
    boolean includeAdditional =  ia == null || ia
    System.out.println("INCLUDE PROP NAME: " +  ctx.includePropName + " level " + ctx.level + " includeAdditinal " +  includeAdditional + " ia " + ia)
    String renderedText = getRenderedText(ctx, includeAdditional)
    System.out.println("INCLUDE ADDITIONAL: " + includeAdditional + " RENDERED TEXT "  + renderedText)
    if (obj != null) {
      def deps = retval.get('deps')
      if (deps == null) {
	deps = new LinkedList();
	retval.put('deps', deps)
      }
      deps.add(
	[
	 'level': ctx.level,
	 'isMeasure': ctx.isMeasure,
	 'itemId': ctx.itemId,
	 'version': ctx.version,
	 'rendered_text': renderedText
	]
      )
      obj.set('rendered_text', renderedText)
    }

    System.out.println("rerenderDependent" + ctx)

    switch (ctx.level) {
    case 0:
      for (o in ctx.script.commentDoc.getObjects('TVPris.Classes.AreaCommentClass')) {
	
	if (_pv(o, 'area') == TOPLEVEL_COMMENT) {
	  continue;
	}
	def ctx0 = new Context(ctx.script, ctx.surgeryId, "" + _pv(o, 'area'), 0, 1, ctx.version, ctx.isPublic, ctx.isSpecial)
	rerenderDependent(ctx0, retval)
	def ctx1 = new Context(ctx.script, ctx.surgeryId, "" + _pv(o, 'area'), 1, 1, ctx.version, ctx.isPublic, ctx.isSpecial)
	rerenderDependent(ctx1, retval)
      }
      break;
    case 1:
      final QueryRunner qr = new QueryRunner('native', ctx.script.xwiki, ctx.script.xcontext)
      qr.reset()
      qr.addParameterSetter(ctx.surgeryId)
      qr.addParameterSetter(ctx.isMeasure != 0)
      qr.addParameterSetter(ctx.version)
      qr.addParameterSetter(ctx.isSpecial)
      qr.addParameterSetter(ctx.itemId)
      for (row in qr.runQuery(dependentPriceComments, null, null)) {
	System.out.println('row: ' + row)
	def ctx0 = new Context(ctx.script, ctx.surgeryId, "" + row[0], row[1], 2, ctx.version, ctx.isPublic, ctx.isSpecial)
	rerenderDependent(ctx0, retval)
      }
      break;
    }


    return renderedText
}

String getRenderedText(ctx, boolean includeAdditional) {
  String renderedText = _getRenderedText(ctx, includeAdditional)
  def xdom = services.rendering.parse(renderedText, 'xwiki/2.1')
  return services.rendering.render(xdom, 'xhtml/1.0')
}

def _pv(obj, propertyName) {
  if (obj == null) {
    return null
  }
  def v = obj.getProperty(propertyName)
  if (v == null) {
    return null
  }
  return v.getValue()
}

String _getRenderedText(ctx, boolean includeAdditional) {

  System.out.println("_getRenderedText " + ctx + " includeAdditional: " + includeAdditional)
  String prefix = ''

  def obj = ctx.getObj()
  
  if (ctx.level > 0 && includeAdditional) {

    Context parentCtx = new Context(
      this,
      ctx.surgeryId,
      ctx.parentId(),
      ctx.isMeasure,
      ctx.level - 1,
      ctx.version,
      ctx.isPublic,
      ctx.isSpecial
    )

    if (parentCtx.getObj() == null) {
      prefix = _getRenderedText(parentCtx, true)
      System.out.println("Parent ctx obj null " + ctx.level + " prefix " + prefix)
    } else {
      def pia = parentCtx.getObj().getProperty(parentCtx.includePropName)
      def id = parentCtx.getObj().getProperty(parentCtx.idPropName)
      prefix = _getRenderedText(parentCtx, pia == null || pia.getValue())
      System.out.println("Parent ctx obj NOT null id: " + id.getValue() +  " parent id: " + (ctx.level == 1 ? TOPLEVEL_COMMENT  : _pv(obj, 'area')) + " pia " + (pia == null ? null : pia.getValue())  + " prefix " + prefix)
    }
  }

  if (obj == null) {
    System.out.println("RETURN obj == ull " + prefix)
    return prefix
  }

  def text = _pv(obj, 'text')

  System.out.println('Getting text of obj ' + obj.getXWikiObject() + ' text: ' + text + " ctx: " + ctx)

  if (text == null) {
    text = ''
  }

  System.out.println("RETURN obj != ull " + (prefix + (prefix.length() > 0 && text.length() > 0 ? "\n\n" : "") + text))

  return prefix + (prefix.length() > 0 && text.length() > 0 ? "\n\n" : "") + text
}

void handlePost(final Map retVal, final ctx) {
    if (services.csrf.getToken() != request.get('form_token')) {
        retVal.put('error', 'CSRF validation failure')
        response.setStatus(response.SC_FORBIDDEN)
        return
    }
    Object obj = ctx.getObj()
    final String text = request.getParameter('text')

    final String ia = request.getParameter('ia')
    Integer includeAdditional = null
    if (ia != null) {
      includeAdditional = ia.equalsIgnoreCase("false") ? 0 : 1
    }

    if (obj == null) {
        def nb = commentDoc.createNewObject(ctx.className)
        obj = commentDoc.getObject(ctx.className, nb)
        obj.set('is_measure', ctx.isMeasure)
        obj.set(ctx.idPropName, ctx.itemId)
    }

    if (includeAdditional != null) {
      System.out.println('Setting ' + ctx.includePropName + ' to  ' + includeAdditional + ' obj ' + obj.getXWikiObject() + " ctx: " + ctx)
      obj.set(ctx.includePropName, includeAdditional)
    }
    obj.set('version', ctx.version)

    def iap = obj.getProperty(ctx.includePropName)
    def inca = iap != null ? iap.getValue() == 1 : true

    if (text != null) {
      String resulttext = validateAndConvertFromHtml(text, ctx.level)
      if (resulttext == null) {
	if (inca) {
	  handleDelete(retVal, ctx)
	  return
	} else {
	  resulttext = ''
	}
      }
      System.out.println("Setting text of obj " + obj + " to text " + text + " ctx " + ctx)
      obj.set('text', resulttext)
      retVal.put('comment_text', toHtml(resulttext))
    }

    String renderedText =  rerenderDependent(ctx, retVal);

    commentDoc.save('Updated comment.')

    System.out.println('inca: ' + inca)

    retVal.put('rendered_text', renderedText)
    retVal.put('include_additional', inca)
}

void handleDelete(final Map retVal, final ctx) {
    if (commentDoc.isNew()) {
        return
    }
    Object obj = ctx.getObj()
    if (obj == null) {
        return
    }
    commentDoc.removeObject(obj)
    commentDoc.save('Removed comment')
}

final Map retVal = new HashMap<String, Object>()

class Context {

  final int surgeryId

  final String itemId

  final int isMeasure

  final int level

  final Long version

  final boolean isPublic

  final boolean isSpecial
    
  final DocumentReference docRef

  final String className

  final String includePropName

  final String idPropName

  final def script

  private def object

  public Context(script, int surgeryId, String itemId, int isMeasure, int level, Long version, boolean isPublic, boolean isSpecial)
  {
    this.script = script
    this.surgeryId = surgeryId
    this.itemId = itemId
    this.isMeasure = isMeasure
    this.level = level
    this.version = version
    this.isPublic = isPublic
    this.isSpecial = isSpecial
    
    className = 'TVPris.Classes.' + (level == 2 ?
      (isPublic ? 'PublicPriceCommentClass' : 'InternalPriceCommentClass') :
						  'AreaCommentClass')

    includePropName = level == 2 ?  'include_area_comment' : 'include_overall_comment'
    idPropName = level == 2 ? 'item_id' : 'area'
  }

  public def getObj() {
    if (this.object != null) {
      return this.object
    }
    for (o0 in script.commentDoc.getObjects(className, idPropName, itemId)) {
      def prop = o0.getProperty('is_measure')
      def ver = o0.getProperty('version')
      boolean a = prop != null ? prop.getValue() != 0 : false
      boolean b = isMeasure != 0
      boolean v = ver != null && ver.getValue() == version
      if ( v && (level == 0 || a == b) ) {
	this.object = o0;
	return o0
      }
    }

    return null
  }

  public def parentId() {
    if (this.level == 1) {
      return "tpk_toplevel_comment";
    }
    if (this.level == 2) {
      int code = Integer.parseInt(itemId)
      def q
      def qr = new QueryRunner('native', script.xwiki, script.xcontext)
      qr.addParameterSetter(isSpecial)
      qr.addParameterSetter(version)
      qr.addParameterSetter(code)
      qr.addParameterSetter(isMeasure != 0)
      def rows = qr.runQuery("""
SELECT
       CASE WHEN ? THEN
          tvpm_area_special
       ELSE
          tvpi_area
       END
FROM tvpris_item LEFT OUTER JOIN tvpris_measure USING (xwo_id)
                LEFT OUTER JOIN tvpris_package USING (xwo_id)
WHERE tvpi_version = ? AND tvpi_code = ? AND is_measure = ?
                   """, null, null)
      for (area in rows) {
	return area
      }
		  
    }
    return null;
  }

  @Override
  public String toString() {
    return "Context level " + level + " docRef: " + docRef + " className:  " + className + " commentDoc: " + script.commentDoc + " version: " + version + " isMeasure: " + isMeasure + " isPublic: " + isPublic + " itemId: " + itemId
  }
  


}

try {
    def boolParam = { p -> ['yes', '1', 'true'].any({ v ->  v.equalsIgnoreCase(request.getParameter(p)) }) }
  
    final def ctx = new Context(this,
				getIntParam('sid'),
				getStringParam('iid'),
				getIntParam('ism'),
				getIntParam('l'),
				getVersionParam(),
				boolParam('public'),
				boolParam('special'))



    docRef = services.model.resolveDocument(services.tpkCompare.SURGERY_EDIT_SPACE + '.' + ctx.surgeryId + (ctx.isPublic ? '.PublicComments' : '.InternalComments'))
    commentDoc = xwiki.getDocument(docRef)

    retVal.put('iid', ctx.itemId)
    retVal.put('ism', ctx.isMeasure)
    retVal.put('version', ctx.version)
    retVal.put('public', ctx.isPublic)
    if (request.getMethod() == 'GET') {
        handleGet(retVal, ctx)
    } else if (request.getMethod() == 'POST') {
        handlePost(retVal, ctx)
    } else if (request.getMethod() == 'DELETE') {
        handleDelete(retVal, ctx)
    } else {
        throw new RuntimeException('Unsupported method: ' + request.getMethod())
    }
} catch (Exception e) {
    retVal.put('error', e.getMessage());
    retVal.put('exception', services.localization.render('tvpris.surgerycomment_caught_exception', [e.getMessage() + ExceptionUtils.getFullStackTrace(e)]))
    response.setStatus(response.SC_INTERNAL_SERVER_ERROR)
}

if (plainMode) {
  response.setContentType("application/json")
}

println('{{{')
println(new JSONTool().serialize(retVal))
println('}}}')

{{/groovy}}
