{{include reference="TVPris.Code.RunQuery"/}}{{groovy}}
import org.xwiki.velocity.VelocityManager
VelocityManager velocityManager = services.component.getInstance(VelocityManager.class)
def vcontext = velocityManager.getVelocityContext()

class RegionQuery {

  private final def xwiki

  private final def xcontext

  public RegionQuery(xwiki, xcontext) {
    this.xwiki = xwiki
    this.xcontext = xcontext
  }

  public Object doQuery(String regionType) {
    def queryRunner = new QueryRunner('native', xwiki, xcontext)

    String queryString = "SELECT borders.name, borders.parent_name FROM gis.region_borders AS borders JOIN gis.region_types as types ON type = types.id WHERE types.name = ?"
    queryRunner.addParameterSetter(regionType)

    def result = queryRunner.runQuery(queryString, null, null)

    return result
  }
}

vcontext.put('regionQuery', new RegionQuery(xwiki, xcontext))
{{/groovy}}
