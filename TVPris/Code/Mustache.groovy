{{groovy}}
import com.github.mustachejava.DefaultMustacheFactory
import com.github.mustachejava.MustacheFactory
import com.github.mustachejava.Mustache

import org.xwiki.model.reference.DocumentReference

class MustacheCompiler {
  
  private static MustacheFactory mustacheFactory = new DefaultMustacheFactory()

  private final def xwiki

  public MustacheCompiler(xwiki) {
    this.xwiki = xwiki
  }

  public Mustache compileTemplate(DocumentReference docRef) {
    def doc = xwiki.getDocument(docRef)
    Reader reader = new StringReader("{{=_( )_=}}" + doc.getRenderedContent())
    return mustacheFactory.compile(reader, docRef.toString())
  }

  public Mustache compileTemplate(String template, String name) {
    Reader reader = new StringReader("{{=_( )_=}}" + template);
    return mustacheFactory.compile(reader, name);
  }
}

mustacheCompiler = new MustacheCompiler();

{{/groovy}}
