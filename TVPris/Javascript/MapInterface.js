define([
    "jquery",
    "EventBus",
    "Notifyer"
], function(jQuery, EventBus, notifyer) {

    var eb = jQuery(EventBus);
    
    // Sweden's centre point
    var defaultCenter = { lat: 59.6749712, lon: 14.5208584 };

    function Surgery(data) {
	this.data = data;
	this.setMap(null);
	this.highlighted = false;
	this.marker = null;
	this.surgeryInfo = null;
	this.openingWindow = false;
    }

    var _sp = Surgery.prototype;

    _sp.setMap = function(map) {
	this.map = map;
	if (map != null && map.mapInitiated) {
	    this.addMarker();
	}
    }

    _sp.markerOpts = function () {
        icon = "$xwiki.getAttachmentURL('TVPris.Icons', 'map_marker.png')";
	    var options = {
	        lat: this.data.lat,
	        lon: this.data.lon,
	        icon: icon
	    };
	    if (typeof(self.data.edit_callback) === 'function') {
	        options.edit_callback = function (pos) {
		        self.data.lat = pos.lat;
		        self.data.lon = pos.lon;
		        self.data.edit_callback(self);
	        }
	    } else {
	        options.click = function() {
		        eb.trigger('surgery-highlighted', [self.data.s_id]);
	        }
	    }
        return options;
    }

    _sp.addMarker = function() {
	    if (this.data.lat === null || this.data.lon === null) {
	        return;
	    }
	    var icon
	    var self = this;
	    this.marker = this.map.createMarker(this.markerOpts());
    }

    _sp.setHighlighted = function(highlighted) {
	console.log('setHighlighted ' + this.data.s_id + ' ' + highlighted);
	if (!!this.highlighted !== !!highlighted) {
        this.marker.setMap(null);
        var options = this.markerOpts();
	    if (!!highlighted) {
            options.scale = 1.6;
	    }
        this.marker = this.map.createMarker(options);
	    this.highlighted = !!highlighted;
	}
    }
    
    _sp.openInfoWindow = function() {
	var self = this;
	if (self.openingWindow) {
	    return;
	}
	self.openingWindow = true;
	if (this.surgeryInfo === null) {
	    var hide = notifyer.progress("$services.localization.render('tvpris.fetch_surgery_info')");

	    jQuery.ajax({
		url: "$xwiki.getURL('TVPris.View.Surgery')",
		data: {
		    xpage: 'plain',
		    sid: self.data.s_id
		},
		dataType: 'html'
	    }).done(function(html) {
		self.surgeryInfo = html;
		self._openInfoWindow()
	    }).fail(function (jqXHR, textStatus, errorThrown ) {
            notifyer.error("$services.localization.render('tvpris.failed_to_fetch_surgery_info'): " + textStatus + ' ' + errorThrown);
		});
	    }).always(function ()  {
		    self.openingWindow = false;
            hide();
        });
	} else {
	    self.openingWindow = false;
	    self._openInfoWindow();
	}
    }

    _sp._openInfoWindow = function () {
	this.infoWindow = this.map.openInfoWindow({
	    content: this.surgeryInfo,
	    anchor: this.marker,
	    surgery: this.data
	});
    }

    _sp.closeInfoWindow = function() {
	if (this.infoWindow !== null) {
	    this.map.closeInfoWindow(this.infoWindow);
	}
	this.infoWindow = null;
    }

    _sp.remove = function () {
	if (this.map !== null && this.marker !== null) {
	    this.map.removeMarker(this.marker);
	}
    }

    function MapInterface(options) {
	var self = this;
	this.surgeries = {};
	this.eventQueue = [];
	self.mapInitiated = false;
	if (typeof(options) === 'object') {
	    this.dirty = false;
	    this.container_id = options.container_id;
	    console.log('MapInterface add listeners');
	    eb.on('surgery-highlighted', function(event, surgeryId) {
		var s = self.surgeries[surgeryId];
		if (s) {
		    s.setHighlighted(true);
		}
	    });
	    eb.on('surgery-unhighlighted', function(event, surgeryId) {
		var s = self.surgeries[surgeryId];
		if (s) {
		    s.setHighlighted(false);
		}
	    });
	    eb.on('surgeries-added', function(event, surgeries) {
		self.addSurgeries(surgeries);
	    });
	    eb.on('surgeries-cleared', function() {
		self.clearSurgeries();
	    });
	}
    }

    var _mp = MapInterface.prototype;

    _mp.Surgery = Surgery;

    _mp.addSurgery = function(surgeryData) {
	this.touch();
	var surgeryId = surgeryData.s_id;
	if (this.surgeries.hasOwnProperty(surgeryId)) {
	    this.removeSurgery(surgeryId);
	}
	var s = new Surgery(surgeryData);
	this.surgeries[surgeryId] = s;
	s.setMap(this);
    }

    _mp.removeSurgery = function(surgeryId) {
	this.touch();
	var s = this.surgeries[surgeryId];
	s.remove();
	delete this.surgeries[surgeryId];
    }

    _mp.clearSurgeries = function() {
	var self = this;
	jQuery.each(self.surgeries, function (surgeryId) {
	    self.removeSurgery(surgeryId);
	});
    }

    _mp.fitSurgeries = function() {
	var ps = [];
	var self = this;
	jQuery.each(self.surgeries, function(surgeryId) {
	    var s = self.surgeries[surgeryId];
	    if (s.data.lon !== null && s.data.lat !== null) {
		ps.push({
		    lon: s.data.lon,
		    lat: s.data.lat
		});
	    }
	});
	this.fitMarkers(ps);
    }

    function GoogleMap(options) {
	MapInterface.call(this, options);
	this.google_map = null;
    }

    _mp.setSurgeries = function(surgeries) {
	this.clearSurgeries();
	this.addSurgeries(surgeries);
    }

    _mp.addSurgeries = function(surgeries) {
	for (var i = 0; i < surgeries.length ; i++) {
	    this.addSurgery(surgeries[i]);
	}
	this.fitSurgeries();
    }

    _mp.touch = function () {
	this.dirty = true;
    }

    _mp.initiateSurgeries = function (map) {
	jQuery.each(this.surgeries, function(surgeryId, surgery) {
	    surgery.addMarker();
	});
	this.fitSurgeries();
    }

    _mp.hide = function() {
	if (this.element) {
	    jQuery(this.element).hide();
	}
	this.hidden = true;
    }

    var _gmp = GoogleMap.prototype = new MapInterface();
    
    _gmp.show = function () {
	    var self = this;
		this.element = document.getElementById(this.container_id);
		jQuery(this.element).show();
	    if (this.google_map === null) {
	        require(['/maps-js?libraries=drawing'], function () {
		        self.mapInitiated = true;
		        self.google = google;
		        self.google_map = new this.google.maps.Map(self.element, {
		            center: {lat: defaultCenter.lat, lng: defaultCenter.lon},
		            zoom: 4
		        });
		        self.initiateSurgeries(self);
		        this.hidden = false;
		        self.runEventQueue();
	        });
	    } else {
	        this.hidden = false;
	        this.runEventQueue();
	    }
    }

    _gmp.runEventQueue = function () {
	while (this.eventQueue.length > 0) {
	    var f = this.eventQueue.shift();
	    f();
	}
	this.hidden = false;
	jQuery(self.element).show();	    
    }

    _gmp.createMarker = function (options) {
	    var isEdit = typeof(options.edit_callback) === 'function';
        var scale = typeof options.scale === "undefined" ? 1 : options.scale;
	var goptions = {
	    position: {
		lat: options.lat,
		lng: options.lon
	    },
	    cursor: 'pointer',
	    map: this.google_map, 
	    icon: {
		size: new this.google.maps.Size(59, 79),
		scaledSize: new this.google.maps.Size(18 * scale, 24 * scale),
		origin: new this.google.maps.Point(0, 0),
		anchor: new this.google.maps.Point(6 * scale, 24 * scale),
		url: options.icon
	    },
	    anchorPoint: new this.google.maps.Point(0, -24 * scale),
	};
	if (isEdit) {
	    goptions.draggable = true;
	} else {
	    goptions.clickable = true;
	}
	var marker = new this.google.maps.Marker(goptions);
	console.log("Marker anchor point: " + JSON.stringify(marker.anchorPoint));
	if (isEdit) {
	    marker.addListener('dragend', function () {
		new_position = marker.getPosition();
		options.edit_callback({
		    lat: new_position.lat,
		    lon: new_position.lng
		});
	    });
	} else {
	    marker.addListener('click', options.click);
	}
	return marker;
    }

    _gmp.fitMarkers = function(ps) {
	var self = this;
	if (ps.length == 0 || typeof(this.google) == 'undefined') {
	    return;
	}
	if (ps.length == 1) {
	    this.google_map.setCenter({
		lat: ps[0].lat,
		lng: ps[0].lon
	    });
	    this.google_map.setZoom(17);
	} else {
	    var bounds = new this.google.maps.LatLngBounds();
	    for (var i = 0; i < ps.length; i++) {
		bounds.extend({ lat: ps[i].lat, lng: ps[i].lon});
	    }
	    if (this.hidden) {
		this.eventQueue.push(function () {
		    self.google_map.fitBounds(bounds);
		});
	    } else {
		this.google_map.fitBounds(bounds);
	    }
	}
    }

    _gmp.removeMarker = function (marker) {
	marker.setMap(null);
    }

    _gmp.openInfoWindow = function (options) {
	var window = new this.google.maps.InfoWindow({
	    content: options.content
	});
	window.open(this.google_map, options.anchor);
	return window;
    }

    _gmp.closeInfoWindow = function (window) {
	window.close();
    }
    
    return GoogleMap;
});
