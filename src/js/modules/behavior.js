/*
 *  Copyright (C) 2017  Andreas Jonsson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* eslint-env browser, amd, es6 */
define(["jquery"], (jQuery) => {
    "use strict";

    const MODAL_MIN_TOP_OFFSET = 30;

    const setClickedFocus = (element) => {
        element.classList.add("clicked-focus");
        let p = element.parentElement;
        while (p !== null && !p.classList.contains("focus-within")) {
            p = p.parentElement;
        }
        if (p !== null) {
            p.classList.add("clicked-focus");
        }
    };

    const clearClickedFocus = (element) => {
        element.classList.remove("clicked-focus");
        let p = element.parentElement;
        while (p !== null && !p.classList.contains("focus-within")) {
            p = p.parentElement;
        }
        if (p !== null) {
            p.classList.remove("clicked-focus");
        }
    };

    const modalPosition = (element) => {
        const target = jQuery(element).find(".modal-dialog");
        const viewPortHeight = Math.max(
            document.documentElement.clientHeight,
            window.innerHeight || 0
        );
        jQuery(element).show();
        const elementHeight = target.outerHeight();
        const desiredTop = (viewPortHeight - elementHeight) / 2;
        if (desiredTop > MODAL_MIN_TOP_OFFSET) {
            target.css("margin-top", desiredTop + "px");
        }
    };

    const clickFocusElement = (element) => {
        jQuery(element).mousedown(() => {
            setClickedFocus(element);
        });
        if (element.getAttribute("data-toggle") === "modal") {
            const target = isJust(element.getAttribute("href")) ? element.getAttribute("href") : element.getAttribute("data-target");
            let wasClickedFocused = false;
            try {
                jQuery(target).on("show.bs.modal", () => {
                    wasClickedFocused = element.classList.contains("clicked-focus");
                    modalPosition(target);
                    jQuery(window).on("resize.tpk-behavior", () => modalPosition(target));
                });
                jQuery(target).on("hidden.bs.modal", () => {
                    if (wasClickedFocused) {
                        setClickedFocus(element);
                    }
                    jQuery(window).off("resize.tpk-behavior");
                });
            } catch (e) {
                // Empty.
            }
        }
        jQuery(element).focusout(() => clearClickedFocus(element));
    };
    const behavior = {
        "accordion": (collapses) => {
            collapses.each((index0, element0) => {
                jQuery(element0).on("show.bs.collapse", () => {
                    collapses.each((index1, element1) => {
                        if (index0 !== index1) {
                            jQuery(element1).collapse("hide");
                        }
                    });
                });
            });
        },
        "clickFocusElement": clickFocusElement,
        "initInputClickedFocus": () => {
            jQuery("input, a, .focus-within, button:not(.not-clickfocus), textarea, select").
                each((index, element) => clickFocusElement(element));
        }
    };
    return behavior;
});
