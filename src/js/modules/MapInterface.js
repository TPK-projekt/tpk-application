/* eslint-env browser, amd, es6 */
/* global google:false */
define([
    "jquery",
    "EventBus"
], (jQuery, EventBus) => {
    "use strict";

    const eb = jQuery(EventBus);

    // Sweden's centre point
    const defaultCenter = {
        "lat": 59.6749712,
        "lon": 14.5208584
    };

    function Surgery (data) {
        this.data = data;
        this.setMap(null);
        this.highlighted = false;
        this.marker = null;
        this.surgeryInfo = null;
        this.openingWindow = false;
    }

    const _sp = Surgery.prototype;
    const DELAY = 100;

    _sp.setMap = function setMap (map) {
        this.map = map;
        if (map !== null && map.mapInitiated) {
            this.addMarker();
        }
    };

    _sp.markerOpts = function markerOpts () {
        const icon = '$xwiki.getAttachmentURL("TVPris.Icons", "map_marker.png")';
        const options = {
            "lat": this.data.lat,
            "lon": this.data.lon,
            "icon": icon
        };
        if (typeof this.data.edit_callback === "function") {
            options.edit_callback = (pos) => {
                this.data.lat = pos.lat;
                this.data.lon = pos.lon;
                this.data.edit_callback(this);
            };
        } else {
            options.click =
                () => eb.trigger("surgery-highlighted", [this.data.s_id]);
        }
        return options;
    };

    _sp.addMarker = function addMarker () {
        if (this.data.lat === null || this.data.lon === null) {
            return;
        }
        this.marker = this.map.createMarker(this.markerOpts());
    };

    _sp.setHighlighted = function setHighlighted (highlighted) {
        if (Boolean(this.highlighted) !== Boolean(highlighted) &&
            this.marker !== null) {
            this.map.removeMarker(this.marker);
            const options = this.markerOpts();
            if (highlighted) {
                options.scale = 1.6;
            }
            this.marker = this.map.createMarker(options);
            this.highlighted = Boolean(highlighted);
        }
    };

    const formatAddress = (data) => {
        const parts = [data.s_address, data.s_zipcode, data.s_city];
        const html = jQuery("<span class=\"address\"/>");
        for (let i = 0; i < parts.length; i += 1) {
            if (!isEmpty(parts[i])) {
                const line = jQuery("<span class=\"addressline\"/>");
                line.text(parts[i]);
                html.append(line);
            }
        }
        return html.html();
    };

    _sp.openInfoWindow = function openInfoWindow () {
        if (this.openingWindow) {
            return;
        }
        this.openingWindow = true;
        if (this.surgeryInfo === null) {
            const {data} = this;
            const info = jQuery("<div class=\"surgery-map-info\"/>");
            const name = jQuery("<div class=\"name\"/>");
            const link = jQuery("<a class=\"surgery-link\" />");
            link.text(data.s_name);
            link.attr("href", data.surgery_view);
            link.attr("data-target-sid", data.s_id);
            name.append(link);
            info.append(name);
            const price = jQuery("<div class=\"price\"/>");
            price.text(data.price_text);
            info.append(price);
            const address = jQuery("<div class=\"address\"/>");
            address.html(formatAddress(data));
            info.append(address);
            const wrapper = jQuery("<div/>");
            wrapper.append(info);

            this.surgeryInfo = wrapper.get(0);
            this.openingWindow = false;
            this._openInfoWindow();
        } else {
            this.openingWindow = false;
            this._openInfoWindow();
        }
    };

    _sp._openInfoWindow = function _openInfoWindow () {
        this.map.openInfoWindow({
            "content": this.surgeryInfo,
            "anchor": this.marker,
            "surgery": this.data,
            "window": (window) => {
                this.infoWindow = window;
            }
        });
    };

    _sp.closeInfoWindow = function closeInfoWindow () {
        if (this.infoWindow !== null) {
            this.map.closeInfoWindow(this.infoWindow);
        }
        this.infoWindow = null;
    };

    _sp.remove = function remove () {
        if (this.map !== null && this.marker !== null) {
            this.map.removeMarker(this.marker);
        }
    };

    function MapInterface (options) {
        this.surgeries = {};
        this.eventQueue = [];
        this.mapInitiated = false;
        if (typeof options === "object") {
            this.dirty = false;
            this.container_id = options.container_id;
            this.setTabindex =
                typeof options.setTabindex !== "undefined" ? options.setTabindex : false;
            eb.on("surgery-highlighted", (event, surgeryId) => {
                if (!this.mapInitiated) {
                    this.eventQueue.push(() => eb.trigger("surgery-highlighted", [surgeryId]));
                    return;
                }
                const s = this.surgeries[surgeryId];
                if (s) {
                    s.setHighlighted(true);
                    s.openInfoWindow(true);
                }
            });
            eb.on("surgery-unhighlighted", (event, surgeryId) => {
                const s = this.surgeries[surgeryId];
                if (s) {
                    s.setHighlighted(false);
                    s.closeInfoWindow(true);
                }
            });
            eb.on("surgeries-added", (event, surgeries) => {
                this.addSurgeries(surgeries);
            });
            eb.on("surgeries-cleared", () => {
                this.clearSurgeries();
            });
        }
    }

    const _mp = MapInterface.prototype;

    _mp.Surgery = Surgery;

    _mp.addSurgery = function addSurgery (surgeryData) {
        this.touch();
        const surgeryId = surgeryData.s_id;
        if (Object.prototype.hasOwnProperty.call(this.surgeries, surgeryId)) {
            this.removeSurgery(surgeryId);
        }
        const s = new Surgery(surgeryData);
        this.surgeries[surgeryId] = s;
        s.setMap(this);
    };

    _mp.removeSurgery = function removeSurgery (surgeryId) {
        this.touch();
        const s = this.surgeries[surgeryId];
        s.remove();
        delete this.surgeries[surgeryId];
    };

    _mp.clearSurgeries = function clearSurgeries () {
        jQuery.each(this.surgeries, (surgeryId) => {
            this.removeSurgery(surgeryId);
        });
    };

    _mp.fitSurgeries = function fitSurgeries () {
        const ps = [];
        jQuery.each(this.surgeries, (surgeryId) => {
            const s = this.surgeries[surgeryId];
            if (s.data.lon !== null && s.data.lat !== null) {
                ps.push({
                    "lon": s.data.lon,
                    "lat": s.data.lat
                });
            }
        });
        this.fitMarkers(ps);
    };

    function GoogleMap (options) {
        MapInterface.call(this, options);
        this.google_map = null;
    }

    _mp.setSurgeries = function setSurgeries (surgeries) {
        this.clearSurgeries();
        this.addSurgeries(surgeries);
    };

    _mp.addSurgeries = function addSurgeries (surgeries) {
        for (let i = 0; i < surgeries.length; i += 1) {
            this.addSurgery(surgeries[i]);
        }
        this.fitSurgeries();
    };

    _mp.touch = function touch () {
        this.dirty = true;
    };

    _mp.initiateSurgeries = function initiateSurgeries (_map) {
        jQuery.each(this.surgeries, (surgeryId, surgery) => {
            surgery.addMarker();
        });
        this.fitSurgeries();
    };

    _mp.hide = function hide () {
        if (this.element) {
            jQuery(this.element).hide();
        }
        this.hidden = true;
    };

    const _gmp = new MapInterface();
    GoogleMap.prototype = _gmp;

    _gmp.show = function show () {
        this.element = document.getElementById(this.container_id);
        jQuery(this.element).show();
        if (this.google_map === null) {
            require(["https://maps.googleapis.com/maps/api/js?libraries=drawing&key=$services.tpkConfig.getGoogleMapsKey()"], () => {
                this.mapInitiated = true;
                this.google = google;
                this.google_map = new this.google.maps.Map(this.element, {
                    "center": {
                        "lat": defaultCenter.lat,
                        "lng": defaultCenter.lon
                    },
                    "zoom": 4,
                    "fullscreenControl": true,
                    "streetViewControl": false,
                    "fullscreenControlOptions": {"position": google.maps.ControlPosition.RIGHT_BOTTOM},
                    "mapTypeControl": false
                });
                if (this.setTabindex) {
                    this.google_map.addListener("tilesloaded", () => setTimeout(() => {
                        jQuery(this.element).find("a, button, [tabindex]").attr("tabindex", "2");
                        jQuery(this.element).find("iframe").attr("tabindex", "-1");
                    }, DELAY));
                }
                this.initiateSurgeries(this);
                this.hidden = false;
                this.runEventQueue();
            });
        } else {
            this.hidden = false;
            this.runEventQueue();
        }
    };

    _gmp.runEventQueue = function runEventQueue () {
        while (this.eventQueue.length > 0) {
            const f = this.eventQueue.shift();
            f();
        }
        this.hidden = false;
        jQuery(this.element).show();
    };

    const ICON_SIZE_X = 59;
    const ICON_SIZE_Y = 79;
    const ICON_SCALE_X = 18;
    const ICON_SCALE_Y = 24;

    _gmp.createMarker = function createMarker (options) {
        const isEdit = typeof options.edit_callback === "function";
        const scale = typeof options.scale === "undefined" ? 1 : options.scale;
        const goptions = {
            "position": {
                "lat": options.lat,
                "lng": options.lon
            },
            "cursor": "pointer",
            "map": this.google_map,
            "icon": {
                "size": new this.google.maps.Size(ICON_SIZE_X, ICON_SIZE_Y),
                "scaledSize": new this.google.maps.Size(ICON_SCALE_X * scale, ICON_SCALE_Y * scale),
                "origin": new this.google.maps.Point(0, 0),
                "anchor": new this.google.maps.Point(ICON_SCALE_X / 2 * scale, ICON_SCALE_Y * scale),
                "url": options.icon
            },
            "anchorPoint": new this.google.maps.Point(0, -ICON_SCALE_Y * scale)
        };
        if (isEdit) {
            goptions.draggable = true;
        } else {
            goptions.clickable = true;
        }
        const marker = new this.google.maps.Marker(goptions);
        if (isEdit) {
            marker.addListener("dragend", () => {
                const new_position = marker.getPosition();
                options.edit_callback({
                    "lat": new_position.lat,
                    "lon": new_position.lng
                });
            });
        } else {
            marker.addListener("click", options.click);
        }
        return marker;
    };

    _gmp.fitMarkers = function fitMarkers (ps) {
        const DEFAULT_ZOOM = 17;
        if (ps.length === 0 || typeof this.google === "undefined") {
            return;
        }
        if (ps.length === 1) {
            this.google_map.setCenter({
                "lat": ps[0].lat,
                "lng": ps[0].lon
            });
            this.google_map.setZoom(DEFAULT_ZOOM);
        } else {
            const bounds = new this.google.maps.LatLngBounds();
            for (let i = 0; i < ps.length; i += 1) {
                bounds.extend({
                    "lat": ps[i].lat,
                    "lng": ps[i].lon
                });
            }
            if (this.hidden) {
                this.eventQueue.push(() => {
                    this.google_map.fitBounds(bounds);
                });
            } else {
                this.google_map.fitBounds(bounds);
            }
        }
    };

    _gmp.removeMarker = function removeMarker (marker) {
        marker.setMap(null);
    };

    _gmp.openInfoWindow = function openInfoWindow (options) {
        if (!this.mapInitiated) {
            this.eventQueue.push(() => this.openInfoWindow(options));
            return;
        }
        const window = new this.google.maps.
            InfoWindow({"content": options.content});
        window.open(this.google_map, options.anchor);
        jQuery(".tpk-map-surgery-link").each((_index, element) => {
            jQuery(element).click(() => {
                const sid = element.getAttribute("data-target-sid");
                if (typeof sid === "string") {
                    EventBus.trigger("surgery-highlighted", [sid]);
                }
            });
        });
        if (isJust(options.window)) {
            options.window(window);
        }
    };

    _gmp.closeInfoWindow = function closeInfoWindow (window) {
        window.close();
    };

    return GoogleMap;
});
