{{include reference="TVPris.Classes.PostedOperation" /}}{{include reference="CSVImporter.Code.CSVMapper" /}}{{include reference="TVPris.Code.RunQuery"/}}{{groovy}}

import org.xwiki.model.reference.AttachmentReference
import org.xwiki.model.reference.AttachmentReferenceResolver
import org.xwiki.model.reference.EntityReference
import org.xwiki.model.reference.SpaceReference
import org.xwiki.model.reference.DocumentReference
import org.xwiki.model.reference.DocumentReferenceResolver
import org.xwiki.model.reference.ObjectReference
import org.apache.commons.csv.CSVFormat
import java.text.NumberFormat
import groovy.transform.Field


import org.apache.http.client.utils.URIBuilder
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.HttpEntity
import org.json.JSONObject
import org.json.JSONArray
import java.net.URI
import java.util.regex.Pattern;
import java.util.regex.Matcher;

@Field
final String PACKAGES_FILENAME = 'Behandlingspaket.csv';
@Field
final String PACKAGE_PRICES_FILENAME = 'Medianpris_paket_ATV.csv';
@Field
final String MEASURE_PRICES_FILENAME = 'Medianpris_atgard_ATV_STV_addresser.csv';
@Field
final String MAPS_API_KEY = services.tpkConfig.getGoogleMapsKey();

PostedOperation [] postedOperations = [
    new PostedOperation(this, 'bump_version', { -> bumpVersion() }, 'Uppgradera version.'),
    new PostedOperation(this, 'import_packages', { -> importPackages() }, 'Importera paket och referenspriser (filen "' + PACKAGES_FILENAME + '" måste först laddas upp som bilaga till denna sida.)'),
    new PostedOperation(this, 'import_measures', { -> importMeasures() }, 'Importera åtgärder och referenspriser (filerna Åtgärder ref priser ATV-STV.csv och Terapiområden.csv   måste först laddas upp som bilaga till denna sida.)'),
    new PostedOperation(this, 'import_measure_prices', { -> importMeasurePrices() }, 'Importera åtgärdspriser (filen "' + MEASURE_PRICES_FILENAME + '" måste först laddas upp som bilaga till denna sida.)'),
    new PostedOperation(this, 'import_package_prices', { -> importPackagePrices() }, 'Importera paketpriser (filen "' + PACKAGE_PRICES_FILENAME + '"  måste först laddas upp som bilaga till denna sida.)'),
    new PostedOperation(this, 'delete_surgeries', { -> deleteSurgeries() }, 'Radera mottagningar.'),
    new PostedOperation(this, 'import_surgeries', { -> importSurgeries() }, 'Importera mottagningar.'),

    new PostedOperation(this, 'import_surgeries_from_price_data', { -> importSurgeriesFromPriceData() }, 'Importera mottagningar från prisdata.'),
    new PostedOperation(this, 'move_editable_object_and_delete_preview_object', { -> moveEditableObjectAndDeletePreviewObject() }, 'Move editable object and delete preview object.'),
    new PostedOperation(this, 'update_region_and_coordinates', { -> updateRegionAndCoordinates() }, 'Update region and coordinates.'),
    new PostedOperation(this, 'split_address', { -> splitAddress() }, 'Dela upp adress.'),
    new PostedOperation(this, 'fix_surgery_object_links', { -> fixSurgeryObjectLinks() }, 'Fixa koppling mellan mottagningsobjekt.'),
];


maybeDoPostedOperations(request, postedOperations)
outputForm(postedOperations)

@Field
final DocumentReferenceResolver drr = services.component.getInstance(DocumentReferenceResolver.class, "current")

@Field
final String appSpace = 'TVPris'
@Field
final String classSpace = appSpace + '.Classes'
@Field
final String surgeryClass = classSpace + ".SurgeryClass"
@Field
final String surgeryEditableClass = classSpace + ".SurgeryEditableClass"
@Field
final String surgeryPreviewClass = classSpace + ".SurgeryPreviewClass"
@Field
final String measurePriceClass = classSpace + ".ItemPriceClass"
@Field
packagePriceClass = classSpace + ".ItemPriceClass"
@Field
final String packageClass = classSpace + ".PackageClass"
@Field
final String packageSpace = 'Paket'
@Field
final String measureClass = classSpace + ".MeasureClass"
@Field
final String measureSpace = 'Åtgärder'
@Field
final String versionClass = classSpace + ".VersionClass"
@Field
final String versionDoc = "Version.WebHome"

AttachmentReference getAttachmentReference(filename)
{
    final AttachmentReferenceResolver arr = services.component.getInstance(AttachmentReferenceResolver.class, "current")
    return arr.resolve(filename)
}

int getVersion()
{
  def vdoc = xwiki.getDocument(versionDoc)
  return vdoc.getObject(versionClass).getProperty('version').getValue() + 1
}

void bumpVersion()
{
    def vdoc = xwiki.getDocument(versionDoc)
    int newVersion = getVersion()
    vdoc.getObject(versionClass).set('version', newVersion)
    vdoc.save('New version ' + newVersion)
}

Map<String, Integer> getPackageCodeMap ()
{
  Map<String, Integer> map = new HashMap<>();
  int version = getVersion()
  for (row in services.query.xwql('SELECT o.text_code, o.code FROM ' + this.packageClass + ' AS o WHERE o.version = :version').bindValue('version', version).execute()) {
    if (row[0] != null && row[0] != '') {
      map.put(row[0], row[1]);
    }
  }
  return map;
}

List<Integer> getMeasureCodes()
{
  List<Integer> list =  new LinkedList<>()
  int version = getVersion()
  for (code in services.query.xwql('SELECT o.code FROM ' + this.measureClass + ' AS o WHERE o.version = :version').bindValue('version', version).execute()) {
    list.add(code)
  }
  return list
}

void importPackages()
{
  final int AREA_FIELD_INDEX = 1;
  final int TEXT_CODE_INDEX = 2;
  final int TITLE_INDEX = 3;
  final int DESCRIPTION_INDEX = 4;
  final int MEASURES_INDEX = 5;
  final int PRICE_INDEX = 6;
  
  ObjectPopulator.Field textCodeField = new ObjectPopulator.Field(TEXT_CODE_INDEX).setFieldName('text_code')
  ObjectPopulator.Field areaField     = new ObjectPopulator.Field(AREA_FIELD_INDEX).setFieldName('area')

  int code = 0
  int version = getVersion()

  CSVMapper csvMapper = new CSVMapper(this, xwiki, services, Locale.forLanguageTag('sv'), new ObjectPopulator.NamingScheme() {
					@Override
					ObjectPopulator.ObjectReference getObjectReference(ObjectPopulator populator, ObjectPopulator.Record record, String fieldName)
					{
					  return new ObjectPopulator.ObjectReference(drr.resolve(packageSpace + ".Version" + version + "." + record.get(textCodeField)),
										     packageClass, 0)
					}
				      }) {
    @Override
    protected CSVMapper.WrappedRecord extendWrappedRecord(CSVMapper.WrappedRecord wrappedRecord) {
      wrappedRecord.setExtended('code', '' + code)
      wrappedRecord.setExtended('version', '' + version)
      code++;
      return wrappedRecord
    }

  };

  csvMapper.addExtendedField('code')
           .addExtendedField('version')
           .addField(textCodeField)
           .addField(areaField)
           .addField(new ObjectPopulator.Field(TITLE_INDEX).setFieldName('title'))
           .addField(new ObjectPopulator.Field(DESCRIPTION_INDEX).setFieldName('description'))
           .addField(new ObjectPopulator.Field(MEASURES_INDEX).setFieldName('measures'))
           .addField(new ObjectPopulator.Field(PRICE_INDEX).setFieldName('price'))


  csvMapper.process(getAttachmentReference(PACKAGES_FILENAME), CSVFormat.DEFAULT.withHeader())
}

void importMeasures()
{
  ObjectPopulator.Field codeField = new ObjectPopulator.Field(0).setFieldName('code')

  int version = getVersion()

  ObjectPopulator.NamingScheme namingScheme = new ObjectPopulator.NamingScheme() {
    @Override
    ObjectPopulator.ObjectReference getObjectReference(ObjectPopulator populator, ObjectPopulator.Record record, String fieldName)
    {
      return new ObjectPopulator.ObjectReference(drr.resolve(measureSpace + ".Version" + version + "." + record.get(codeField)),
						 measureClass, 0)
    }
  }

  CSVMapper csvMapper = new CSVMapper(this, xwiki, services, Locale.forLanguageTag('sv'), namingScheme) {
    @Override
    protected CSVMapper.WrappedRecord extendWrappedRecord(CSVMapper.WrappedRecord wrappedRecord) {
      wrappedRecord.setExtended('version', '' + version)
      return wrappedRecord;
    }

  };

  csvMapper.addField(codeField)
           .addExtendedField('version')
           .addField(new ObjectPopulator.Field(1).setFieldName('title'))
           .addField(new ObjectPopulator.Field(2).setFieldName('price').setAllowNull(true))
           .addField(new ObjectPopulator.Field(3).setFieldName('price_special').setAllowNull(true))


  csvMapper.process(getAttachmentReference("Åtgärder ref priser ATV-STV.csv"), CSVFormat.DEFAULT.withHeader())

  csvMapper = new CSVMapper(this, xwiki, services, Locale.forLanguageTag('sv'), namingScheme)
  csvMapper.addField(new ObjectPopulator.Field(1).setFieldName('area'))
           .addField(new ObjectPopulator.Field(2).setFieldName('area_special'))

  csvMapper.process(getAttachmentReference("Terapiområden.csv"), CSVFormat.DEFAULT.withHeader())
}

void importMeasurePrices()
{
  int version = getVersion()

  final NumberFormat numberFormat = NumberFormat.getInstance(Locale.forLanguageTag('sv'))

  List<Exception> exceptions = new LinkedList<>()

  final int SURGERY_ID_INDEX = 0;
  final int CODE_INDEX = 1;
  final int PRICE_INDEX = 2;
  final int REFPRICE_INDEX = 3;
  final int SPECIAL_INDEX = 4;

  final ObjectPopulator.Field priceField = new ObjectPopulator.Field(PRICE_INDEX).setFieldName('tvpip_price')

  for (filename in [MEASURE_PRICES_FILENAME]) {
      CSVMapper csvMapper = new CSVMapper(this, xwiki, services, xcontext, Locale.forLanguageTag('sv'), 'tpk_itemprice') {
	
	@Override
	protected CSVMapper.WrappedRecord extendWrappedRecord(CSVMapper.WrappedRecord wrappedRecord) {
	  wrappedRecord.setExtended('tvpip_is_measure', 'true')
	  wrappedRecord.setExtended('tvpip_type', '' + ("Ja".equalsIgnoreCase(wrappedRecord.getFieldValue(SPECIAL_INDEX)) ? 2 : 1))
	  wrappedRecord.setExtended('tvpip_version', '' + version)
	  return wrappedRecord
	}

	@Override
	protected boolean filterRecord(ObjectPopulator.Record record) {
	  try {
	    numberFormat.parse(record.get(priceField))
	    return false
	  } catch (java.text.ParseException e) {
	    return true
	  }
	}
				  
	@Override
	protected void initiateHeader(Map<String, Integer> headerMap) {
	  addField(priceField)
	}

      }

      csvMapper.addExtendedField('tvpip_is_measure')
        .addExtendedField('tvpip_type')
        .addExtendedField('tvpip_version')
        .addField(new ObjectPopulator.Field(SURGERY_ID_INDEX)
		  .setFieldName('tvpip_surgery_id'))
        .addField(new ObjectPopulator.Field(CODE_INDEX)
		  .setFieldName('tvpip_item_id'))
        .addField(priceField)
      csvMapper.setExceptions(exceptions);
      try {
	csvMapper.process(getAttachmentReference(filename), CSVFormat.DEFAULT.withHeader())
      } catch (java.text.ParseException e) {
	  throw e
      } catch (Exception e) {
	exceptions.add(e)
      }
  }

  if (exceptions.size() > 0) {
    println('{{warning}}There where xceptions:')
    for (e in exceptions) {
      println(e)
    }
    println('{{/warning}}')
  }
}

void importSurgeries()
{
    def random = new Random(10222)

    final ObjectPopulator.Field categoryField = new ObjectPopulator.Field(1).setFieldName('category');

    final List warnings = new LinkedList()
    final List errors = new LinkedList()

    final CoordinatesAndRegion coordinatesAndRegion = new CoordinatesAndRegion(xwiki, services);

    CSVMapper csvMapper = new CSVMapper(this, xwiki, services, Locale.forLanguageTag('sv'),
					new ObjectPopulator.NamingScheme() {
					  @Override
					  ObjectPopulator.ObjectReference getObjectReference(ObjectPopulator populator, ObjectPopulator.Record record, String fieldName)
					  {
					    boolean isEditable = [ 'latitude', 'longitude', 'address' ].contains(fieldName)
					    return new ObjectPopulator.ObjectReference(
					      drr.resolve((services.tpkCompare.SURGERY_VIEW_SPACE)
                                                          + '.' + record.get(populator.getFields().get(0)) + '.WebHome'),
					      isEditable ? surgeryEditableClass : surgeryClass, 0)
					 }
					}) {
      @Override
      protected CSVMapper.WrappedRecord extendWrappedRecord(CSVMapper.WrappedRecord wrappedRecord) {
	double [] pos = randomPosition(random)
	wrappedRecord.setExtended('name', randomName(random, wrappedRecord.get(categoryField)))
	String addressCS = wrappedRecord.getFieldValue(5) + ", " + wrappedRecord.getFieldValue(6) + ", " + "Sverige"
	String address = wrappedRecord.getFieldValue(5) + "\n" + wrappedRecord.getFieldValue(7) + "\n" +  wrappedRecord.getFieldValue(6)
	def nf = NumberFormat.getInstance(Locale.forLanguageTag('sv'))
	nf.setMaximumFractionDigits(10)
	def coordinates = coordinatesAndRegion.getCoordinates(addressCS, warnings, errors)
	wrappedRecord.setExtended('address', address)
	if (coordinates != null) {
	  wrappedRecord.setExtended('longitude', nf.format(coordinates.lon))
	  if (coordinates.lat == null) {
	    throw new RuntimeException('latute is null, lon: ' + coordinates.lon + ' for address ' + address)
	  }
	  def formattedLat = nf.format(coordinates.lat)
	  if (formattedLat == null) {
	    throw new RuntimeException('latute is null, lon: ' + coordinates.lon + ' for address ' + address)
	  }
	  wrappedRecord.setExtended('latitude', nf.format(coordinates.lat))
	  def regions = getRegions(coordinates, errors)
	  if (regions != null) {
	    String county = regions.get("COUNTY")
	    String municipality = regions.get("MUNICIPALITY")
	    String sublocality = regions.get("SUBLOCALITY")
	    if (county != wrappedRecord.getFieldValue(2)) {
	      warnings.add("Surgery " + wrappedRecord.getFieldValue(0) + " have suspect county.  From file: "
			   + wrappedRecord.getFieldValue(2) + ", from service: " + county)
	    }
	    wrappedRecord.setExtended('municipality', municipality)
	    wrappedRecord.setExtended('county', county)
	    wrappedRecord.setExtended('sublocality', sublocality)
	  }
	} else {
	  wrappedRecord.setExtended('county', wrappedRecord.getFieldValue(2))
	}
	return wrappedRecord
      }

      @Override
      protected void initiateProcessing() {
	super.initiateProcessing()
	getField('latitude').setAllowNull(true)
	getField('longitude').setAllowNull(true)
      }
    }

    csvMapper
    .addExtendedField('name')
    .addExtendedField('latitude')
    .addExtendedField('longitude')
    .addExtendedField('address')
    .addExtendedField('municipality')
    .addExtendedField('county')
    .addExtendedField('sublocality')
    .addField(new ObjectPopulator.Field(0)
	      .setFieldName('surgery_id'))
    .addField(categoryField)

    csvMapper.addFieldModifier(categoryField, { s -> s.equalsIgnoreCase('Privat') ? 'Privat' : 'Landsting'})

    csvMapper.process(getAttachmentReference("Mottagningsadresser.csv"), CSVFormat.DEFAULT.withHeader())

    if (errors.size() > 0) {
      println("{{error}}\nThere where errors!\n{{/error}}")
      for (error in errors) {
	println("* " + error)
      }
    }

    if (warnings.size() > 0) {
      println("{{warning}}\nThere where warnings!\n{{/warning}}")
      for (warning in warnings) {
	println("* " + warning)
      }
    }

}

void importSurgeriesFromPriceData()
{
    final List warnings = new LinkedList();
    final List errors = new LinkedList();

    final int SURGERY_ID_INDEX = 0;
    final int NAME_INDEX = 5;
    final int STREET_INDEX = 6;
    final int CITY_INDEX = 7;
    final int POST_CODE_INDEX = 8;
    final int CATEGORY_INDEX = 9;
    final int COMPANY_INDEX = 10;
    final def script = this;
    
    final CoordinatesAndRegion coordinatesAndRegion = new CoordinatesAndRegion(xwiki, services);

    final Set editableFields = [
      'latitude',
      'longitude',
      'address'
    ];

    final ObjectPopulator.Field categoryField = new ObjectPopulator.Field(CATEGORY_INDEX).setFieldName('category');
    final ObjectPopulator.Field surgeryIdField = new ObjectPopulator.Field(SURGERY_ID_INDEX).setFieldName('surgery_id');

    final NumberFormat numberFormat = NumberFormat.getInstance(Locale.forLanguageTag('sv'))
	
    CSVMapper csvMapper = new CSVMapper(
      this,
      xwiki,
      services,
      Locale.forLanguageTag('sv'),
      new ObjectPopulator.NamingScheme() {
	@Override
	ObjectPopulator.ObjectReference getObjectReference(ObjectPopulator populator, ObjectPopulator.Record record, String fieldName)
	{
	  boolean isEditable = editableFields.contains(fieldName)
	  return new ObjectPopulator.ObjectReference(
	    drr.resolve((services.tpkCompare.SURGERY_VIEW_SPACE)
			+ '.' + record.get(populator.getFields().get(0)) + '.WebHome'),
	    isEditable ? surgeryEditableClass : surgeryClass, 0)
	}
      },
      new ObjectPopulator.DefaultObjCache(xwiki, services) {
	@Override
        void endTransaction() {
	  for (doc in docs.values()) {
	    if (doc.getFullName().startsWith(services.tpkCompare.SURGERY_VIEW_SPACE + '.')) {
	      if (doc.isNew()) {
		doc.setTitle('#set($c=\'TVPris.Classes.Surgery\')#set($r=$doc.getObject("${c}Class"))#set($e=$doc.getObject("${c}EditableClass"))#if("$!{e.display(\'name\',\'view\')}"==\'\')$!{r.display(\'name\',\'view\')}#{else}$!{e.display(\'name\',\'view\')}#end');
	      }
	      def editObj = doc.getObject(surgeryEditableClass);
	      def surgeryObj = doc.getObject(surgeryClass);

	      if (editObj != null) {
		surgeryObj.set('surgery_editable_id', editObj.getBaseObject().getId())

		def surgeryRef = doc.getDocumentReference();
		def previewRootRef = services.model.resolveSpace(services.tpkCompare.SURGERY_EDIT_SPACE);
		def previewSpaceRef = services.model.createSpaceReference(surgeryRef.getParent().getName(), previewRootRef);
		def previewDocRef = services.model.createDocumentReference('WebHome', previewSpaceRef);

		previewDoc = xwiki.getDocument(previewDocRef);
		if (previewDoc.isNew()) {
		  def previewObj = previewDoc.getObject(surgeryPreviewClass, true)
		  if (editObj == null) {
		    continue
		  }

		  copyObjectProperties(editObj, previewObj);
		  previewDoc.save('ObjectImport: Creating document.');
		}
	      }
	    }
	  }
	  super.endTransaction();
	}

      }
    ) {

      private Integer prevSurgeryId = null

      private boolean filter(ObjectPopulator.Record record) {
	try {
	  int sid = numberFormat.parse(record.get(surgeryIdField));
	  boolean ret = prevSurgeryId == sid;
	  prevSurgeryId = sid;
	  if (ret) {
	    return ret;
	  }
	  // For now filter existing
	  if (!xwiki.getDocument(services.tpkCompare.SURGERY_VIEW_SPACE + "." + sid + ".WebHome").isNew()) {
	    script.println("* Filtered " + sid)
	    return true;
	  }
	  return ret
	} catch (java.text.ParseException e) {
	  return true
	}
      }
      
      @Override
      protected CSVMapper.WrappedRecord extendWrappedRecord(CSVMapper.WrappedRecord wrappedRecord) {
	if (filter(wrappedRecord)) {
	  return null
	}
	def nf = NumberFormat.getInstance(Locale.forLanguageTag('sv'))
	nf.setMaximumFractionDigits(10)
	def coordinates = coordinatesAndRegion.getCoordinates(wrappedRecord.getFieldValue(ADDRESS_INDEX), wrappedRecord.getFieldValue(ZIPCODE_INDEX), wrappedRecord.getFieldValue(CITY_INDEX), warnings, errors)
	if (coordinates != null) {
	  wrappedRecord.setExtended('longitude', nf.format(coordinates.lon))
	  def formattedLat = nf.format(coordinates.lat)
	  wrappedRecord.setExtended('latitude', nf.format(coordinates.lat))
	  if (coordinates.municipality != null) {
	    wrappedRecord.setExtended('municipality', coordinates.municipality);
	  }
	  if (coordinates.county != null) {
	    wrappedRecord.setExtended('county', coordinates.county);
	  }
	  if (coordinates.sublocality != null) {
	    wrappedRecord.setExtended('sublocality', coordinates.sublocality);
	  }
	}

	wrappedRecord.setExtended('address', address)
	return wrappedRecord
      }

      @Override
      protected void initiateProcessing() {
	super.initiateProcessing()
	getField('latitude').setAllowNull(true).setNoUpdate(true)
	getField('longitude').setAllowNull(true).setNoUpdate(true)
	getField('address').setNoUpdate(true)
      }
    }

    final ObjectPopulator.Field zipcodeField = new ObjectPopulator.Field(ZIPCODE_INDEX)
	      .setFieldName('zipcode')

    csvMapper
    .addExtendedField('latitude')
    .addExtendedField('longitude')
    .addExtendedField('address')
    .addExtendedField('municipality')
    .addExtendedField('county')
    .addExtendedField('sublocality')
    .addField(surgeryIdField)
    .addField(new ObjectPopulator.Field(NAME_INDEX)
	      .setFieldName('name'))
    .addField(new ObjectPopulator.Field(COMPANY_INDEX)
	      .setFieldName('company'))
    .addField(new ObjectPopulator.Field(ADDRESS_INDEX)
	      .setFieldName('address'))
    .addField(zipcodeField)
    .addField(new ObjectPopulator.Field(CITY_INDEX)
	      .setFieldName('city'))
    .addField(categoryField)

    csvMapper.addFieldModifier(categoryField, { s -> s.equalsIgnoreCase('Privat') ? 'Privat' : 'Landsting'})
    csvMapper.addFieldModifier(zipcodeField, { s -> String s0 = s.replaceAll("\\s", ""); s0.substring(0, 3) + " " + s0.substring(3, 2) })

    csvMapper.process(getAttachmentReference(MEASURE_PRICES_FILENAME), CSVFormat.DEFAULT.withHeader())

    if (errors.size() > 0) {
      println("{{error}}\nThere where errors!\n{{/error}}")
      for (error in errors) {
	println("* " + error)
      }
    }

    if (warnings.size() > 0) {
      println("{{warning}}\nThere where warnings!\n{{/warning}}")
      for (warning in warnings) {
	println("* " + warning)
      }
    }

}


void importPackagePrices() {

    final NumberFormat numberFormat = NumberFormat.getInstance(Locale.forLanguageTag('sv'))
    final Map packageCodeMap = getPackageCodeMap()
    final int version = getVersion()

    for (code in packageCodeMap.keySet()) {

        int codeId = packageCodeMap.get(code)

	def script = this

        CSVMapper csvMapper = new CSVMapper(this, xwiki, services, xcontext, Locale.forLanguageTag('sv'), 'tpk_itemprice') {

	                                        private ObjectPopulator.Field priceField

                                                @Override
                                                protected CSVMapper.WrappedRecord extendWrappedRecord(CSVMapper.WrappedRecord wrappedRecord) {
                                                    wrappedRecord.setExtended('tvpip_item_id', '' + codeId)
                                                    wrappedRecord.setExtended('tvpip_is_measure', 'false')
                                                    wrappedRecord.setExtended('tvpip_type', '0')
                                                    wrappedRecord.setExtended('tvpip_version', '' + version)
						    return wrappedRecord
                                                }

                                                @Override
                                                protected boolean filterRecord(ObjectPopulator.Record record) {
                                                    try {
                                                        numberFormat.parse(record.get(priceField))
                                                        return false
                                                    } catch (java.text.ParseException e) {
                                                        return true
                                                    }
                                                }

						@Override
						protected void initiateHeader(Map<String, Integer> headerMap) {
						     Integer column = headerMap.get(code)
						     if (column == null) {
						       throw new RuntimeException('There is no column for package ' + code + '!')
						     }
						     priceField = new ObjectPopulator.Field(column).setFieldName('tvpip_price')
						     addField(priceField)
						}
                                            }

        csvMapper.addField(new ObjectPopulator.Field(0)
                           .setFieldName('tvpip_surgery_id'))
	         .addExtendedField('tvpip_item_id')
                 .addExtendedField('tvpip_type')
                 .addExtendedField('tvpip_is_measure')
                 .addExtendedField('tvpip_version')

	try {
	  csvMapper.process(getAttachmentReference(PACKAGE_PRICES_FILENAME), CSVFormat.DEFAULT.withHeader())
	} catch (Exception e) {
	  println('{{error}}' + 'Code: ' + code + ': exception ' + e + '{{/error}}')
	}
    }
}

String randomName (Random random, String category) {
      String [] fruits = [
      'Alaskahallonet',
      'Allåkerbäret',
      'Amanatsun',
      'Amaranthen',
      'Amerikanska blåbäret',
      'Amerikanska tranbäret',
      'Ananasen',
      'Ananasguavan',
      'Ananassmultronet',
      'Apelsinen',
      'Aprikosen',
      'Auberginen',
      'Avokadon',
      'Aronian',
      'Babacon',
      'Backsmultronet',
      'Bananen',
      'Bergamotten',
      'Bigarråen',
      'Bilimbin',
      'Bittergurkan',
      'Bittermandeln',
      'Björnbäret',
      'Blodpomelon',
      'Blåbäret',
      'Blåhallonet',
      'Boysenbäret',
      'Brakveden',
      'Brytärten',
      'Brödfrukten',
      'Bröstbäret',
      'Bäverbäret',
      'Blodapelsinen',
      'Cantaloupen',
      'Carambolan',
      'Cashewnöten',
      'Cashewäpplet - falsk frukt',
      'Cayennepepparn',
      'Charantaisen',
      'Chayoten',
      'Cherimoyan',
      'Chikun',
      'Chilipepparn',
      'Citronen',
      'Clausellinan',
      'Clementinen',
      'Cocktailkiwin',
      'Colanöten',
      'Curuban',
      'Dadeln',
      'Daidaien',
      'Dessertkiwin',
      'Dhudin',
      'Drakfrukten',
      'Druvflädern',
      'Durianen',
      'Durran',
      'Dvärgapelsinen',
      'Dvärgtranbäret',
      'Druvan',
      'Ekollonet',
      'Ekorrbäret',
      'Ellendalen',
      'Enbäret',
      'Feijoan',
      'Fikonet',
      'Fikonkaktusen',
      'Fjällodonet',
      'Flaskkurbitsen',
      'Flädern',
      'Frasbäret',
      'Fotbladet',
      'Galiamelonen',
      'Granadinen',
      'Granatäpplet',
      'Grapefrukten',
      'Grenadillan',
      'Greve moltken',
      'Grodskinnsmelonen',
      'Guavan',
      'Gula kiwin',
      'Gurkan',
      'Gurkträdet',
      'Hagtornet',
      'Hallonet',
      'Hasselnöten',
      'Havrekornet',
      'Havtornet',
      'Hernandinan',
      'Hickorynöten',
      'Hirskornet',
      'Hjortronet',
      'Honungsmelonen',
      'Hundrovan',
      'Indiska marmeladträdet',
      'Iyokanen',
      'Jackfrukten',
      'Japanske dvärgapelsinen',
      'Japanske mispeln',
      'Japanske persimonen',
      'Johannesbrödet',
      'Jordgubben',
      'Jordnöten',
      'Jujubäret',
      'Kafirlimen',
      'Kakin',
      'Kaktusfikonet',
      'Kalamondinen',
      'Kalebassen',
      'Kardborren',
      'Karellan',
      'Kapkrusbäret',
      'Kaprisen',
      'Kastanjenöten',
      'Kayoten',
      'Kermesbäret',
      'Kinesiske persimonen',
      'Kiwaien',
      'Kiwanon',
      'Kiwin',
      'Klementinen',
      'Kokbananen',
      'Kokosanen',
      'Kokosnöten',
      'Kolokvinten',
      'Kornet',
      'Koromandelkrusbäret',
      'Krikonet',
      'Krusbäret',
      'Krusbärsaktindian',
      'Kryddpepparn',
      'Kråkbäret',
      'Kumquaten',
      'Kvitten',
      'Körsbäret',
      'Langsaten',
      'Limefrukten',
      'Limequaten',
      'Limonellcitronen',
      'Lingonet',
      'Litchin',
      'Longanen',
      'Loquaten',
      'Majskornet',
      'Malvasion',
      'Mameyen',
      'Mandarinen',
      'Mandeln',
      'Mandorn',
      'Mangon',
      'Mangostanen',
      'Maracuyan',
      'Marmeladträdet',
      'Melonen',
      'Mexikanska äpplet',
      'Mikanen',
      'Minneolan',
      'Mirabellan',
      'Mispeln',
      'Mjölbananen',
      'Morellen',
      'Mullbäret',
      'Murgrönan',
      'Muskotnöten',
      'Måbäret',
      'Månadssmultronet',
      'Nashipäronet',
      'Nejkonet',
      'Nektarinen',
      'Nouret',
      'Nyponet',
      'Nätmelonen',
      'Odonet',
      'Ogenmelonen',
      'Okran',
      'Oliven',
      'Olvonet',
      'Orangelon',
      'Ormbäret',
      'Ortalinen',
      'Oxbäret',
      'Oxeln',
      'Papayan',
      'Paprikan',
      'Paranöten',
      'Parksmultronet',
      'Passionsfrukten',
      'Pekannöten',
      'Pepinon',
      'Pepparn',
      'Persikan',
      'Persimonen',
      'Physalisen',
      'Piel de Sapon',
      'Pimenten',
      'Pistaschnöten',
      'Pitahayan',
      'Plommonet',
      'Pomeransen',
      'Pomelon',
      'Pumpan',
      'Purpurgrenadillan',
      'Päronet',
      'Rambutanen',
      'Rangpuren',
      'Riskornet',
      'Rågkornet',
      'Röda vinbäret',
      'Rönnbäret',
      'Salaken',
      'Salmbäret',
      'Sandpäronet',
      'Sapodillen',
      'Satsuman',
      'Scharlakanssmultronet',
      'Sharonfrukten',
      'Sjönöten',
      'Skuggmorellan',
      'Slånbäret',
      'Smulgubben',
      'Smultronet',
      'Smyrnafikonet',
      'Sockermelonen',
      'Sockerärtan',
      'Solrosfröet',
      'Spaghettipumpan',
      'Springgurkan',
      'Sprutgurkan',
      'Squashen',
      'Stenbäret',
      'Stenhallonet',
      'Stickelbäret',
      'Stinkfrukten',
      'Stjärnfrukten',
      'Suckatcitronen',
      'Sudachin',
      'Suntinan',
      'Surkörsbäret',
      'Svarta vinbäret',
      'Sviskonet',
      'Sweetien',
      'Sötcitronen',
      'Sötkörsbäret',
      'Sötgrenadillan',
      'Sötmandeln',
      'Tamarillon',
      'Tamarinden',
      'Tangelon',
      'Tangerinen',
      'Tangorn',
      'Tomaten',
      'Tranbäret',
      'Trädkrusbäret',
      'Trädtomaten',
      'Uglin',
      'Valnöten',
      'Vattenmelonen',
      'Vetekornet',
      'Vikonet',
      'Vildapeln',
      'Vinbäret',
      'Vindruvan',
      'Vite sapoten',
      'Vägtornen',
      'Yuzun',
      'Åkerbäret',
      'Äggplantan',
      'Äppelbananen',
      'Äppelpäronet',
      'Äpplet' ]

    String [] fname = ['Maria', 'Anna', 'Margareta', 'Elisabeth', 'Eva', 'Kristina', 'Birgitta', 'Karin', 'Elisabet', 'Marie', 'Ingrid', 'Christina', 'Linnéa', 'Sofia', 'Kerstin', 'Marianne', 'Lena', 'Helena', 'Emma', 'Inger', 'Johanna', 'Linnea', 'Sara', 'Cecilia', 'Elin', 'Erik', 'Lars', 'Karl', 'Anders', 'Johan', 'Per', 'Nils', 'Carl', 'Mikael', 'Jan', 'Lennart', 'Hans', 'Olof', 'Peter', 'Gunnar', 'Sven', 'Fredrik', 'Bengt', 'Bo', 'Daniel', 'Åke', 'Gustav', 'Göran', 'Alexander', 'Magnus']

    String [] lname = ['Johansson', 'Andersson', 'Karlsson', 'Nilsson', 'Eriksson', 'Larsson', 'Olsson', 'Persson', 'Svensson', 'Gustafsson', 'Pettersson', 'Jonsson', 'Jansson', 'Hansson', 'Bengtsson', 'Jönsson', 'Carlsson', 'Petersson', 'Lindberg', 'Magnusson', 'Lindström', 'Gustavsson', 'Olofsson', 'Lindgren', 'Axelsson']

    String [] noun = ['ön', 'ädelstenen', 'ödemarken', 'ån', 'ägget', 'öknen', 'ålen', 'älgen', 'älvan', 'änderna', 'ängen', 'ångorna', 'äpplet', 'öppenvården', 'öringens', 'örnen', 'åskmolnet', 'åttiosjuan', 'äventyraren', 'abakusen', 'abborren', 'adjunkten', 'affären', 'akademien', 'akuten',  'alfabetet', 'analytikern', 'ananas', 'ankan', 'ankaret', 'anslagstavlan', 'apelsinen', 'apotekaren', 'arbetsplatsen', 'arenan', 'aspen', 'avenyn', 'bågen', 'backen', 'balanserande', 'bananen', 'batteriet', 'bautastenen', 'berget', 'bergskedjan', 'biet', 'björken', 'björnen', 'bjällran', 'bjälken', 'blåklinten', 'bocken', 'bofinken', 'bojen', 'brädan', 'brännässlan', 'byn', 'citronen']
    String [] adj  = ['öde', 'ädla', 'absoluta', 'abstrakta', 'alarmistiska', 'allmänna', 'allmännyttiga', 'angenäma', 'anspråkslösa', 'approximativa', 'artonde', 'auktoriserade', 'bästa', 'barocka', 'basiska', 'bayerska', 'behandlade', 'beiga', 'bekymmrade', 'belåtna', 'berömda', 'blå', 'blixtrande', 'blomstrande', 'blygsamme', 'botade', 'briljanta', 'brummande', 'bullrande', 'charmiga', 'chicka']

    return fruits[random.nextInt(fruits.length)] + "s " + ("Landsting".equalsIgnoreCase(category) ? "folktandvård" : "mottagning")
}

Double [] randomPosition(Random random) {
    double neLat = 68.739693
    double neLong  = 25.722528
    double swLat = 54.769679
    double swLong  = 5.837868

    def r = { u, l -> random.nextDouble() * (u - l) + l }

    return [ r(neLong, swLong), r(neLat, swLat) ]
}

void deleteSurgeries() {
  for (s in services.query.xwql('from doc.object(' + surgeryClass + ') as se').execute()) {
    try {
      def sd = xwiki.getDocument(s)
      sd.delete();
    } catch (Exception e) {
      println('* Failed to delete ' + s + ': ' + e)
    }
  }
  for (s in services.query.xwql('from doc.object(' + surgeryEditableClass + ') as s').execute()) {
    try {
      def sd = xwiki.getDocument(s)
      def previewRef = sd.getDocumentReference()
      def publicCommentRef = services.model.createDocumentReference('PublicComments', previewRef.getParent())
      def internalCommentRef = services.model.createDocumentReference('InternalComments', previewRef.getParent())
      def pcd = xwiki.getDocument(publicCommentRef);
      if (!pcd.isNew()) {
	pcd.delete();
      }
      def icd = xwiki.getDocument(internalCommentRef);
      if (!icd.isNew()) {
	icd.delete();
      }
      sd.delete();
    } catch (Exception e) {
      println('* Failed to delete ' + s + ': ' + e)
    }
  }

}

void setSurgeryTitles() {
    for (s in services.query.xwql('from doc.object(TVPris.Classes.SurgeryClass) as se').execute()) {
	def surgeryDoc = xwiki.getDocument(s)
	surgeryDoc.setTitle('#set($c=\'TVPris.Classes.Surgery\')#set($r=$doc.getObject("${c}Class"))#set($e=$doc.getObject("${c}EditableClass"))#if("$!{e.display(\'name\',\'view\')}"==\'\')$!{r.display(\'name\',\'view\')}#{else}$!{e.display(\'name\',\'view\')}#end')
	surgeryDoc.save('Setting title')
    }
}

void fixEditableObjectReferences() {
    for (s in services.query.xwql('from doc.object(' + surgeryClass + ') as se').execute()) {
	def surgeryDoc = xwiki.getDocument(s)
	def surgeryObj = surgeryDoc.getObject(surgeryClass)
	def editObj = surgeryDoc.getObject(surgeryEditableClass)
	surgeryObj.set('surgery_editable_id', editObj.getBaseObject().getId())
	surgeryDoc.save('Set editable object references')
    }
}

void restoreRepair() {
    for (s in services.query.xwql('from doc.object(' + surgeryClass + ') as se').execute()) {
	def surgeryDoc = xwiki.getDocument(s)
	def surgeryObj = surgeryDoc.getObject(surgeryClass)
	def roRef = surgeryDoc.getDocumentReference()
	def editableRef = services.model.createDocumentReference('Editable', roRef.getParent())
	def editDoc = xwiki.getDocument(editableRef)
	def editObj = editDoc.getObject(surgeryEditableClass)

	def editRevInfo = editDoc.getRevisionInfo(editDoc.getVersion())

	if (editRevInfo.getComment() == 'Refactoring for search and preview') {
	  println('should roll back ' +  editDoc.getFullName() + " to " + editDoc.getPreviousVersion())
	  xwiki.getXWiki().rollback(editDoc.getDocument(), editDoc.getPreviousVersion(), xcontext.getContext())
	}

	def revInfo = surgeryDoc.getRevisionInfo(surgeryDoc.getVersion())
	if (revInfo.getComment() == 'Refactoring for search and preview') {
	  println('should roll back ' +  surgeryDoc.getFullName() + " to " + surgeryDoc.getPreviousVersion())
	  xwiki.getXWiki().rollback(surgeryDoc.getDocument(), surgeryDoc.getPreviousVersion(), xcontext.getContext())
	}
	
    }
}

void copyObjectProperties(srcObj, dstObj) {
  for (prop in srcObj.getxWikiClass().getPropertyNames()) {
    def value = srcObj.getProperty(prop) != null ? srcObj.getProperty(prop).getValue() : null
    dstObj.set(prop, value)
  }
}

void copySurgeryEditObj() {

    for (s in services.query.xwql('from doc.object(' + surgeryClass + ') as se').execute()) {
	def surgeryDoc = xwiki.getDocument(s)
	def surgeryObj = surgeryDoc.getObject(surgeryClass)
	def roRef = surgeryDoc.getDocumentReference()
	def editableRef = services.model.createDocumentReference('Editable', roRef.getParent())
	def editDoc = xwiki.getDocument(editableRef)
	def editObj = editDoc.getObject(surgeryEditableClass)
	def newEditObj = surgeryDoc.getObject(surgeryEditableClass, true)
	def previewObj = editDoc.getObject(surgeryPreviewClass, true)
	if (editObj == null) {
	    continue
	}

	for (prop in previewObj.getxWikiClass().getPropertyNames()) {
	    def value = editObj.getProperty(prop) != null ? editObj.getProperty(prop).getValue() : null
	    previewObj.set(prop, value)
	    newEditObj.set(prop, value)
	}

	editDoc.removeObject(editObj)
	editDoc.save('Refactoring for search and preview')
	surgeryDoc.save('Refactoring for search and preview')
    }
}

void movePreviewObject() {
  for (s in services.query.xwql('from doc.object(TVPris.Classes.SurgeryClass) as se').execute()) {
    def surgeryDoc = xwiki.getDocument(s)
    def surgeryRef = surgeryDoc.getDocumentReference()
    def invalidPreviewObj = surgeryDoc.getObject('TVPris.Classes.SurgeryPreviewClass')
    if (invalidPreviewObj != null) {
      def previewRootRef = services.model.resolveSpace(services.tpkCompare.SURGERY_EDIT_SPACE);
      def previewSpaceRef = services.model.createSpaceReference(surgeryRef.getParent().getName(), previewRootRef);
      def previewDocRef = services.model.createDocumentReference('WebHome', previewSpaceRef);
      def previewDoc = xwiki.getDocument(previewDocRef)
      def previewObj = previewDoc.getObject(surgeryPreviewClass, true)
      copyObjectProperties(invalidPreviewObj, previewObj)
      surgeryDoc.removeObject(invalidPreviewObj)
      previewDoc.save('Copied preview object')
      surgeryDoc.save('Deleted preview object')
    }
  }
}

void moveEditableObjectAndDeletePreviewObject() {
  for (s in services.query.xwql('from doc.object(TVPris.Classes.SurgeryClass) as se').execute()) {
    def surgeryDoc = xwiki.getDocument(s)
    def surgeryRef = surgeryDoc.getDocumentReference()
    def surgeryObj = surgeryDoc.getObject('TVPris.Classes.SurgeryClass')
    def oldEditObj = surgeryDoc.getObject('TVPris.Classes.SurgeryEditableClass')
    if (oldEditObj != null) {
      def editRootRef = services.model.resolveSpace(services.tpkCompare.SURGERY_EDIT_SPACE);

      
      def previewSpaceRef = services.model.createSpaceReference(surgeryRef.getParent().getName(), editRootRef);
      def previewDocRef = services.model.createDocumentReference('WebHome', previewSpaceRef);
      def previewDoc = xwiki.getDocument(previewDocRef)

      def editSpaceRef = previewSpaceRef
      def editDocRef = previewDocRef
      def editDoc = previewDoc
      
      def previewObj = previewDoc.getObject(surgeryPreviewClass, true)
      previewDoc.removeObject(previewObj)

      previewDoc.save('Removed preview object')

      def editObj = editDoc.getObject(surgeryEditableClass, true)

      copyObjectProperties(oldEditObj, editObj)

      editDoc.save('Copied edit object')

      surgeryDoc.removeObject(oldEditObj)
      surgeryDoc.save('Removed edit object')
      
    }
  }
}

void fixSurgeryObjectLinks() {
  for (s in services.query.xwql('from doc.object(TVPris.Classes.SurgeryClass) as se').execute()) {
    def editRootRef = services.model.resolveSpace(services.tpkCompare.SURGERY_EDIT_SPACE);
    def surgeryDoc = xwiki.getDocument(s)
    def surgeryRef = surgeryDoc.getDocumentReference()
    def surgeryObj = surgeryDoc.getObject(surgeryClass)
    def editSpaceRef = services.model.createSpaceReference(surgeryRef.getParent().getName(), editRootRef);
    def editDocRef = services.model.createDocumentReference('WebHome', editSpaceRef);
    def editDoc = xwiki.getDocument(editDocRef)
    def editObj = editDoc.getObject(surgeryEditableClass, true)
    surgeryObj.set('surgery_editable_id', editObj.getXWikiObject().getId())
    surgeryDoc.save('fixed link to editable object')
  }
}


void moveEditablePages() {
    def surgeryRootRef = services.model.resolveSpace(services.tpkCompare.SURGERY_EDIT_SPACE)

    for (s in services.query.xwql('from doc.object(TVPris.Classes.SurgeryClass) as se').execute()) {
	def surgeryDoc = xwiki.getDocument(s)
	def surgeryObj = surgeryDoc.getObject('TVPris.Classes.SurgeryClass')
	def roRef = surgeryDoc.getDocumentReference()
	def editableRef = services.model.createDocumentReference('Editable', roRef.getParent())
	def editDoc = xwiki.getDocument(editableRef)
	def editObj = editDoc.getObject('TVPris.Classes.SurgeryPreviewClass')

	def surgerySpaceRef = services.model.createSpaceReference('' + surgeryObj.getProperty('surgery_id').getValue(), surgeryRootRef)

	def newSurgeryRef = services.model.createDocumentReference('WebHome', surgerySpaceRef)

	def publicDoc = xwiki.getDocument(services.model.createDocumentReference('PublicComments', roRef.getParent()))
	def internalDoc = xwiki.getDocument(services.model.createDocumentReference('InternalComments', roRef.getParent()))

	if (editDoc.isNew()) {
	    println("" + editableRef + " ----> " + newSurgeryRef)
	    editDoc.rename(newSurgeryRef)
	}
	if (!publicDoc.isNew()) {
	    publicDoc.rename(services.model.createDocumentReference('PublicComments', surgerySpaceRef))
	    println("" + publicDoc + " ----> " + services.model.createDocumentReference('PublicComments', surgerySpaceRef))
	}
	if (!internalDoc.isNew()) {
	    internalDoc.rename(services.model.createDocumentReference('InternalComments', surgerySpaceRef))
	    println("" + internalDoc + " ----> " + services.model.createDocumentReference('InternalComments', surgerySpaceRef))
	}
    }
}

void surgeryFakeData() {
  def random = new Random(10222)
  def nf = NumberFormat.getInstance(Locale.forLanguageTag('sv'))
  for (s in services.query.xwql('from doc.object(' + surgeryClass + ') as se').execute()) {
    println('* ' + s)
    def surgeryDoc = xwiki.getDocument(s)
    def editableRef = services.model.createDocumentReference('Editable', surgeryDoc.getDocumentReference().getParent())
    def editableDoc = xwiki.getDocument(editableRef)
    def editableObj = editableDoc.getObject(surgeryEditableClass)
    def surgeryObj = surgeryDoc.getObject(surgeryClass)

    for (int i = 0; i < 50; i++) {
      def pos = randomPosition(random)
      String queryString = "SELECT borders.name, types.name FROM gis.region_borders AS borders JOIN gis.region_types as types ON type = types.id WHERE gis.ST_Contains(border, gis.ST_Transform(gis.ST_GeometryFromText(CONCAT('POINT(', ?, ' ', ?, ')'), ?), 3857))"

      def qr = new QueryRunner('native', xwiki, xcontext)
      qr.addParameterSetter('' + pos[0])
      qr.addParameterSetter('' + pos[1])
      qr.addParameterSetter(4326)

      def r = qr.runQuery(queryString, null, null)

      List regions = new ArrayList()

      editableObj.set('longitude', pos[0])
      editableObj.set('latitude', pos[1])

      for (row in r) {
	regions.add([
		      'name': row[0],
		     'type': row[1]
		    ])
      
	if (row[1] == 'MUNICIPALITY') {
	  surgeryObj.set('municipality', row[0])
	} else if (row[1] == 'COUNTY') {
	  surgeryObj.set('county', row[0])
	}
      }
      if (regions.size() > 0) {
	surgeryDoc.save('Updated regions')
	editableDoc.save('Updated position')
	break;
      }
    }
  }
}

class CoordinatesAndRegion {

  public final static int OTHER_TYPE = 0;

  public final static int COUNTY_TYPE = 1;

  public final static int MUNICIPALITY_TYPE = 2;

  public final static int SUBLOCALITY_TYPE = 3;

  private final def services;

  private final def xwiki;

  private final String MAPS_API_KEY;

  public CoordinatesAndRegion(xwiki, services) {
    this.xwiki = xwiki;
    this.services = services;
    MAPS_API_KEY = services.tpkConfig.getGoogleMapsKey();
  }


  Map getRegions(Object coordinates, List errors)
  {
    URI uri = new URIBuilder().setScheme("http").setHost("tpk-dev.kreablo.se").setPath(xwiki.getURL('TVPris.Services.LookupRegion'))
    .addParameter("xpage", "plain")
    .addParameter("outputSyntax", "plain")
    .addParameter('srid', '4326')
    .addParameter('lon', "" + coordinates.lon)
    .addParameter('lat', "" + coordinates.lat)
    .build();

    Map regions = new HashMap()

    String json = getHttpContents(uri, errors)

    if (json == null) {
      return null
    }
  
    def jsonObj = new JSONObject(json)

    def a = jsonObj.getJSONArray('regions')
    for (int i = 0; i < a.length(); i++) {
      final def x = a.getJSONObject(i)
      final String name = x.getString('name')
      final String type = x.getString('type')

      regions.put(type, name)
    }

    return regions
  }

  String getHttpContents(URI uri, errors)
  {
    CloseableHttpClient httpclient = HttpClients.createDefault();

    HttpGet httpget = new HttpGet(uri);
    CloseableHttpResponse response = httpclient.execute(httpget);

    String content = ''

    try {
      HttpEntity entity = response.getEntity();
      if (entity != null) {
	InputStream instream = entity.getContent();
	BufferedReader reader = new BufferedReader(new InputStreamReader(instream))
	try {
	  String line = reader.readLine()
	  while (line != null) {
	    content += line
	    line = reader.readLine()
	  }
	} finally {
	  reader.close();
	}
      }
    } catch (Exception e) {
      errors.add("Caught exception: " + e)
      return null
    } finally {
      response.close()
    }
    return content
  }

  int addressComponentType(JSONObject ac) {
    JSONArray types = ac.getJSONArray('types');
    for (int i = 0; i < types.length(); i++) {
      final String type = types.getString(i);
      if ("administrative_area_level_1".equals(type)) {
	return COUNTY_TYPE;
      }
      if ("administrative_area_level_2".equals(type)) {
	return MUNICIPALITY_TYPE;
      }
      if ("sublocality_level_1".equals(type)) {
	return SUBLOCALITY_TYPE;
      }
    }
    return OTHER_TYPE;
  }

  private final Pattern postalNumberPattern = Pattern.compile("^\\s*[1-9](?:(?:\\s*)[0-9]){4}\\s*\$", Pattern.MULTILINE);

  private String extractPostalNumber(String address)
  {
    Matcher m = postalNumberPattern.matcher(address);
    if (m.find()) {
      String pnr = m.group();
      return pnr.replaceAll("\\s", "");
    }
    return null;
  }

  private String cleanAddress(String address)
  {
    Matcher m = postalNumberPattern.matcher(address);
    final String a0 = m.replaceFirst("");
    return a0.replaceAll("[\\n\\r]+", " ")
  }

  public Object getCoordinates(String address, List warnings, List errors)
  {
    return getCoordinates_(address, warnings, errors, true)
  }

  private Object getCoordinates_(String address, String zipcode, String city, List warnings, List errors, boolean usePostalNumber) {

    final String pnr = zipcode.replaceAll("\\s", "");
    final String components = "country:SE" + (pnr == null || !usePostalNumber ? "" : "|postal_code:" + pnr);

    URI uri = new URIBuilder()
    .setScheme("https")
    .setHost("maps.googleapis.com")
    .setPath("/maps/api/geocode/json")
    .addParameter("address", address + "," + city)
    .addParameter("components", components)
    .addParameter("key", MAPS_API_KEY)
    .build();

    String json = getHttpContents(uri, errors)

    def jsonObj = new JSONObject(json)

    def results = jsonObj.getJSONArray('results')

    if (results.length() > 0) {
      if (results.length() > 1) {
	warnings.add('There where more than one location matching address "' + address + "," + city  + '"')
      }
      def result0 = results.getJSONObject(0);
      if (result0 == null) {
	return null;
      }
      def location = result0.getJSONObject('geometry').getJSONObject('location')
      final Object ret = new Object() {
	public final double lat = location.getDouble('lat');
	public final double lon = location.getDouble('lng');
	public String county = null;
	public String municipality = null;
	public String sublocality = null;
      };

      URI uri1 = new URIBuilder()
      .setScheme("https")
      .setHost("maps.googleapis.com")
      .setPath("/maps/api/geocode/json")
      .addParameter("latlng", "" + ret.lat + "," + ret.lon)
      .addParameter("language", "sv")
      .addParameter("key", MAPS_API_KEY)
      .build();

      String json1 = getHttpContents(uri1, errors)

      def jsonObj1 = new JSONObject(json1)
      def results1 = jsonObj1.getJSONArray('results')

      for (int j = 0; j < results1.length(); j++) {
	def result1 = results1.getJSONObject(j)
	JSONArray addressComponents = result1.getJSONArray('address_components')
	for (int i = 0; i < addressComponents.length(); i++) {
	  final JSONObject ac = addressComponents.getJSONObject(i);
	  switch (addressComponentType(ac)) {
	  case OTHER_TYPE:
	  break;
 	  case COUNTY_TYPE:
	  ret.county = ac.getString("long_name");
	  break;
	  case MUNICIPALITY_TYPE:
	  ret.municipality = ac.getString("long_name");
	  if (ret.municipality == "Malung" || ret.municipality == "Sälen") {
	    ret.municipality = "Malung-Sälen";
	  }
	  break;
	  case SUBLOCALITY_TYPE:
	  ret.sublocality = ac.getString("long_name");
	  break;
	  default:
	  break;
	  }
	}
      }
      return ret;
    }
    if (usePostalNumber && pnr != null)  {
      /* Fallback to ignoring postall number */
      return getCoordinates_(address, zipcode, city, warnings, errors, false)
    }
    warnings.add('No location matched address "' + address + "," + city + '"')
    return null
  }

  private boolean update(obj, String propname, Object value) {
    Object prop = obj.getProperty(propname);

    if (prop == null) {
      if (value != null) {
	obj.set(propname, value)
	return true
      }
      return false
    }
    
    Object cur = prop.getValue();
    if (value != null && !value.equals(cur)) {
      obj.set(propname, value);
      return true;
    }
    return false;
  }

  public boolean updateCoordinatesAndRegion(final int surgeryid, final List warnings, final List errors) {

    boolean updated = false;
    boolean editableUpdated = false;

    def q = services.query.xwql('SELECT doc.fullName, edoc.fullName FROM Document AS doc, Document AS edoc, doc.object(TVPris.Classes.SurgeryClass) AS s, edoc.object(TVPris.Classes.SurgeryEditableClass) AS e WHERE e.id = s.surgery_editable_id AND s.surgery_id = :surgeryid').bindValue('surgeryid', surgeryid);
    for (res in q.execute()) {
      def sn = res[0]
      def en = res[1]
      def surgeryDoc = xwiki.getDocument(sn)
      def surgeryObj = surgeryDoc.getObject('TVPris.Classes.SurgeryClass')
      def surgeryEditDoc = xwiki.getDocument(en)
      def surgeryEditObj = surgeryEditDoc.getObject('TVPris.Classes.SurgeryEditableClass')
      def stuff = getCoordinates(surgeryEditObj.getProperty('address').getValue(),
				 surgeryEditObj.getProperty('zipcode').getValue(),
				 surgeryEditObj.getProperty('city').getValue(), warnings, errors)
      if (stuff != null) {
	editableUpdated |= update(surgeryEditObj, 'latitude', stuff.lat);
	editableUpdated |= update(surgeryEditObj, 'longitude', stuff.lon);
	updated |= update(surgeryObj, 'county', stuff.county);
	updated |= update(surgeryObj, 'municipality', stuff.municipality);
	updated |= update(surgeryObj, 'sublocality', stuff.sublocality);
	if (updated) {
	  surgeryDoc.save("Updated region and/or coordinates");
	}
	if (editableUpdated) {
	  if (!updated || !surgeryDoc.getDocumentReference().equals(surgeryEditDoc.getDocumentReference())) {
	    surgeryEditDoc.save("Updated region and/or coordinates");
	  }
	}
      }
    }
    return updated || editableUpdated;
  }
}

void updateRegionAndCoordinates() {
  final CoordinatesAndRegion coordinatesAndRegion = new CoordinatesAndRegion(xwiki, services);

  final List warnings = new ArrayList();
  final List errors = new ArrayList();

  for (surgeryid in services.query.xwql('SELECT s.surgery_id FROM Document doc, doc.object(TVPris.Classes.SurgeryClass) AS s').execute()) {
    
    boolean updated = coordinatesAndRegion.updateCoordinatesAndRegion(surgeryid, warnings, errors);

    if (updated) {
      println("* " + surgeryid + " was updated")
    }
  }

  println("* warnings: " + warnings)
  println("* errors: " + errors)
}


void splitAddress() {
  for (editDocName in services.query.xwql('SELECT doc.fullName FROM Document doc, doc.object(TVPris.Classes.SurgeryEditableClass) AS s').execute()) {
    def editDoc = xwiki.getDocument(editDocName)
    def editObj = editDoc.getObject('TVPris.Classes.SurgeryEditableClass')
    def addressProp = editObj.getProperty('address')
    if (addressProp && addressProp.getValue()) {
      def parts = addressProp.getValue().split("[\\n\\r]+")
      if  (parts.size() != 3) {
	println('* {{error}}The address ((("' + addressProp.getValue() + '"))) have ' + parts.size() + ' components{{/error}}')
      } else {
	editObj.set('address', parts[0])
	editObj.set('zipcode', parts[2])
	editObj.set('city', parts[1])

	editDoc.save('Split address')
      }
    }
  }
}

void testOperation() {
  CSVMapper csvMapper = new CSVMapper(this, xwiki, services, xcontext, Locale.forLanguageTag('sv'), 'tvpris_itemprice')
}

void upgradeMeasureVersions() {
  final int version = getVersion();
  Iterable<VersionedObject> measureIterable = new VersionedSpaceClassIterable(this, version - 1, measureClass);
  for (vo in measureIterable) {
    vo.setVersion(version);
  }
}

interface VersionedObject {

  void setVersion(Integer version);
  
}

class VersionedSpaceClassIterable implements Iterable<VersionedObject> {

  private final def script;

  private final Integer version = version;

  private final String className;
  
  public VersionedSpaceClassIterable(script, Integer version, String className) {
    this.script = script;
    this.version = version;
    this.className = className;
  }

  @Override
  public Iterator<VersionedObject> iterator() {
    final def query = this.script.services.query
       .xwql('from doc.object(' + className + ') as m where m.version = :version')
       .bindValue('version', this.version);

    final def iterator = query.execute().iterator();
    final def xwiki = this.script.xwiki

    return new Iterator<VersionedObject>() {

      @Override
      public VersionedObject next() {
	return new VersionedSpaceClassObj(script, iterator.next(), className)
      }

      @Override
      public boolean hasNext() {
	return iterator.hasNext()
      }

      @Override
      public void remove() {
	iterator.remove()
      }
      
    };
    
  }

  private static class VersionedSpaceClassObj implements VersionedObject {

    private final def mDocName

    private final String className

    private final def script
    
    VersionedSpaceClassObj(script, mDocName, className) {
      this.mDocName = mDocName
      this.className = className
      this.script = script
    }

    @Override
    public void setVersion(Integer version) {
      final def mDoc = script.xwiki.getDocument(mDocName)
      final def ref = mDoc.getDocumentReference()
      final def versionRef = ref.getParent()
      final def newVersionRef = new SpaceReference("Version" + version, versionRef.getParent())
      final def newRef = new DocumentReference(ref.getName(), newVersionRef)
      final def newDoc = script.xwiki.getDocument(newRef)
      final def m = mDoc.getObject(className)
      final def mNew = newDoc.getObject(className, true)
      script.copyObjectProperties(m, mNew);
      mNew.set('version', version)
      newDoc.save("ObjectImport upgraded to version " + version)
    }
    
  }
  
}

{{/groovy}}
