
CREATE SCHEMA gis;
SET search_path=gis;
CREATE EXTENSION postgis;
CREATE EXTENSION postgres_fdw;
CREATE SERVER osm FOREIGN DATA WRAPPER postgres_fdw OPTIONS ( dbname 'gis' );
CREATE USER MAPPING FOR current_user SERVER osm;

-- ST_Contains won't work when called from any other schema.
ALTER FUNCTION gis.ST_Contains(geometry, geometry) SET search_path=gis;
ALTER FUNCTION gis.ST_Transform(geometry, integer) SET search_path=gis;
ALTER FUNCTION gis.ST_GeometryFromText(text, integer) SET search_path=gis;

CREATE FOREIGN TABLE polygon (
    admin_level text,
    boundary text,
    name text,
    way geometry(Geometry,3857)
) SERVER osm
  OPTIONS (schema_name 'public', table_name 'planet_osm_polygon');

CREATE TABLE region_types (
  id INTEGER PRIMARY KEY,
  name VARCHAR(16)
);

CREATE INDEX region_types_name_index ON region_types (name);

CREATE TABLE region_borders (
  id SERIAL PRIMARY KEY,
  type INTEGER NOT NULL REFERENCES region_types(id),
  name VARCHAR(256) NOT NULL,
  parent_type INTEGER,
  parent_name VARCHAR(256),
  border geometry(MULTIPOLYGON, 3857) NOT NULL,
  UNIQUE (type, name),
  FOREIGN KEY(parent_type, parent_name) REFERENCES region_borders (type, name)
);

CREATE INDEX region_borders_type_and_name ON region_borders (type, name);
CREATE INDEX region_borders_border_index ON region_borders USING GIST (border);

GRANT USAGE ON SCHEMA gis              TO xwiki;
GRANT SELECT ON gis.region_borders     TO xwiki;
GRANT SELECT ON gis.region_types       TO xwiki;
GRANT SELECT ON gis.geometry_columns   TO xwiki;
GRANT SELECT ON gis.region_types TO xwiki;

INSERT INTO region_types VALUES (0, 'COUNTRY');       -- SV: Land
INSERT INTO region_types VALUES (1, 'COUNTY');        -- SV: Län
INSERT INTO region_types VALUES (2, 'MUNICIPALITY');  -- SV: Kommun
INSERT INTO region_types VALUES (3, 'CITY');          -- SV: Stad
INSERT INTO region_types VALUES (4, 'DISTRICT');      -- SV: Stadsdel

INSERT INTO region_borders (type, name, parent_type, parent_name, border) SELECT 0, name, NULL, NULL, ST_Collect(way) FROM polygon WHERE boundary = 'administrative' AND admin_level = '2' AND name = 'Sverige' GROUP BY name ORDER BY name;
INSERT INTO region_borders (type, name, parent_type, parent_name, border) SELECT 1, name, 0, 'Sverige', ST_Collect(way) FROM polygon WHERE boundary = 'administrative' AND admin_level = '4' AND name like '% län' GROUP BY name ORDER BY name;
INSERT INTO region_borders (type, name, border) SELECT 2, name, ST_Collect(way) FROM polygon WHERE boundary = 'administrative' AND admin_level = '7' AND name like '% kommun' GROUP BY name ORDER BY name;
UPDATE region_borders AS m SET parent_type = ptype, parent_name = pname
FROM (SELECT c.type AS ptype, c.name AS pname, c.border AS pborder FROM region_borders AS c WHERE c.type = 1) AS p WHERE m.type = 2 AND ST_Contains(pborder, m.border);
UPDATE region_borders SET parent_type=1, parent_name='Jönköpings län' WHERE type=2 AND name='Mullsjö kommun';  -- Mullsjö kommun have a small area inside Västra götalands län.  So, no county "contains" Mullsjö kommun.


select name, type from region_borders where ST_Contains(border, ST_GeometryFromText('POINT(1908762.1242920565 8551580.215394225)', 3857));


sudo -u postgres psql -q gis <<'EOF' > counties.csv
\t
copy (select name, 1, way from planet_osm_polygon where admin_level='4' and boundary = 'administrative' and name like '% län' order by name) to stdout with csv;
EOF
'
sudo -u postgres psql -q border_test <<'EOF' < counties.csv
\t
copy region_borders from stdin with csv;
EOF
