/* eslint-env browser, amd, es6 */
define([
    "jquery",
    "Highlight",
    "Notifyer",
    "bootstrap"
], (jQuery, Highlight, Notifyer) => {
    "use strict";
    const KEYS = {
        "BACKSPACE": 8,
        "TAB": 9,
        "ENTER": 13,
        "SHIFT": 16,
        "CTRL": 17,
        "ALT": 18,
        "ESC": 27,
        "SPACE": 32,
        "PAGE_UP": 33,
        "PAGE_DOWN": 34,
        "END": 35,
        "HOME": 36,
        "LEFT": 37,
        "UP": 38,
        "RIGHT": 39,
        "DOWN": 40,
        "DELETE": 46
    };

    const onlyDigits = new RegExp("^\\s*\\d+\\s*$");

    const KEYPRESS_DELAY = 30;

    const triggerKeyboardEvent = (el, keyCode) => {
        const eventObj = document.createEventObject
            ? document.createEventObject()
            : document.createEvent("Events");

        if (eventObj.initEvent) {
            eventObj.initEvent("keydown", true, true);
        }

        eventObj.keyCode = keyCode;
        eventObj.which = keyCode;

        if (typeof el.dispatchEvent === "function") {
            el.dispatchEvent(eventObj);
        } else {
            el.fireEvent("onkeydown", eventObj);
        }
    };

    const defaultOptions = {
        "multi": false,
        "errbox": jQuery([]),
        "matchEnd": false,
        "multiSelectTextTemplate": "{} items selected"
    };

    const isCollapsedToggle = (element) => jQuery(element).
        hasClass("structured-combobox-toggle") &&
          jQuery(element).parent().hasClass("collapsed");

    function StructuredCombobox (jq, opts) {
        this.opts = jQuery.extend({}, defaultOptions, opts);
        this.toggle = jq;
        this.element = this.toggle.parent();
        this.menu = jQuery(this.element).find(".dropdown-menu");
        const searchElement = this.element.find(".structured-combobox-search");
        const searchInput = searchElement.find("input");
        this.searchInput = searchInput;
        this.search = searchElement;
        this.candidate = false;
        const self = this;
        searchInput.focus(() => {
            if (this.candidate) {
                this.candidate.addClass("candidate");
            }
        });
        searchInput.blur(() => {
            if (this.candidate) {
                this.candidate.removeClass("candidate");
            }
        });
        this.menu.each((index, element) => {
            element.addEventListener(
                "keydown",
                (event) => {
                    const key = event.which;
                    if ([
                        KEYS.ESC,
                        KEYS.UP,
                        KEYS.DOWN,
                        KEYS.SPACE,
                        KEYS.TAB,
                        KEYS.ENTER
                    ].indexOf(key) < 0 &&
                        !(/input/i).test(event.target.tagName)) {
                        searchInput.focus();
                        searchInput.
                            each((_index, s) => triggerKeyboardEvent(s, key));
                    } else if (key === KEYS.DOWN) {
                        if (isCollapsedToggle(event.target)) {
                            jQuery(event.target).parent().
                                next().next().find("a:first").focus();
                            event.stopPropagation();
                            event.preventDefault();
                        } else {
                            const s = jQuery(event.target).parents(".structured-combobox-search");
                            if (s.length > 0) {
                                s.next().next().find("a:first").focus();
                                event.stopPropagation();
                                event.preventDefault();
                            }
                        }
                    } else if (key === KEYS.UP) {
                        const pprev = jQuery(event.target).parent().
                            prev().prev();
                        const prev = pprev.find("a:first");
                        if (isCollapsedToggle(prev)) {
                            prev.focus();
                            event.stopPropagation();
                            event.preventDefault();
                        } else if (pprev.hasClass("structured-combobox-search")) {
                            pprev.find("input").focus();
                            event.stopPropagation();
                            event.preventDefault();
                        }
                    } else if (key === KEYS.ESC) {
                        event.stopPropagation();
                        event.preventDefault();
                        self.close();
                    } else if (key === KEYS.ENTER) {
                        if ((/input/i).test(event.target.tagName)) {
                            self.selectCandidate();
                            event.stopPropagation();
                            event.preventDefault();
                        }
                    }
                },
                true
            );
        });
        searchElement.each((index, element) => {
            let hadFocusOnMousedown = false;
            element.addEventListener(
                "mousedown",
                () => {
                    hadFocusOnMousedown = this.searchInput.is(":focus");
                    return null;
                },
                true
            );
            element.addEventListener(
                "click",
                (e) => {
                    try {
                        if (!hadFocusOnMousedown) {
                            this.searchInput.focus();
                        } else {
                            this.close();
                        }
                    } finally {
                        hadFocusOnMousedown = false;
                    }
                    e.preventDefault();
                    e.stopPropagation();
                },
                true
            );
        });
        this.menu.click(() => false);
        this.menu.find("li.structured-combobox-item a, li.structured-combobox-item, li.structured-combobox-item marker").click((event) => this._select(event.target));
        const ph = this.toggle.attr("data-placeholder");
        this.originalText = isEmpty(ph) ? this.toggle.text() : ph;
        this.delayTimeout = null;
        this.opening = false;
        this.closing = false;

        this.toggle.dropdown();
        this.toggle.on("mousedown", () => false);
        this.toggle.on("focusin", (e_) => setTimeout(() => this.open(), KEYPRESS_DELAY));
        this.searchInput.on("keydown", (event) => this.handleKeypress(event));
        this.initSections();
        this.values = new Set();
        this.element.on("shown.bs.dropdown", () => {
            this.searchInput.val("");
            this.searchInput.focus();
            this.delayAndFilter(true);
            this.clearInvalid();
        });
        this.element.on("hidden.bs.dropdown", () => {
            jQuery(window).scrollTop(this.scrollTop);
            if (this.values.size === 0) {
                this.selectCandidate();
            }
            if (!isEmpty(this.val())) {
                this.element.trigger("scb-done");
            }
        });


        this.menu.on("disabled_keydown", (event) => {
            const key = event.which;
            if ([
                KEYS.ESC,
                KEYS.UP,
                KEYS.DOWN,
                KEYS.SPACE,
                KEYS.TAB,
                KEYS.ENTER
            ].indexOf(key) < 0 &&
                !(/input/i).test(event.target.tagName)) {
                this.searchInput.
                    each((_index, s) => triggerKeyboardEvent(s, key));
            }
        });

    }

    const scp = StructuredCombobox.prototype;

    const toArray = (s) => {
        const a = [];
        s.forEach((e) => a.push(e));
        return a;
    };

    scp.handleKeypress = function handleKeypress (evt) {
        const key = evt.which;

        if (this.isOpen()) {
            const navkeys = [KEYS.ESC, KEYS.UP, KEYS.DOWN, KEYS.TAB];
            if (navkeys.indexOf(key) >= 0) {
                this.search.each((
                    _index,
                    search
                ) => triggerKeyboardEvent(
                    search,
                    key === KEYS.DOWN ? KEYS.TAB : key
                ));
            } else if (key === KEYS.ENTER) {
                evt.preventDefault();
                this.search.each((
                    _index,
                    search
                ) => triggerKeyboardEvent(search, KEYS.DOWN));
                const active = this.menu.find("li.structured-combobox-item a:focus");
                if (active.length > 0) {
                    active.click();
                }
            } else if (key === KEYS.SPACE && evt.ctrlKey) {
                this.trigger("results:toggle", {});

                evt.preventDefault();
            } else {
                this.delayAndFilter(false);
            }
        }
    };

    scp.isOpen = function isOpen () {
        return this.element.hasClass("open");
    };

    const itemValue = (item) => {
        let value = item.getAttribute("data-value");
        if (isJust(value)) {
            return value;
        }
        value = jQuery(item).parent().attr("data-value");
        if (isJust(value)) {
            return value;
        }
        value = jQuery(item).find("[data-value]").attr("data-value");
        if (isJust(value)) {
            return value;
        }
        value = jQuery(item).parent().parent().attr("data-value");
        if (isJust(value)) {
            return value;
        }
        return "";
    };


    scp._select = function _select (item) {
        this.select(item);
        if (!this.opts.multi) {
            this.close();
            jQuery(".dropdow-backdrop").remove();
        }
        return false;
    };

    scp.select = function select (item) {
        const value = itemValue(item);
        if (this.opts.multi) {
            this.searchInput.val("");
        } else {
            this.element.dropdown("toggle");
            this.values.clear();
        }
        if (this.values.has(value)) {
            this.values["delete"](value);
        } else {
            this.values.add(value);
        }
        this.markSelected();
        this.clearInvalid();
        return false;
    };

    scp.val = function val (newVal) {
        if (typeof newVal !== "undefined") {
            this.values.clear();
            if (Array.isArray(newVal)) {
                for (let i = 0; i < newVal.length; i += 1) {
                    if (newVal[i] !== "") {
                        this.values.add(newVal[i]);
                    }
                }
            } else if (!isEmpty(newVal)) {
                this.values.add(newVal);
            }
            this.markSelected();
            this.expandSelectedSections();
        }

        return Array.isArray(this.values) ? this.values : toArray(this.values);
    };


    scp.markSelected = function markSelected () {
        if (this.values.size === 0) {
            this.toggle.find(".button-text").text(this.originalText);
            this.toggle.addClass("placeholder");
        } else if (this.values.size === 1) {
            const a = toArray(this.values);
            let e = null;
            if (this.opts.matchEnd) {
                e = this.menu.find("[data-value$=\"" +
                                   a[0] + "\"]");
            } else {
                e = this.menu.find("[data-value=\"" +
                                   a[0] + "\"]");
            }
            this.toggle.find(".button-text").text(e.text());
            this.toggle.removeClass("placeholder");
        } else {
            this.toggle.find(".button-text").
                text(this.opts.multiSelectTextTemplate.
                    replace("{}", this.values.size));
            this.toggle.removeClass("placeholder");
        }

        this.menu.find(".structured-combobox-item a").each((_index, element) => {
            if (this.values.has(element.getAttribute("data-value"))) {
                jQuery(element).addClass("selected");
            } else {
                jQuery(element).removeClass("selected");
            }
        });
        this.element.trigger("change");
    };

    scp.delayAndFilter = function delayAndFilter (expandSelected) {

        if (this.delayTimeout !== null) {
            clearTimeout(this.delayTimeout);
        }
        this.delayTimeout = setTimeout(() => {
            if (this.candidate) {
                this.candidate.removeClass("candidate");
            }
            this.candidate = false;
            this.filter(this.searchInput.val());
            this.delayTimeout = null;
            if (expandSelected) {
                this.expandSelectedSections();
            }
        }, KEYPRESS_DELAY);
    };

    scp.open = function open () {
        if (!this.opening && !this.isOpen()) {
            try {
                this.scrollTop = jQuery(window).scrollTop();
                this.opening = true;
                this.toggle.dropdown("toggle");
            } finally {
                this.opening = false;
            }
        }
    };

    scp.close = function close () {
        if (!this.closing && this.isOpen()) {
            try {
                this.closing = true;
                this.toggle.dropdown("toggle");
            } finally {
                this.closing = false;
            }
        }
    };

    scp.filter = function filter (q) {
        const disabledSections = [];
        const enabledSections = [];
        let sectionToggle = null;
        let sectionEnabled = false;
        let headerMatch = false;
        let sw = 0;

        const pushSection = () => {
            if (sectionToggle !== null) {
                if (sectionEnabled) {
                    sectionToggle.attr("data-match-weight", sw);
                    enabledSections.push(sectionToggle);
                } else {
                    sectionToggle.attr("data-match-weight", 0);
                    disabledSections.push(sectionToggle);
                }
                sw = 0;
            }
        };

        const items = this.menu.find(".structured-combobox-item, .structured-combobox-header");
        for (let i = 0; i < items.length; i += 1) {
            const item = jQuery(items[i]);
            let hl = item.data("scb-highlighter");
            if (isNothing(hl)) {
                const root = item.hasClass("structured-combobox-header")
                    ? item.find("a") : item;
                if (root.length > 0) {
                    hl = new Highlight({
                        "root": root.get(0),
                        "classname": "scb-highlight",
                        "delimiter": /[\s]+/
                    });
                    item.data("scb-highlighter", hl);
                }
            }

            const match = hl && hl.match(q);
            const w = hl.weight();
            item.attr("data-match-weight", w);

            if (item.hasClass("structured-combobox-header")) {
                pushSection();
                sectionToggle = item;
                sectionEnabled = match;
                headerMatch = match && !q.match(onlyDigits);
            } else if (match || headerMatch) {
                sectionEnabled = true;
                item.removeClass("disabled");
            } else {
                item.addClass("disabled");
            }
            sw = w > sw ? w : sw;
        }
        pushSection();
        this.sortItems();
        for (let i = 0; i < enabledSections.length; i += 1) {
            if (Highlight.prototype.pattern(q).length > 0) {
                enabledSections[i].trigger("structured-combobox-section-expand");
            } else {
                enabledSections[i].trigger("structured-combobox-section-collapse");
            }
            enabledSections[i].removeClass("disabled");
            enabledSections[i].prev().removeClass("disabled");
        }
        for (let i = 0; i < disabledSections.length; i += 1) {
            disabledSections[i].trigger("structured-combobox-section-collapse");
            disabledSections[i].addClass("disabled");
            disabledSections[i].prev().addClass("disabled");
        }
    };

    scp.sortItems = function sortItems () {
        const sort = (index, element) => {
            const group = jQuery(element);
            const items = group.children("[data-match-weight]");
            const is = [];
            items.each((index1, element1) => {
                const h = document.createElement("li");
                is.push(h);
                element1.parentNode.replaceChild(h, element1);
            });
            items.sort((a, b) => {
                const wa = a.getAttribute("data-match-weight");
                const wb = b.getAttribute("data-match-weight");
                if (wb < wa) {
                    return -1;
                }
                if (wb > wa) {
                    return 1;
                }
                return jQuery(wa).text().localeCompare(jQuery(wb).text());
            });
            items.each((index1, element1) => {
                const h = is.shift();
                h.parentNode.replaceChild(element1, h);
            });
        };
        this.menu.each(sort);
        this.menu.find("ul").each(sort);
        const l = this.menu.find("ul").children();
        if (l.length > 0 && !l.get(0).classList.contains("disabled")) {
            this.candidate = jQuery(l.get(0));
            this.candidate.addClass("candidate");
        }
    };

    scp.selectCandidate = function selectCandidate () {
        if (this.candidate) {
            const a = this.candidate.find("a");
            if (a.length > 0) {
                this._select(a.get(0));
            }
        }
    };

    scp.expandSelectedSections = function expandSelectedSections () {
        this.sections.forEach((section) => {
            const selectedItems = section.items.find("a.selected");
            if (selectedItems.length > 0) {
                section.toggle.trigger("structured-combobox-section-expand");
            } else {
                section.toggle.trigger("structured-combobox-section-collapse");
            }
        });
    };

    scp.initSections = function initSections () {
        const sections = new Map();

        let sectionName = null;
        let sectionToggle = null;
        let sectionCollapse = null;
        let sectionItems = [];

        const expand = (section) => {
            section.collapse.addClass("collapse");
            section.collapse.addClass("in");
            section.toggle.attr("aria-expanded", "true");
            section.toggle.removeClass("collapsed");
        };
        const collapse = (section) => {
            if (this.opts.multi && section.items.find("a.selected").length > 0) {
                return;
            }
            section.collapse.addClass("collapse");
            section.collapse.removeClass("in");
            section.toggle.attr("aria-expanded", "false");
            section.toggle.addClass("collapsed");
        };
        const toggle = (section) => {
            if (section.toggle.attr("aria-expanded") === "true") {
                collapse(section);
            } else {
                expand(section);
            }
        };

        const pushSection = () => {
            if (sectionName !== null) {
                const section = {
                    "name": sectionName,
                    "toggle": sectionToggle,
                    "collapse": sectionCollapse,
                    "items": jQuery(sectionItems)
                };
                sections.set(sectionName, section);
                sectionToggle.on("structured-combobox-section-expand", () => expand(section));
                sectionToggle.on("structured-combobox-section-collapse", () => collapse(section));
                sectionToggle.click(() => toggle(section));
                sectionItems = [];
                if (section.toggle.attr("aria-expanded") !== "true") {
                    collapse(section);
                }
            }
        };

        const items = this.menu.find(".structured-combobox-item, .structured-combobox-header");
        for (let i = 0; i < items.length; i += 1) {
            const item = jQuery(items[i]);
            if (item.hasClass("structured-combobox-header")) {
                pushSection();
                sectionToggle = item;
                sectionName = item.text();
                sectionCollapse = item.find("ul");
            } else {
                sectionItems.push(items[i]);
            }
        }
        pushSection();

        this.sections = sections;
    };

    scp.relabel = function relabel (oid, nid) {
        jQuery("label[for=\"" + oid + "\"]").
            each((index, element) => element.setAttribute("for", nid));
    };

    scp.markInvalid = function markInvalid (msg) {
        this.toggle.addClass("invalid");
        this.opts.errbox.text(msg);
        Notifyer.error(msg);
        this.opts.errbox.show();
    };

    scp.clearInvalid = function clearInvalid () {
        this.toggle.removeClass("invalid");
        this.opts.errbox.text("");
        this.opts.errbox.hide();
    };

    return StructuredCombobox;
});
