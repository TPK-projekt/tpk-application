/* eslint-env browser, amd, es6 */
define([
    "jquery",
    "EventBus",
    "Notifyer",
    "History",
    "SelectionPresenter",
    "StructuredCombobox"
], (
    jQuery,
    EventBus,
    notifyer,
    history,
    SelectionPresenter,
    StructuredCombobox
) => {
    "use strict";

    const defaults = {};

    function Position (options) {
        const o = (n) => {
            if (isUndefined(options[n])) {
                if (isUndefined(defaults[n])) {
                    throw new Error("Missing mandatory option: " + n);
                }
                this[n] = defaults[n];
            } else {
                this[n] = options[n];
            }
        };

        o("block");
        o("unblock");
        o("sublocalityInput");
        o("municipalityInput");
        o("countyInput");
        o("inputVal");

        const positionChangeEvents = "change:" + this.sublocalityInput.id +
              " change:" + this.municipalityInput.id +
              " change:" + this.countyInput.id;

        const OTHER_TYPE = 0;

        const COUNTY_TYPE = 1;

        const MUNICIPALITY_TYPE = 2;

        const SUBLOCALITY_TYPE = 3;

        const setVal = (input, val) => {
            if (!isEmpty(val)) {
                this.inputVal(input, val);
            } else {
                this.inputVal(input, null);
            }
        };

        const positionToggle = jQuery("#structured-combobox-button-position");
        const positionCombo = new StructuredCombobox(positionToggle, {
            "matchEnd": true,
            "multi": true,
            "multiSelectTextTemplate": "{} kommuner eller stadsdelar valda."
        });
        positionCombo.relabel("position-input", "structured-combobox-button-position");

        new SelectionPresenter({
            "container": jQuery(".location-presenter"),
            "events": positionChangeEvents,
            "delimiter": ", ",
            "placeHolder": "Hela landet",
            "source": () => {
                const vs = positionCombo.val();
                const ls = [];
                for (let i = 0; i < vs.length; i += 1) {
                    const [c, m, s] = vs[i].split("|");
                    if (isJust(s)) {
                        ls.push(m + " - " + s);
                    } else if (isJust(m)) {
                        ls.push(m);
                    } else if (isJust(c)) {
                        ls.push(c);
                    }
                }
                return ls;
            }
        });

        const ARRAY_DELIM = "$services.tpkCompare.ARRAY_DELIM";

        const arrayParse = (s) => {
            if (isJust(s)) {
                if (s === "") {
                    return [];
                }
                return s.split(ARRAY_DELIM);
            }
            return null;
        };

        const arraySerialize = (a) => {
            if (isJust(a)) {
                return a.join(ARRAY_DELIM);
            }
            return null;
        };

        history.register({
            "load": (v) => positionCombo.val(v),
            "save": () => positionCombo.val(),
            "parse": arrayParse,
            "serialize": arraySerialize,
            "name": "loc"
        });

        const toArray = (s) => {
            const a = [];
            s.forEach((v) => a.push(v));
            return a;
        };

        const setLocations = (vs) => {
            if (!isJust(vs)) {
                setVal(this.countyInput, null);
                setVal(this.municipalityInput, null);
                setVal(this.sublocalityInput, null);
            } else {
                const cs = new Set();
                const ms = new Set();
                const sls = new Set();
                for (let i = 0; i < vs.length; i += 1) {
                    const [c, m, s] = vs[i].split("|");
                    if (!isEmpty(c)) {
                        cs.add(c);
                    }
                    if (!isEmpty(m)) {
                        ms.add(m);
                    }
                    if (!isEmpty(s)) {
                        sls.add(s);
                    }
                }
                setVal(this.countyInput, toArray(cs));
                setVal(this.municipalityInput, toArray(ms));
                setVal(this.sublocalityInput, toArray(sls));
            }
        };

        positionCombo.element.on("change", () => setLocations(positionCombo.val()));

        this.validate = () => {
            const s = this.inputVal(this.countyInput);
            if (!isEmpty(s)) {
                return true;
            }

            positionCombo.markInvalid("Välj en kommun eller stadsdel!");

            return false;
        };

        EventBus.on(positionChangeEvents, () => EventBus.trigger("parameters-updated"));
    }

    return Position;
});
