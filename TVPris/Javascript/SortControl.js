/*
 *  Copyright (C) 2017  Andreas Jonsson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

(function (factory) {
    if (typeof define === 'function' && define.amd) {
	define(['jquery'], factory);
    } else {
	factory(jQuery);
    }
})(function ($) {

    var defaults = {
        className: 'sort-control',
        stateAsc:  'state-asc',
        stateDesc: 'state-desc',
	eventBus: null,
        state: null
    };

    function SortControl(options) {
        var self = this;
        var o = function(n) {
            if (typeof(options[n]) === 'undefined') {
                if (typeof(defaults[n]) === 'undefined') {
                    throw "Missing mandatory option: " + n;
                }
                self[n] = defaults[n];
            } else {
                self[n] = options[n];
            }
        }
        o('className');
        o('stateAsc');
        o('stateDesc');
        o('selector');
        o('state');
	o('eventBus');

	if (this.eventBus === null) {
	    this.eventBus = jQuery(this);
	}

	this.on();
    }

    var p = SortControl.prototype;

    p.each = function(f) {
        $(this.selector).find('.' + this.className).each(f);
    }

    p.setState = function(field, order) {
        var self = this;
        this.each(function () {
            $(this).removeClass(self.stateAsc + ' ' + self.stateDesc);
            if ($(this).attr('data-field') === field) {
                if (order === 'asc') {
                    $(this).addClass(self.stateAsc);
                } else if (order === 'desc') {
                    $(this).addClass(self.stateDesc);
                }
            }
        });
        this.state = [field, order];
        this.eventBus.trigger('sort-order-change', this.state);
    }

    p.toggle = function (field) {
        if (this.state === null || this.state[0] != field) {
            this.state = [field, null];
        }
        this.setState(field, this.state[1] === 'desc' ? 'asc' : 'desc');
    }

    p.off = function () {
        this.each(function () {
            var self = this;
            $(this).off('click');
	});
    }

    p.on = function () {
	if (this.state !== null) {
            this.setState(this.state[0], this.state[1]);
        }
        var sc = this;
        this.each(function () {
            var self = this;
            $(this).click(function () {
                var field = $(self).attr('data-field');
                sc.toggle(field);
            });
        });
    }

    return SortControl;
});
