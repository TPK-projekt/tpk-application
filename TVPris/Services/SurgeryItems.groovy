{{include reference="TVPris.Code.RunQuery"/}}{{groovy}}
import org.xwiki.velocity.tools.JSONTool

def plainMode = request.get('outputSyntax') == 'plain' && request.get('xpage') == 'plain'
def debug = false

if (plainMode) {
  response.setContentType("application/json")
}

def retVal = new HashMap<String, Object>()

Integer limit = null
Integer offset = null
Integer sid = null
boolean wantComment = true
Integer itemCount = null
if (request.get('count')) {
    try {
        itemCount = Integer.parseInt(request.get('count'))
    } catch (NumberFormatException e) {
    }
}

QueryRunner queryRunner = new QueryRunner('native', xwiki, xcontext)

if (request.get('limit')) {
    try {
        limit = Integer.parseInt(request.get('limit'))
    } catch (NumberFormatException e) {
    }
}
if (request.get('offset')) {
    try {
        offset = Integer.parseInt(request.get('offset'))
    } catch (NumberFormatException e) {
    }
}
if (request.get('sid')) {
    try {
        sid = Integer.parseInt(request.get('sid'))
    } catch (NumberFormatException e) {
    }
}
String typeRestriction = null
if (request.get('type') == 'm') {
    typeRestriction = 'is_measure'
} else if (request.get('type') == 'p') {
    typeRestriction = 'NOT is_measure '
}
String sort_order = 'ASC'
if (request.get('sort-order') == 'desc') {
    sort_order = 'DESC NULLS LAST'
}
String sort_field = 'code'
if (request.get('sort-field') == 't' ) {
    sort_field = 'title'
}
if (request.get('sort-field') == 'price' ) {
    sort_field = 'tvpip_price'
}
if (request.get('sort-field') == 'deviation') {
    sort_field = 'deviation'
}

if (sid == null) {
    retVal.put('error', 'No surgery id specified');
} else {

    StringBuilder qb = new StringBuilder();

    def columns = [
        ['code',                    'tvpi_code'],
        ['is_measure',              'is_measure'],
        ['title',                   'tvpi_title'],
        ['description',             'tvpi_description'],
        ['price',                   'tvpip_price'],
        ['surgery_price_comment',   'tvppc_text'],
        ['reference_price',         'tvpi_price'],
        ['version',                 'tvpip_version'],
        ['deviation',               'cast(abs(tvpi_price - tvpip_price) as double precision)/ tvpi_price as deviation']
    ]

    qb.append("WITH version (version) AS (SELECT tpkv_version  FROM tvpris_versionclass JOIN xwikiobjects USING (xwo_id) WHERE xwo_name = ?) SELECT ")
    queryRunner.addParameterSetter(services.tpkCompare.VERSION_DOCUMENT)
    boolean first = true
    for (c in columns) {
        if (first) {
            first = false
        } else {
            qb.append(', ')
        }
        qb.append(c[1])
    }

    qb.append(' FROM')
    qb.append('  version JOIN tvpris_item ON tvpi_version = version')
    qb.append(" LEFT OUTER JOIN (SELECT tvpip_price, tvpip_item_id, tvpip_version, tvpip_is_measure FROM tpk_itemprice WHERE tvpip_surgery_id = ?) AS ourPrices ON tvpip_item_id = tvpi_code AND tvpip_version = version")
    queryRunner.addParameterSetter(sid)
    if (wantComment) {
        qb.append(' LEFT OUTER JOIN tvpris_publicpricecomment ON    tvppc_surgery_id = ?')
        queryRunner.addParameterSetter(sid)
        qb.append(' AND tvpi_code = tvppc_item_id AND is_measure = (tvppc_is_measure != 0) AND (tvppc_version IS NULL OR tvppc_version = version)')
    }
    qb.append('  AND tvpip_is_measure = is_measure')
    if (typeRestriction != null) {
        qb.append(' WHERE ').append(typeRestriction).append(' ')
    }
    qb.append(' ORDER BY ')
    if (sort_field != null) {
        if (sort_field == 'code') {
            qb.append('is_measure, tvpi_code')
        } else {
            qb.append(sort_field);
        }
    }
    qb.append(' ').append(sort_order)

    def query = qb.toString()

    if (debug) {
        println('{{{')
        println(query)
        println('}}}')
    }
    def result = queryRunner.runQuery(query, limit, offset)

    def results = new ArrayList(result.size())

    for (row in result) {
        def map = new HashMap()
        int i = 0;
        for (c in columns) {
            if (c[0] == 'surgery_price_comment' || c[0] == 'description') {
                def xdom = services.rendering.parse(row[i], 'xwiki/2.1')
                map.put(c[0], services.rendering.render(xdom, 'xhtml/1.0'));
            } else {
                map.put(c[0], row[i])
            }
            i++
        }
        results.add(map)
    }

    retVal.put('r', results)

    if (itemCount == null) {
        queryRunner = new QueryRunner('native', xwiki, xcontext)
        result = queryRunner.runQuery('SELECT count(*) FROM tvpris_item' + (typeRestriction == null  ? '' : ' WHERE ' + typeRestriction), null, null)
        if (result.size() == 0) {
            retVal.put('error', 'No result in count!')
            response.setStatus(response.SC_INTERNAL_SERVER_ERROR)
        } else {
            if (result.size() > 1) {
                retVal.put('warning', 'More than one count in result!')
            }
            retVal.put('count', result.get(0))
        }
    }
}

println('{{{')
println(new JSONTool().serialize(retVal))
println('}}}')

{{/groovy}}
