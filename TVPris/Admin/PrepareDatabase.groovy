{{include reference="TVPris.Classes.PostedOperation" /}}{{groovy}}

PostedOperation [] postedOperations = [
    new PostedOperation(this, 'create_triggers', { -> createTriggers() }, 'Skapa triggers på kommentarstabeller.'),
    new PostedOperation(this, 'create_item_price_table', { -> createItemPriceTable() }, 'Skapa tabeller och index som ej skapas av hibernate.'),
];


maybeDoPostedOperations(request, postedOperations)
outputForm(postedOperations)

void createTriggers() {
    def context2 = xcontext.getContext()
    def store = xwiki.getXWiki().getStore().getStore()
    store.beginTransaction(context2)
    def session = store.getSession(context2)
    def connection = session.connection()
    def statement = connection.createStatement()
    try {
        def sql = '''
CREATE OR REPLACE FUNCTION set_surgery_id() RETURNS trigger AS $set_surgery_id$
  DECLARE
    spacename xwikidoc.xwd_web%TYPE;
    id        text;
  BEGIN
    spacename := (SELECT xwd_web FROM xwikidoc JOIN xwikiobjects ON xwo_name = xwd_fullname where xwo_id = NEW.xwo_id);
    id := (select regexp_replace(spacename, \'^((Mottagning)|(''' + services.tpkCompare.SURGERY_EDIT_SPACE + '''))\\.\', \'\'));
    IF id = spacename THEN
        RAISE EXCEPTION \'Refusing to save in space %.\', spacename;
    ELSE
        IF TG_ARGV[0] = \'public\' THEN
           NEW.tvppc_surgery_id := to_number(id, \'99999999999999999999\');
        ELSE
           IF TG_ARGV[0] = \'area\' THEN
              NEW.tpkac_surgery_id := to_number(id, \'99999999999999999999\');
           ELSE
              NEW.tvpic_surgery_id := to_number(id, \'99999999999999999999\');
           END IF;
        END IF;
        RETURN NEW;
    END IF;
  END;
$set_surgery_id$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS set_surgery_id_internal ON tvpris_internalpricecomment;
CREATE TRIGGER set_surgery_id_internal BEFORE INSERT OR UPDATE ON tvpris_internalpricecomment FOR EACH ROW EXECUTE PROCEDURE set_surgery_id(\'internal\');
DROP TRIGGER IF EXISTS set_surgery_id_public ON tvpris_publicpricecomment;
CREATE TRIGGER set_surgery_id_public   BEFORE INSERT OR UPDATE ON tvpris_publicpricecomment   FOR EACH ROW EXECUTE PROCEDURE set_surgery_id(\'public\');
DROP TRIGGER IF EXISTS set_surgery_id_area ON tpk_areacommentclass;
CREATE TRIGGER set_surgery_id_area   BEFORE INSERT OR UPDATE ON tpk_areacommentclass   FOR EACH ROW EXECUTE PROCEDURE set_surgery_id(\'area\');
''';
        statement.executeUpdate(sql);
    } catch (Exception e) {
        println("{{error}}Caught exception: " + e + "{{/error}}")
    } finally {
        statement.close()
        store.endTransaction(context2, true)
    }
}

void createPackageAreaTable() {
    def context2 = xcontext.getContext()
    def store = xwiki.getXWiki().getStore().getStore()
    store.beginTransaction(context2)
    def session = store.getSession(context2)
    def connection = session.connection()
    def statement = connection.createStatement()
    try {
      def sql = '''
CREATE TABLE IF NOT EXISTS tpk_package_areas (
  xwo_id bigint not null,
  tpa_area varchar(256)
);

CREATE INDEX IF NOT EXISTS tpk_package_areas_xwo_id ON tpk_package_areas USING BTREE (xwo_id);
CREATE INDEX IF NOT EXISTS tpk_package_areas_area ON tpk_package_areas USING BTREE (tpa_area);
DO $$ 
    BEGIN
        BEGIN
            ALTER TABLE ONLY tpk_package_areas ADD CONSTRAINT tpk_package_areas_id_fkey FOREIGN KEY (xwo_id) REFERENCES tvpris_package(xwo_id) ON UPDATE CASCADE ON DELETE CASCADE;
        EXCEPTION
            WHEN duplicate_object THEN RAISE NOTICE 'constraint tpk_package_areas_id_fkey already exists in tpk_package_areas.';
        END;
    END;
$$;
CREATE INDEX IF NOT EXISTS tpk_package_areas_idx ON tpk_package_areas USING BTREE (xwo_id, tpa_area);
DO $$ 
    BEGIN
        BEGIN
	ALTER TABLE ONLY tpk_package_areas ADD CONSTRAINT tpk_package_areas_unique UNIQUE (xwo_id, tpa_area);
        EXCEPTION
            WHEN duplicate_table THEN RAISE NOTICE 'constraint tpk_package_areas_unique already exists in tpk_package_areas.';
        END;
    END;
$$;


INSERT INTO tpk_package_areas
SELECT DISTINCT t.xwo_id, tvpi_area
FROM
  (SELECT xwo_id, CAST(unnest(regexp_matches(tvpp_measures, '[[:<:]][[:digit:]]{3}[[:>:]]', 'g')) AS INT) AS measure, tvpi_version FROM tvpris_package JOIN tvpris_item USING (xwo_id)) AS t
  JOIN tvpris_item ON is_measure AND t.measure=tvpi_code AND t.tvpi_version=tvpris_item.tvpi_version
ON CONFLICT DO NOTHING;
      '''

        statement.executeUpdate(sql);
    } catch (Exception e) {
        println("{{error}}Caught exception: " + e + "{{/error}}")
    } finally {
        statement.close()
        store.endTransaction(context2, true)
    }
}

void createItemPriceTable() {
    def context2 = xcontext.getContext()
    def store = xwiki.getXWiki().getStore().getStore()
    store.beginTransaction(context2)
    def session = store.getSession(context2)
    def connection = session.connection()
    def statement = connection.createStatement()
    try {
        def sql = '''
CREATE TABLE IF NOT EXISTS tpk_itemprice (
    tvpip_id serial NOT NULL PRIMARY KEY,
    tvpip_surgery_id integer,
    tvpip_type integer,
    tvpip_is_measure boolean CHECK((tvpip_type <> 0) = tvpip_is_measure),
    tvpip_item_id integer,
    tvpip_version bigint,
    tvpip_price integer
);
CREATE INDEX IF NOT EXISTS tpk_tvpip_item_price      ON tpk_itemprice USING btree  (tvpip_price);
CREATE INDEX IF NOT EXISTS tpk_tvpip_item_price_item ON tpk_itemprice USING btree (tvpip_item_id);
CREATE INDEX IF NOT EXISTS tpk_tvpip_sid_item_price  ON tpk_itemprice USING btree (tvpip_surgery_id, tvpip_price);
CREATE INDEX IF NOT EXISTS tpk_tvpip_surgery_id      ON tpk_itemprice USING btree (tvpip_surgery_id);
CREATE INDEX IF NOT EXISTS tpk_tvpip_version         ON tpk_itemprice USING btree (tvpip_version);
CREATE INDEX IF NOT EXISTS tpk_tvpip_type            ON tpk_itemprice USING btree (tvpip_type);
CREATE INDEX IF NOT EXISTS tpk_tvpip_composite       ON tpk_itemprice USING btree (tvpip_is_measure, tvpip_version, tvpip_item_id);
CREATE INDEX IF NOT EXISTS tpk_area_comment_area     ON tpk_areacommentclass USING btree (tpkac_area);
ALTER TABLE tpk_areacommentclass       ADD COLUMN IF NOT EXISTS tpkac_surgery_id integer;
CREATE INDEX IF NOT EXISTS tpk_area_surgery_id       ON tpk_areacommentclass USING btree (tpkac_surgery_id);
CREATE INDEX IF NOT EXISTS tvpi_item_composite       ON tvpris_item USING btree (is_measure, tvpi_code, tvpi_version);
ALTER TABLE tvpris_publicpricecomment  ADD COLUMN IF NOT EXISTS tvppc_surgery_id integer;
CREATE INDEX IF NOT EXISTS tvpris_public_surgery_id  ON tvpris_publicpricecomment USING btree (tvppc_surgery_id);
ALTER TABLE tvpris_internalpricecomment       ADD COLUMN IF NOT EXISTS tvpic_surgery_id integer;
CREATE INDEX IF NOT EXISTS tvpris_tvpic_price_composite ON tvpris_internalpricecomment USING btree (tvpic_surgery_id, tvpic_item_id, tvpic_is_measure, tvpic_version);
CREATE INDEX IF NOT EXISTS tvpris_tvppc_price_composite ON tvpris_publicpricecomment USING btree (tvppc_surgery_id, tvppc_item_id, tvppc_is_measure, tvppc_version);
CREATE INDEX IF NOT EXISTS tvpris_internal_surgery_id       ON tvpris_internalpricecomment USING btree (tvpic_surgery_id);
DO $$ 
    BEGIN
        BEGIN
	    ALTER TABLE ONLY tvpris_item ADD CONSTRAINT tvpris_item_composite_unique UNIQUE (is_measure, tvpi_version, tvpi_code);
        EXCEPTION
            WHEN duplicate_table THEN RAISE NOTICE 'constraint tvpris_item_composite_unique already exists in tvpris_item.';
        END;
    END;
$$;
DO $$ 
    BEGIN
        BEGIN
            ALTER TABLE ONLY tpk_itemprice ADD CONSTRAINT tpk_item_composite_unique UNIQUE (tvpip_type, tvpip_version, tvpip_item_id, tvpip_surgery_id);
        EXCEPTION
            WHEN duplicate_table THEN RAISE NOTICE 'constraint tpk_item_composite_unique already exists in tvpris_itemprice.';
        END;
    END;
$$;

-- Hibernate seems to be deleting rows during update, so we cannot use foreign key constraints.
-- ALTER TABLE ONLY tpk_itemprice ADD CONSTRAINT tvpris_surgery_id_item_fkey FOREIGN KEY (tvpip_surgery_id) REFERENCES tvpris_surgery(tvps_surgery_id) ON DELETE CASCADE;
-- ALTER TABLE ONLY tpk_itemprice
--    ADD CONSTRAINT tpk_itemprice_item_fkey FOREIGN KEY (tvpip_is_measure, tvpip_version, tvpip_item_id) REFERENCES tvpris_item(is_measure, tvpi_version, tvpi_code) ON DELETE CASCADE;

CREATE TABLE IF NOT EXISTS tpk_large_municipalities (
     tlm_municipality varchar(20) UNIQUE PRIMARY KEY
);

INSERT INTO tpk_large_municipalities VALUES
('Stockholm'),
('Göteborg'),
('Malmö')
ON CONFLICT DO NOTHING;

DO $$ 
    BEGIN
        BEGIN
	    CREATE COLLATION swedish (
	      LOCALE = 'sv_SE.utf8'
	    );
        EXCEPTION
            WHEN duplicate_object THEN RAISE NOTICE 'Collation swedish already exists.';
        END;
    END;
$$;

ALTER TABLE tvpris_surgery ALTER COLUMN tvps_municipality TYPE varchar(255) COLLATE swedish;
ALTER TABLE tvpris_surgery ALTER COLUMN tvps_company TYPE varchar(255) COLLATE swedish;
ALTER TABLE tvpris_surgery ALTER COLUMN tvps_county TYPE varchar(255) COLLATE swedish;
ALTER TABLE tvpris_surgery ALTER COLUMN tvps_sublocality TYPE varchar(255) COLLATE swedish;
ALTER TABLE tvpris_surgery ALTER COLUMN tvps_name TYPE text COLLATE swedish;
ALTER TABLE tvpris_surgery ALTER COLUMN tvps_category TYPE text COLLATE swedish;

ALTER TABLE tvpris_measure ALTER COLUMN tvpm_area_special TYPE varchar(1024) COLLATE swedish;
''';
        statement.executeUpdate(sql);
    } catch (Exception e) {
        println("{{error}}Caught exception: " + e + "{{/error}}")
    } finally {
        statement.close()
        store.endTransaction(context2, true)
    }
    createPackageAreaTable()
}

{{/groovy}}
