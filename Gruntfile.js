module.exports = function( grunt, init ) {
    const child_process = require('child_process');

    var outprop = "admoutput";
    
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        dirs: {
            src: 'src',
            dest: 'dest/<%= pkg.name %>/<%= pkg.version %>'
        },
        requirejs: {
            tpk: {
                options: {
                    baseUrl: '<%= dirs.dest %>/requirejs',
                    dir: '<%= dirs.dest %>/js',
                    paths: {
                        SurgeryComment: 'modules/SurgeryComment',
                        OverlaySpinner: 'widgets/overlayspinner',
                        Upload: 'widgets/Upload',
                        Notifyer: 'widgets/Notifyer',
                        AjaxEdit: 'modules/AjaxEdit',
                        Infobox: 'widgets/Infobox',
                        RelatedInfo: 'widgets/RelatedInfo',
                        EventBus: 'modules/EventBus',
                        Position: 'modules/Position',
                        Inputs: 'modules/Inputs',
                        Highlight: 'modules/Highlight',
                        History: 'modules/History',
                        MapInterface: 'modules/MapInterface',
                        StateTransition: 'modules/StateTransition',
                        StructuredCombobox: 'modules/StructuredCombobox',
                        SortControl: 'modules/SortControl',
                        Pagination: 'modules/Pagination',
                        SelectionPresenter: 'modules/SelectionPresenter',
                        behavior: "modules/behavior",
                        Result: "modules/Result",
                        LazyArray: 'modules/LazyArray',
                        GoogleAnalytics: 'modules/GoogleAnalytics',
                        Header: 'modules/Header',
                        jquery: 'empty:',
                        deferred: 'empty:',
                        bootstrap: 'empty:',
                        "babel-polyfill": '../../../../node_modules/babel-polyfill/browser'
                    },
                    modules: [
                        {
                            name: 'surgery-editable-sheet',
                            exclude: [
                                'jquery',
                                'bootstrap'
                            ]
                        },
                        {
                            name: 'tpk-client',
                            exclude: [
                                'jquery',
                                'deferred',
                                'bootstrap'
                            ]
                        },
                        {
                            name: 'linkable-infotext-sheet',
                            exclude: [
                                'jquery',
                                'deferred',
                                'bootstrap'
                            ]
                        },
                        {
                            name: 'break-cache',
                            exclude: [
                                'jquery',
                                'deferred',
                                'bootstrap'
                            ]
                        }
                    ],
                    uglify2: {
                        comments: '/^!/'
                    },
                    optimize: "none"
                }
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src: 'src/<%= pkg.name %>.js',
                dest: 'build/<%= pkg.name %>.min.js'
            }
        },
        less: {
            surgery: {
                options: {
                    sourceMap: true,
                    sourceMapFileInline: true,
                    paths: ['<%= dirs.src %>/less/common/', '<%= dirs.src %>/less/surgery/']
                },
                src: ['<%= dirs.src %>/less/common/**.less', '<%= dirs.src %>/less/surgery/**.less'],
                dest: '<%= dirs.dest %>/css/surgery-style.css'
            },
            app: {
                options: {
                    sourceMap: true,
                    sourceMapFileInline: true,
                    paths: ['<%= dirs.src %>/less/common/', '<%= dirs.src %>/less/app/']
                },
                src: ['<%= dirs.src %>/less/common/**.less', '<%= dirs.src %>/less/app/**.less'],
                dest: '<%= dirs.dest %>/css/app-style.css'
            }
        },
        babel: {
            options: {
                sourceMaps: false,
                presets: ['env'],
            },
            dist: {
                files: {
                    'dest/tpk-resources/0.1.0/requirejs/tpk-client.js': 'src/js/tpk-client.js',
                    'dest/tpk-resources/0.1.0/requirejs/break-cache.js': 'src/js/break-cache.js',
                    'dest/tpk-resources/0.1.0/requirejs/surgery-editable-sheet.js': 'src/js/surgery-editable-sheet.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/SurgeryComment.js': 'src/js/modules/SurgeryComment.js',
                    'dest/tpk-resources/0.1.0/requirejs/widgets/overlayspinner.js': 'src/js/widgets/overlayspinner.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/AjaxEdit.js': 'src/js/modules/AjaxEdit.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/behavior.js': 'src/js/modules/behavior.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/EventBus.js': 'src/js/modules/EventBus.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/Header.js': 'src/js/modules/Header.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/SelectionPresenter.js': 'src/js/modules/SelectionPresenter.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/Position.js': 'src/js/modules/Position.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/Inputs.js': 'src/js/modules/Inputs.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/History.js': 'src/js/modules/History.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/MapInterface.js': 'src/js/modules/MapInterface.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/LazyArray.js': 'src/js/modules/LazyArray/LazyArray.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/SortControl.js': 'src/js/modules/SortControl.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/Pagination.js': 'src/js/modules/Pagination.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/StateTransition.js': 'src/js/modules/StateTransition.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/StructuredCombobox.js': 'src/js/modules/StructuredCombobox.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/Highlight.js': 'src/js/modules/Highlight.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/Result.js': 'src/js/modules/Result.js',
                    'dest/tpk-resources/0.1.0/requirejs/modules/GoogleAnalytics.js': 'src/js/modules/GoogleAnalytics.js',
                    'dest/tpk-resources/0.1.0/requirejs/widgets/Infobox.js': 'src/js/widgets/Infobox.js',
                    'dest/tpk-resources/0.1.0/requirejs/widgets/RelatedInfo.js': 'src/js/widgets/RelatedInfo.js',
                    'dest/tpk-resources/0.1.0/requirejs/widgets/Upload.js': 'src/js/widgets/Upload.js',
                    'dest/tpk-resources/0.1.0/requirejs/widgets/Notifyer.js': 'src/js/widgets/Notifyer.js',
                    'dest/tpk-resources/0.1.0/requirejs/linkable-infotext-sheet.js': 'src/js/linkable-infotext-sheet.js',
                }
            }
        },
        eslint: {
            surgery: {
                options: {
                    "parserOptions": {
                        "ecmaVersion": 6
                    },
                    configFile: 'node_modules/eslint/conf/eslint-all.js',
                    globals: [
                        "XWiki:false",
                        "isJust:false",
                        "isNothing:false",
                        "isEmpty:false",
                        "isFunc:false",
                        "isDefined:false",
                        "isUndefined:false",
                        "ID:false",
                        "ARRAY_DELIM:false"
                    ],
                    rules: {
                        "new-cap": [
                            "error",
                            {
                                "capIsNewExceptions": ["Deferred"]
                            }
                        ],
                        "no-constant-condition": [
                            "error",
                            {
                                "checkLoops": false
                            }
                        ],
                        "one-var": "off",
                        "id-length": "off",
                        "padded-blocks": "off",
                        "no-console": "off",
                        "no-inner-declarations": "off",
                        "no-new": "off",
                        "max-len": [
                            "error",
                            {
                                "ignoreStrings": true,
                                "ignoreTemplateLiterals": true
                            }
                        ],
                        "max-lines": "off",
                        "max-params": "off",
                        "max-statements": "off",
                        "prefer-template": "off",
                        "require-jsdoc": "off",
                        "camelcase": "off",
                        "no-negated-condition": "off",
                        "sort-keys": "off",
                        "no-use-before-define": "off",
                        "no-ternary": "off",
                        "no-underscore-dangle": "off",
                        "no-confusing-arrow": "off",
                        "no-empty-function": "off",
                        "multiline-ternary": "off",
                        "newline-per-chained-call": "off",
                        "object-shorthand": "off",
                        "consistent-this": ["error", "self"],
                        "array-element-newline": "off",
                        "global-require": "off",
                        "no-unused-vars": [
                            "error",
                            {
                                "argsIgnorePattern": "_",
                                "varsIgnorePattern": "_"
                            }
                        ],
                        "quotes": [
                            "error",
                            "double",
                            {
                                "avoidEscape": true
                            }
                        ],
                        "dot-notation": [
                            "error",
                            {
                                "allowKeywords": false
                            }
                        ],
                        "func-style": [
                            "error",
                            "declaration",
                            {
                                "allowArrowFunctions": true
                            }
                        ],
                        "no-magic-numbers": [
                            "error",
                            {
                                "ignore": [-1, 0, 1, 2]
                            }
                        ]
                    },
                },
                files: {
                    src: ['<%= dirs.src %>/js/**/*.js']
                }
            }
        }
    });

    grunt.loadTasks('/usr/lib/nodejs/grunt-contrib-uglify/tasks');
    grunt.loadTasks('node_modules/grunt-contrib-concat/tasks');

    require('load-grunt-tasks')(grunt);
    

    // Default task(s).
    grunt.registerTask('default', ['less', 'eslint', 'babel', 'requirejs', 'deploy']);

    grunt.registerTask('deploy', function () {

        child_process.execSync('xwiki-deployer --project-json=xwiki-project.json', {
            stdio: 'inherit'
        });
    });

};
