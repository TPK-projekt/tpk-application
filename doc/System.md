## Applikationsinformation

Syftet med detta dokument är att beskriva hur Tandpriskollen är konfigurerat samt vilka gränssnitt som finns i, till och från systemet. 
Tandpriskollen är en prisjämförelsetjänst för tandvården.

Om Tandpriskollen är ur funktion blir konsekvensen att patienterna ej kan använda den för att leta fram bästa tandvårdsalternativ för dem.

## Systemöversikt

### Mjukvaruversioner
<table>
  <tr>
    <td>Debian</td><td>9.5</td>
  </tr>
  <tr>
    <td>Tomcat</td><td>8.5.14</td>
  </tr>
  <tr>
    <td>XWiki</td><td>9.11.4</td>
  </tr>
  <tr>
    <td>Apache</td><td>2.4.10</td>
  </tr>
  <tr>
    <td>PostgreSQL</td><td>9.6.10</td>
  </tr>
  <tr>
    <td>LibreOffise</td><td>5.2.7</td>
  </tr>
  <tr>
	<td>jOOQ</td><td>3.10.4</td>
  </tr>
</table>

					
## Konfiguration

Server 1 

<table>
  <tr>
   <td>Hostnamn</td><td>tpk-dev.kreablo.se/tpk-test.kreablo.se</td>
  </tr>
  <tr>
   <td>Servertyp</td><td>Virtuell maskin</td>
  </tr>
  <tr>
   <td>IP</td><td>94.254.0.24</td>
  </tr>
  <tr>
   <td>CPU</td><td>Intel Xeon</td>
  </tr>
  <tr>
   <td>Processor(s)</td><td>1	</td>
  </tr>
  <tr>
   <td>Ram</td><td>4GB		</td>
  </tr>
  <tr>
   <td>OS</td><td>Debian 9.5	 </td>
  </tr>
  <tr>
   <td>DB</td><td>	PostgreSQL</td>
  </tr>
  <tr>
   <td>Funktion</td><td>Test/utveckling</td>
  </tr>
</table>

Server 2 

<table>
  <tr>
   <td>Hostnamn</td><td>tpk-prod.kreablo.se</td>
  </tr>
  <tr>
   <td>Servertyp</td><td>Virtuella maskiner</td>
  </tr>
  <tr>
   <td>IP</td><td> 	</td>
  </tr>
  <tr>
   <td>Modell</td><td></td>
  </tr>
  <tr>
   <td>Processor(s)</td><td> 5</td>
  </tr>
  <tr>
   <td>Ram</td><td>8GB</td>
  </tr>
  <tr>
   <td>OS</td><td>	Debian 9.5 </td>
  </tr>
  <tr>
   <td>DB</td><td>		PostgreSQL</td>
  </tr>
  <tr>
   <td>Funktion</td><td>   Produktion</td>
  </tr>
</table>


### Applikationssökvägar

    /var/lib/xwiki
    /usr/lib/xwiki
    /usr/share/xwiki
    /etc/xwiki
    /etc/postgresql
    /etc/apache2

### Databassökvägar

    /var/lib/postgresql


### Loggfiler

    /var/log/tomcat8

### Installerade moduler

* jOOQ
* tpk-components

### Anpassningar som måste hanteras vid uppgradering
Den centrala sökfunktionen är implementerad som java-modul vilken exponerar ett REST-API med optimerad databassökning.  Jar-filerna för tpk-components och jOOQ måste installeras separat vid uppgradering av XWiki.

### Backupkonfiguration
Dagliga, veckovisa, månatliga och årliga databasdumpar.

## Övervakning och statuskontroll

* Icinga

### Larmpunkter

* Server uppe
* Svarar på http
* Applikationsserver uppe (REST-API:et svarar på http)
* Databasserver uppe
* CPU-belastning
* Ledigt diskutrymme
* Antal Zombie-processer
* Totalt antal processer
* Ej installerade säkerhetsuppdateringar

### Felsökning

* jdb

## Omstartsrutin

Systemet startar i sin helhet vid vanlig omstart av servrarna.

## Licenshantering

Systemet är baserat på fri programvara.

## Systemberoenden

### Externa system

#### Google maps
Systemet använder Google maps.  Det behövs ett konto med registrerade faktureringsuppgifter hos Google för detta. API-nyckeln behöver läggas in i konfigurationen.
Kända fel och lösningar

## Kontaktpersoner


Funktion Utveckling/förvaltning
Namn Andreas Jonsson 
Mail andreas.jonsson@kreablo.se


## Installation

Installation sker genom ansible-skript.  Redigera konfigurationsvariablerna i main.yml och serverkonfigurationen i inventory.  Installationen kräver en användare med root-rättigheter på målservrarna.

Instruktioner finns här: https://gitlab.com/TPK-projekt/tpk-ansible

## Sjösättning av produktion från test

Sjösättningen sker med hjälp av ansible-skripten som används vid installationen.

## Utveckling

TPK består av tre  olika gränssnitt för att stödja rollerna besökare, mottagningar och personal.  Utvecklingen sker på platformen XWiki, vilken stödjer snabb utveckling av prototypkod samtidigt som det finns en väg fram till en optimerad produktionslösning genom specialiserade java-komponeneter och klientkod i javaskript.

Ur utvecklarperspektiv har vi följande delar:

* HTML-dokument/server-skript
* Resurser till klientapplikation (javascript/css)
* Java-komponenter
* Deployment-verktyg

### HTML-dokument/server-skript

HTML-koden genereras på serversidan av en kombination av ren HTML-kod, XWiki-syntax, och skript-kod skriven i Apache Velocity och Groovy.  Denna kod finns i Git-repositoriet och sjösätts ovanpå platformen med hjälp av deployment-skriptet.

### Resurser till klientapplikationen

Javaskriptkoden utvecklas i ECMA-script 6 och kompileras till ECMA-skript 5 för kompatiblitet med webbläsare.  Verktyget eslint tillämpas för att se till att koden följer uppsatta kodningsregler och stil.

#### Källkod

https://gitlab.com/TPK-projekt/tpk-application

### Java-komponenter

För att optimera framförallt databassökningar har viss del av applikationen lagts i komponenter implementerade i Java.  Databaskoden bygger här på jOOQ-ramverket (https://www.jooq.org/javadoc/latest/).

Vid uppdatering av databasschemat behöver Java-schemat synkroniseras med det upaterade schemat.

#### Källkod

https://gitlab.com/TPK-projekt/tpk-components

### Deployment-verktyg

Velocity- och Groovy-skript och annat wiki-innehåll som är en del av applikationen kan deployas från Git-repositoriet med ett särskilt verktyg – xwiki-script-deployer.  Wiki-innehållet kopieras till wikiinstallationen genom REST-api:et.  Hur detta sker konfigureras genom json-filen xwiki-project.json.   En fil credentials.json behöver skapas med användarnamn och lösenord till en användare som har programmeringsrättighter på wiki-installationen.

    {
        "user": "Användarnamn",
        "password": "lösenord"
    }

Verktyget styrs av en json-fil:

    {
        "url": "https://tpk-dev.kreablo.se",
        "credentials": "xwiki-credentials.json",
        "wiki": "tandvrdspriser",
        "deployExcludeSpaces": ["MottagningensInformation", "Mottagning", "Åtgärder", "Paket"],
        "entities": [
             {
                   "file":   "TVPris/Forms/PriceQueryInput",
                   "entity": "TVPris.Forms.PriceQueryInput"
             },
			   ...



#### Källkod

https://gitlab.com/Andreas.Jonsson/xwiki-deployer

### Cache-strategi

För att förhöja användarupplevelsen och avlasta applikationsservern utnyttjas cachning i flera nivåer.

Ett mål är att effektivt kunna cacha sidinehåll och resurser i Apache med diskaccellerering använvänds mod_cache_disk.

<table>
  <tr>
   <td>API-anrop</td><td>ej cachade (Cache-control: no-cache, Expiry: -1)</td>
  </tr>
  <tr>
   <td>Default sidor</td><td>ej cachade (Cache-control: no-cache, Expiry: -1)</td>
  </tr>
  <tr>
   <td>Default resurser</td><td>cache (Cache-control: public, Expiry: +1 year), cache-brytning via query-parameter.</td>
  </tr>
  <tr>
   <td>Förstasidan/sökformulär och Infosidor</td><td>cache (Cache-control: public, Expiry: +8 hours), cache-brytning via query-parameter.  Cache-makrot med cache-version som parameter.</td>
  </tr>
  <tr>
   <td>Mottagningssidor/allmänhetens vy</td><td>cache (Cache-control: must-revalidate).   Cache-makrot med sid-version som parameter.</td>
  </tr>
</table>
