/* eslint-env browser, amd, es6 */
define([
    "jquery",
    "$services.webjars.url('jquery-ui', 'jquery-ui.js')".toString()
], () => {
    "use strict";

    const DEFAULT_DURATION = 400;

    const defaults = {
        "toggleElementOpenedClass": "opened",
        "toggleElementClosedClass": "closed",
        "bodyElementOpenedClass": "opened",
        "bodyElementClosedClass": "closed",
        "onOpen": () => {},
        "onClose": () => {},
        "startOpened": false,
        "openEffect": "blind",
        "closeEffect": "blind",
        "eventBus": null,
        "closeEvent": null,
        "duration": DEFAULT_DURATION,
        "allowToggle": true,
        "stopClickPropagation": true
    };

    let id = 0;

    function Expandable (options) {

        const o = (n) => {
            if (typeof options[n] === "undefined") {
                if (typeof defaults[n] === "undefined") {
                    throw new Error("Missing mandatory option: " + n);
                }
                this[n] = defaults[n];
            } else {
                this[n] = options[n];
            }
        };

        o("toggleElement");
        o("bodyElement");
        o("toggleElementOpenedClass");
        o("toggleElementClosedClass");
        o("bodyElementOpenedClass");
        o("bodyElementClosedClass");
        o("onOpen");
        o("onClose");
        o("openEffect");
        o("closeEffect");
        o("startOpened");
        o("eventBus");
        o("closeEvent");
        o("duration");
        o("allowToggle");
        o("stopClickPropagation");

        this.id = id;
        id += 1;

        if (this.startOpened) {
            this._body().show();
            this._open();
        } else {
            this._body().hide();
            this._close();
        }
        this._toggle().click(this.toggle.bind(this));
    }

    const ep = Expandable.prototype;

    ep._body = function _body () {
        return this.bodyElement;
    };

    ep._toggle = function _toggle () {
        return this.toggleElement;
    };

    ep._close = function _close () {
        const be = this._body();
        const te = this._toggle();

        if (this.closeEvent !== null && this.eventBus !== null) {
            this.eventBus.off(this.closeEvent + "." + this.id);
        }

        te.removeClass(this.toggleElementOpenedClass);
        be.removeClass(this.bodyElementOpenedClass);

        te.addClass(this.toggleElementClosedClass);
        be.addClass(this.bodyElementClosedClass);
    };

    ep._open = function _open () {
        if (this.closeEvent !== null && this.eventBus !== null) {
            this.eventBus.trigger(this.closeEvent);
            this.eventBus.on(this.closeEvent + "." + this.id, () => {
                this.close();
            });
        }
        const be = this._body();
        const te = this._toggle();
        te.removeClass(this.toggleElementClosedClass);
        te.addClass(this.toggleElementOpenedClass);
        be.removeClass(this.bodyElementClosedClass);
        be.addClass(this.bodyElementOpenedClass);
    };

    ep.isOpened = function isOpened () {
        return this._body().hasClass(this.bodyElementOpenedClass);
    };

    ep.toggle = function toggle (event) {
        if (this.stopClickPropagation) {
            event.preventDefault();
            event.stopPropagation();
        } else if (event.target.tagName === "A" || event.target.tagName === "BUTTON" ||
                  event.target.tagName === "INPUT") {
            return;
        }
        if (this.isOpened()) {
            if (this.allowToggle) {
                this.close();
            }
        } else {
            this.open();
        }
    };

    ep.open = function open () {
        if (this.isOpened()) {
            return;
        }
        if (this.openEffect !== null) {
            this._body().show(this.openEffect, {"duration": this.duration});
        } else {
            this._body().show();
        }
        this._open();
        this.onOpen();
    };

    ep.close = function close () {
        if (!this.isOpened()) {
            return;
        }
        if (this.closeEffect !== null) {
            this._body().hide(this.closeEffect, {"duration": this.duration});
        } else {
            this._body().hide();
        }
        this._close();
        this.onClose();
    };

    return Expandable;
});
