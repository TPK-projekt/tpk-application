
## Sjösätta testversion från utvecklingsmiljön.

1. Se till att vara inloggad på utvecklingsmiljön.
2. Gå till https://tpk-dev.kreablo.se/TVPris/DeployTest/
3. Välj "sjösätt testversion" och klicka på "verkställ".

Observera att denna process också hämtar infotexer från testservern.  Infotexter skall ej redigeras på utvecklingsmiljön.  (Detta eftersom infotexterna förväntas underhållas av TLV och testmiljön skall vara stabilare än utvecklingsmiljön.)

## Lägga till och redigera infotexter

Observera att infotexter skall redigeras på testservern.

1. Logga in på testmiljön
2. Gå till https://tpk-test.kreablo.se/Infotexter
3. Klicka på "lägg till".
4. Fyll i titel.
5. Skriv texten.
6. Infotexterna kan redigeras genom att besöka infotextens sida och välja redigera.
7. Många infotexter kräver att cachen rensas (se nedan) innan uppdaringar slår igenom.

## Lägga till infosida

En infosida är en presentations vy av en infotext.  För att en infotext skall gå att länka till som en vanlig webbsida behöver man lägga till en infosida för infotexten.

1. Se till att vara inloggad.
2. Välj mallen "Infosida"
3. Skriv en titel på sidan
4. Klicka "lägg till"
5. Ändra koden i dokumentinnehållet till önskad infotext.

##  Rensa cachen

Innehållet på vissa sidor cachas och cachen behöver brytas genom att öka versionsnummret för cachen.  Detta gör genom att klicka på ikonen "Rensa cachen".  Vid felmeddelande om att versionsnummret var felaktigt, ladda om sidan och klicka på ikonen igen.
