## We handle the ability to disable the minification, by choosing the file extension to add to the script paths.
#set ($jsExtension = '.min.js')
#if (!$services.debug.minify)
  #set ($jsExtension = '.js')
#end
##
## JS Libraries.
##
<script src="$services.webjars.url('requirejs', "require$jsExtension")"></script>
##
## xwiki-min.js and xwiki-min.css aggregate a couple of JavaScript and CSS files that are used on each page. In order to
## help debugging we provide/bundle the individual files also and you can load them instead of the aggregated files by
## passing 'minify=false' in the request query string or setting debug.minify property in xwiki.properties file.
##
## Hooks for inserting JavaScript skin extensions
##
#javaScriptExtensionHooks
##
## Compatibility "aspect" file for deprecated code.
## Placed at the very end of the stream so that skin file extensions code can be deprecated easily as well.
##
<script>
// <![CDATA[
## Requirejs will automatically add a ".js" suffix if the generated URL doesn't contain a "?". It happens that we
## don't generate a URL with "?" when we export in HTML for example. In this case we remove the ".js" suffix since
## requirejs will add one...
## Note that we cannot do this generically in the webjars module when exporting in HTML because the webjars module
## provide generic URLs and they don't all go through requirejs...
## Note that this comment and code below are not correctly indentend voluntarily in order to generate nicely formatted
## (and valid) code at runtime!
#macro(removeJsSuffix $expr)
## Note that velocity takes argument references by name (see: https://velocity.apache.org/engine/releases/velocity-1.5/user-guide.html#velocimacros).
## So we set the value of the $expr in the $url variable to not execute $expr multiple times.
#set ($url = $expr)
#if (!$url.contains('?'))$stringtool.removeEnd($url, '.js')#else$url#{end}
#end
##
## Start the requirejs config.'
##
require.config({
  paths: {
    'jquery': '#removeJsSuffix($services.webjars.url("jquery", "jquery${jsExtension}"))',
    'bootstrap': '#removeJsSuffix($services.webjars.url("bootstrap", "js/bootstrap${jsExtension}"))',
    'mustache': "$xwiki.getURL('TVPris.Javascript.Mustache', 'jsx', "r=$xcontext.get('tpk_cache_version')")",
    'deferred': "/resources/uicomponents/require/deferred",
    'text': "$xwiki.getURL('TVPris.Javascript.RequireText', 'jsx', "r=$xcontext.get('tpk_cache_version')")",
  },
  shim: {
    'bootstrap' : ['jquery'],
  },
  ## see: http://requirejs.org/docs/jquery.html#noconflictmap to see why this works.
  map: {
    '*': {
      'jquery': 'jQueryNoConflict'
    },
    'jQueryNoConflict': {
      'jquery': 'jquery'
    },
  },
  urlArgs: function(id, url) {
      if (url.substring(0, '/jsx/'.length)    !== '/jsx/' &&
          url.substring(0, '/TVPris/'.length) !== '/TVPris/') {
          return '';
      }
      
      var a = document.createElement('a');
      a.href = url;

      if (a.search) {
          var ps = a.search.substring(1).split('&');
          for (var i = 0; i < ps.length; i++) {
              var p = ps[i].split('=');
              if (p[0] === 'r') {
                  return '';
              }
          }
          return "&r=$xcontext.get('tpk_cache_version')";
      } else {
          return "?r=$xcontext.get('tpk_cache_version')";
      }
  }
});
define('jQueryNoConflict', ['jquery'], function ($) {
    return $.noConflict();
});
if (typeof(window.XWiki) == 'undefined') {
   window.XWiki = {};
}
XWiki.webapppath = "${xwiki.webAppPath}";
XWiki.servletpath = "${xwiki.servletPath}";
XWiki.contextPath = "${request.contextPath}";
XWiki.mainWiki = "$!{escapetool.javascript(${xcontext.mainWikiName})}";
define('template', {
    load: function(name, parentRequire, onload, config) {
        require(['mustache', "text!" + XWiki.viewUrl(name, 'xpage=plain')], function(mustache, template) {
            mustache.tags = ['_(', ')_'];
            onload(function (view) {return mustache.render(template, view);});
        });
    }
});
XWiki.jsxUrl = function(page) {
     return '/jsx/' + page.split('.').join('/') + '?v=' + "$xcontext.get('tpk_cache_version')";
};
XWiki.viewUrl = function(page, query) {
     return '/' + page.split('.').join('/') + (query ? '?' + query : '');
};
var htmlElement = document.documentElement;
if (["TPK.WebHome", "TVPris.WebHome"].indexOf(htmlElement.getAttribute("data-xwiki-document")) >= 0) {
    htmlElement.setAttribute("style", "display: none");
}
window.onload = function () {
    var toArray = function (c) {
        var a = [];
        for (var i = 0; i < c.length; i++) {
            a.push(c[i]);
        }
        return a;
    };
    var c = toArray(document.getElementsByClassName("tvpris-script"));
    for (var i = 0; i < c.length; i++) {
        var e = c[i];
        e.classList.remove("tvpris-script");
    }
    c = toArray(document.getElementsByClassName("script"));
    for (var i = 0; i < c.length; i++) {
        var e = c[i];
        e.classList.remove("script");
    }
    c = toArray(document.getElementsByClassName("tvpris-noscript"));
    for (var i = 0; i < c.length; i++) {
        var e = c[i];
        e.setAttribute("style", "display: none");
    }
    require(["deferred!EventBus"], function (ebPromise) {
        ebPromise.done(function (EventBus) {
            var de = document.documentElement;
            EventBus.onInit("application-init-complete", de.removeAttribute.bind(de, "style"));
        });
    });
};
// ]]>
</script>
