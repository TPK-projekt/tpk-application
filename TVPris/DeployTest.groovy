{{include reference="TVPris.Classes.PostedOperation" /}}{{groovy}}

import org.xwiki.model.reference.DocumentReference
import org.xwiki.model.reference.WikiReference
import org.xwiki.model.reference.SpaceReference
import org.xwiki.model.EntityType
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.Writer;

PostedOperation [] postedOperations = [
    new PostedOperation(this, 'deploy_test', { -> deployTest() }, 'Sjösätt testversion.'),
    //    new PostedOperation(this, 'fix_author', { -> fixAuthor() }, 'Fixa dokumentförfattare etc.'),
]

maybeDoPostedOperations(request, postedOperations)
outputForm(postedOperations)

void copyDocuments(String query, Date time, String sourceWiki, String destWiki, Map<DocumentReference, Exception> failures, Map<DocumentReference, DocumentReference> copied, String deployTimeDocName)
{
  int limit  = 100;
  boolean doEnd = false
  final String database = xcontext.getDatabase();
  xcontext.setDatabase(sourceWiki);
  try {
    for (int offset = 0; !doEnd ; offset += limit) {
      doEnd = true
      for (dn in services.query.xwql(query)
      .bindValue('date', time)
      .bindValue('deploytimedoc', deployTimeDocName)
      .bindValue('versiondoc', services.tpkCompare.VERSION_DOCUMENT)
      .setOffset(offset).setLimit(limit).execute()) {
	doEnd = false
	final def srcDoc = xwiki.getDocument(dn)
	final DocumentReference srcDocRef = srcDoc.getDocumentReference()
	final List<SpaceReference> spaceRefs = srcDocRef.getSpaceReferences()
	if (spaceRefs.size() > 0 && "XWiki".equals(spaceRefs.get(0).getName())) {
	    continue
	}
	final DocumentReference destDocRef = srcDocRef.replaceParent(srcDocRef.extractReference(EntityType.WIKI), new WikiReference(destWiki))
	final def destDoc = xwiki.getDocument(destDocRef)
	if (destDoc.isNew() || destDoc.getDate().before(srcDoc.getDate())) {
	  try {
	    boolean success = xwiki.copyDocument(srcDocRef, destDocRef, null, false, !destDoc.isNew())
	    if (!success) {
	      failures.put(srcDocRef, null)
	    } else {
	      copied.put(srcDocRef, destDocRef)
	      maybeInjectCustomMapping(srcDoc, destDoc);
	    }
	  } catch (Exception e) {
	    failures.put(srcDocRef, e)
	  }
	}
      }
    }
  } finally {
      xcontext.setDatabase(database);
  }
}

void deployTest() {
  final String devWiki = 'tandvrdspriser';
  final String testWiki = 'tpktest';
  final String deployTimeDocName = 'TVPris.Data.DeployTime';

  final def deployTimeDoc = xwiki.getDocument(deployTimeDocName);
  final def deployTimeObj = deployTimeDoc.getObject('TVPris.Classes.DeployTimeClass');
  final Date start = deployTimeObj.getProperty('start').getValue();
  final Date end   = deployTimeObj.getProperty('end').getValue();

  final Date time = end.before(start) ? end : start;
  deployTimeObj.set('start', new Date())
  deployTimeDoc.save('starting deploy')
  
  final Map<DocumentReference, Exception> failures = new TreeMap<DocumentReference, Exception>()
  final Map<DocumentReference, DocumentReference> copied   = new TreeMap<DocumentReference, DocumentReference>()
  copyDocuments("where doc.date > :date and not (doc.fullName = :deploytimedoc) and not (doc.fullName = 'XWiki.XWikiComments' OR doc.fullName = :versiondoc OR doc.fullName LIKE '" + services.tpkCompare.SURGERY_VIEW_SPACE + ".%' or doc.fullName LIKE '" + services.tpkCompare.SURGERY_EDIT_SPACE + ".%' or doc.fullName LIKE 'Åtgärder.%' or doc.fullName LIKE 'Paket.%' or doc.fullName LIKE 'Infotexter.Boxar.%' or doc.fullName LIKE 'Infotexter.Länkbara.%' or doc.fullName LIKE 'Infosidor.%')",
		time,
		devWiki,
		testWiki,
		failures,
		copied,
	        deployTimeDocName)

  println("=== Kopierade till målwiki dokument ===")
  for (d in copied.entrySet()) {
    println('* ' + d)
  }

  copied.clear();

  copyDocuments("where doc.date > :date and not (doc.fullName = :deploytimedoc or doc.fullName = :versiondoc) and (doc.fullName LIKE 'Infotexter.Boxar.%' or doc.fullName LIKE 'Infotexter.Länkbara.% or doc.fullName LIKE Infosidor.%')",
		time,
		testWiki,
		devWiki,
		failures,
		copied,
	        deployTimeDocName)  

    println("=== Kopierade från målwiki dokument ===")
    for (d in copied.entrySet()) {
      println('* ' + d)
    }

  if (failures.entrySet().size() > 0) {
    println("= Fel =")
    for (failure in failures.entrySet()) {
	Writer sw = new StringWriter();
	Writer w = new PrintWriter(sw);
	if (failure.getValue() != null) {
	    failure.getValue().printStackTrace(w);
	    StringBuffer stackTrace = sw.getBuffer();
	    println('* ((({{error}}' + failure.getKey() + ' ' + failure.getValue() + '{{{' + stackTrace.toString() + '}}}' + '{{/error}})))')
	    w.close();
	} else {
	    println('* ((({{error}}' + failure.getKey() + '{{/error}})))')
	}
    }
  } else {
    deployTimeObj.set('end', new Date())
    deployTimeDoc.save('deploy successfully completed')
  }
  println()
}

void fixAuthor () {
  for (dn in services.query.xwql("WHERE (doc.fullName NOT LIKE 'Paket.%' AND (doc.author LIKE '%:%' AND doc.author NOT LIKE 'xwiki:%') OR (doc.contentAuthor LIKE '%:%' AND doc.contentAuthor NOT LIKE 'xwiki:%') OR (doc.creator LIKE '%:%' AND doc.creator NOT LIKE 'xwiki:%') OR (doc.parent LIKE '%:%'))").execute()) {
    def d = xwiki.getDocument(dn)
    println("* " + dn + " " + d.author + " " + d.contentAuthor + " " + d.creator + " " + d.parent)

    Pattern p = Pattern.compile("^([^:]+):(.*)\$");

    Matcher m = p.matcher(d.author);

    if (m.matches() && m.group(1) != 'xwiki') {
      println("** author to " + m.group(2))
      d.getDocument().setAuthor(m.group(2));
    }

    m = p.matcher(d.contentAuthor);

    if (m.matches() && m.group(1) != 'xwiki') {
      println("** content author to " + m.group(2))
      d.getDocument().setContentAuthor(m.group(2));
    }

    m = p.matcher(d.creator);

    if (m.matches() && m.group(1) != 'xwiki') {
      println("** creator to " + m.group(2))
      d.getDocument().setCreator(m.group(2));
    }

    m = p.matcher(d.parent);

    if (m.matches()) {
      println("** parent to " + m.group(2))
      d.setParent(m.group(2));
    }

    println("*** fullname: " + d.fullName)
    if (d.fullName.indexOf("Paket") != 0) {
      d.save("fixed parent")
    }

  }
}

void maybeInjectCustomMapping(srcDoc, destDoc) {

    def xc = xcontext.getContext();
    def wiki = xc.getWiki();
    def srcClass = srcDoc.getDocument().getxWikiClass()

    if (srcClass != null && srcClass.getCustomMapping() == "internal") {
      destXDoc = destDoc.getDocument()
      def destClass = destXDoc.getxWikiClass()
      destClass.setCustomMapping("internal")
      wiki.saveDocument(destXDoc, xcontext.getContext());
      if (!"internal".equals(destXDoc.getxWikiClass().getCustomMapping())) {
	throw new RuntimeException("Failed to set custom mapping internal on class " + destDoc.getFullName());
      }
    }

}

{{/groovy}}
