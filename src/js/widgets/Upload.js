/* eslint-env browser, amd, es6 */
define([
    "jquery",
    "Notifyer"
], (jQuery, notifyer) => {
    "use strict";

    const UPLOAD_SERVICE = "TVPris.Services.SurgeryUpload";

    if (jQuery.event.props.indexOf("dataTransfer") < 0) {
        jQuery.event.props.push("dataTransfer");
    }

    const defaultOptions = {"uploadInput": null};

    function Upload (uploadType, options) {
        this.options = jQuery.extend({}, defaultOptions, options);
        this.uploadType = uploadType;
        this.selector = "#tvpris-surgery-" + uploadType + "-upload";
        const dropArea = jQuery(this.selector + " .droparea");
        this.dropArea = dropArea;
        this.imageArea = jQuery(this.selector + " .imagearea");

        dropArea.on("dragenter", this.dragStart.bind(this));
        dropArea.on("dragover", this.dragStart.bind(this));
        dropArea.on("dragend", this.dragStop.bind(this));
        dropArea.on("dragleave", this.dragStop.bind(this));
        dropArea.on("drop", (e) => {
            this.dragStop(e);

            e.dataTransfer.dropEffect = "copy";
            const {files} = e.dataTransfer;
            if (files.length > 0) {
                this.submitFile(files.item(0));
            }
        });

        if (this.options.uploadInput !== null) {
            this.initUploadInput();
        }
    }

    const p = Upload.prototype;

    p.dragStart = function dragStart (e) {
        this.dropArea.addClass("droparea-active");
        e.preventDefault();
        e.stopPropagation();
    };

    p.dragStop = function dragStop (e) {
        this.dropArea.removeClass("droparea-active");
        e.preventDefault();
        e.stopPropagation();
    };

    p.submitFile = function submitFile (file) {
        this.dropArea.addClass("uploading");
        const surgery = XWiki.Model.resolve(
            XWiki.currentSpace,
            XWiki.EntityType.SPACE
        ).extractReference(XWiki.EntityType.SPACE).getName();
        const formData = new FormData();
        formData.append("file", file);
        formData.append("type", this.uploadType);
        formData.append("form_token", jQuery("html").attr("data-xwiki-form-token"));
        formData.append("surgery", surgery);
        const hide = notifyer.progress("$services.localization.render('tvpris.surgery_uploading_file')");
        jQuery.ajax({
            "url": new XWiki.Document(XWiki.Model.resolve(UPLOAD_SERVICE, XWiki.EntityType.DOCUMENT)).
                getURL("view", "xpage=plain&outputSyntax=plain"),
            "type": "POST",
            "contentType": false,
            "async": true,
            "data": formData,
            "processData": false,
            "cache": false
        }).done((resp) => {
            this.imageArea.html(resp.image);
        }).always(() => {
            this.dropArea.removeClass("uploading");
            hide();
        }).fail((_, textStatus, errorThrown) => {
            notifyer.error("$services.localization.render('tvpris.surgery_upload_failed'): " +
                           textStatus + " " + errorThrown, "error");
        });

    };

    p.initUploadInput = function initUploadInput () {
        this.options.uploadInput.change(() => {
            this.options.uploadInput.
                each((_index, element) => {
                    if (element.files.length > 0) {
                        const [file] = element.files;
                        this.submitFile(file);
                    }
                });
            return false;
        });
    };

    return Upload;
});
