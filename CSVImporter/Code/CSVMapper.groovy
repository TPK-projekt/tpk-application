{{groovy output="none"}}

import org.xwiki.model.reference.DocumentReference
import org.xwiki.model.reference.EntityReference
import org.xwiki.model.reference.AttachmentReference
import org.xwiki.model.reference.DocumentReferenceResolver
import org.xwiki.bridge.DocumentAccessBridge
import org.apache.commons.csv.CSVRecord
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.text.NumberFormat
import java.nio.charset.Charset
import java.nio.CharBuffer
import java.io.InputStream
import java.io.InputStreamReader
import java.io.Reader


abstract class CSVProcessor {

    private final Locale locale

    private final def xwiki

    private CSVParser parser

    private int skipLines = 0;

    protected List<Exception> exceptions

    public CSVProcessor(xwiki, Locale locale, List<Exception> exceptions) {
        this.xwiki = xwiki
        this.locale = locale
	if (exceptions == null) {
	  this.exceptions = new ArrayList<>();
	} else {
	  this.exceptions = exceptions;
	}
    }

    public CSVProcessor(xwiki, locale) {
	this(xwiki, locale, null);
    }

    public void setExceptions(List<Exception> exceptions) {
	this.exceptions = exceptions;
    }

    public void process(String csvContent, CSVFormat format) {
        if (parser == null) {
           parser = CSVParser.parse(csvContent, format)
	}

	initiateHeader(parser.getHeaderMap())

        initiateProcessing()

	int skip = skipLines;

        for (CSVRecord record : parser) {
	    if (skip > 0) {
		skip--
	    } else {
		processRecord(record)
	    }
        }
    }

    public setSkipLines(int skipLines) {
	this.skipLines = skipLines;
    }

    protected void initiateHeader(Map<String, Integer> headerMap) {
    }

    public void process(AttachmentReference attachmentReference, Charset attachmentCharset, CSVFormat format) {

        try {
            def dab = services.component.getInstance(DocumentAccessBridge.class)

            InputStream content
            try {
                content = dab.getAttachmentContent(attachmentReference)
            } catch (NullPointerException e) {
                throw new RuntimeException("Attachment " + attachmentReference + " probably doesn't exist.", e)
            }
            Reader reader = new InputStreamReader(content, attachmentCharset)

            StringBuilder sb = new StringBuilder()

            try {
                CharBuffer cb = CharBuffer.allocate(1024)

		int n

                while ((n = reader.read(cb)) > -1) {
                    cb.rewind()
		    cb.limit(n)
                    sb.append(cb)
		    cb.clear()
                }
            } finally {
                reader.close()
            }

            process(sb.toString(), format)
        } finally {
            close()
        }
    }

    public void process(AttachmentReference attachmentReference, CSVFormat format) {
        process(attachmentReference, Charset.defaultCharset(), format)
    }

    public void process(String csvContent) {
        process(csvContent, CSVFormat.DEFAULT)
    }

    public void process(AttachmentReference attachmentReference) {
        process(attachmentReference, CSVFormat.DEFAULT)
    }

    protected void close() {
      if (parser != null) {
        parser.close()
      }
    }

    protected void initiateProcessing() {
    }

    public abstract void processRecord(CSVRecord record)

}

class ObjectPopulator {

    private final NamingScheme namingScheme

    protected final ObjCache objCache

    private final def xwiki

    private final def services

    protected final NumberFormat numberFormat

    private final List<Field> fields = new ArrayList<>()

    private final Map<String, ? extends List<Closure>> fieldModifiers = new HashMap<>()

    protected List<Exception> exceptions = new ArrayList<Exception>();

    public ObjectPopulator(xwiki, services, Locale locale, Character decimalGroupingSeparator, NamingScheme namingScheme, ObjCache objCache) {
        this.xwiki = xwiki
        this.services = services
        this.namingScheme = namingScheme
        this.numberFormat = NumberFormat.getInstance(locale)
        if (decimalGroupingSeparator != null) {
            def dfs = numberFormat.getDecimalFormatSymbols()
            dfs.setGroupingSeparator(' ' as char)
            numberFormat.setDecimalFormatSymbols(dfs)
        }
        this.objCache = objCache
    }

    public ObjectPopulator(xwiki, services, Locale locale, NamingScheme namingScheme, ObjCache objCache) {
        this(xwiki, services, locale, null, namingScheme, objCache)
    }

    public ObjectPopulator(xwiki, services, Locale locale, Character decimalGroupingSeparator, NamingScheme namingScheme) {
        this(xwiki, services, locale, decimalGroupingSeparator, namingScheme, new DefaultObjCache(xwiki, services))
    }

    public ObjectPopulator(xwiki, services, Locale locale, NamingScheme namingScheme) {
        this(xwiki, services, locale, null, namingScheme, new DefaultObjCache(xwiki, services))
    }

    public List<Field> getFields() {
        return fields
    }

    void setExceptions(List<Exception> exceptions) {
	this.exceptions = exceptions;
    }

    public ObjectPopulator addField(Field field) {
        this.fields.add(field)
	this.fieldModifiers.put(field.getFieldName(), new LinkedList())
        return this
    }

    public void addFieldModifier(Field field, Closure closure) {
        this.fieldModifiers.get(field.getFieldName()).add(closure);
    }

    public void populateObject(Record record) throws ObjectPopulatorException {

        for (int i = 0; i < getFields().size(); i++) {
            Field field = getFields().get(i)
            String stringValue = record.get(field)
	    for (modifier in this.fieldModifiers.get(field.getFieldName())) {
	      stringValue = modifier(stringValue)
	    }
	    //println(field.getFieldName() + ": " + stringValue)
            def obj = objCache.getObject(namingScheme.getObjectReference(this, record, field.getFieldName()))
            setPropertyField(obj, field, stringValue)
        }

        objCache.endIteration()
    }

    private boolean isNullProp(prop) {
      prop.getProperty() == null || prop.getProperty().getValue() == null
    }

    protected void setPropertyField(obj, Field field, String stringValue) {
        final boolean noUpdate = field.noUpdate() && obj.getBaseObject().getOwnerDocument().isNew()

        obj.set(field.getFieldName(), null)
        def prop = obj.getProperty(field.getFieldName())

        if (prop == null) {
            throw new ObjectPopulatorException("The class '" + obj.getxWikiClass().getName() + "' doesn't appear to have any property named '" + field.getFieldName() + "', prop was: " + prop)
        }
        String type = prop.getPropertyClass().getClassType()

        if ("String".equals(type) || "StaticList".equals(type) || "TextArea".equals(type)) {
	  if (!noUpdate || (isNullProp(prop) || prop.getProperty().getValue().equals(""))) {
	    prop.getProperty().setValue(stringValue)
	  }
        } else if ("Date".equals(type)) {
	  if (!noUpdate || isNullProp(prop)) {
            DateFormat df = new SimpleDateFormat(prop.getPropertyClass().getDateFormat(), locale)
            Date date = df.parse(stringValue)
            prop.getProperty().setValue(date)
	  }
        } else if ("Number".equals(type)) {
	  if (!noUpdate || isNullProp(prop)) {
	    Number number
	    if (stringValue == null || stringValue.equals("")) {
	        if (field.allowNull()) {
		  number = null
		} else {
		  throw new ObjectPopulatorException("Null or empty value on field " + field.getFieldName() + "!")
		}
	    } else {
	      number = numberFormat.parse(stringValue)
	    }
	    String numberType = prop.getPropertyClass().getPropertyClass().getNumberType()
	    if (number == null) {
	      prop.getProperty().setValue(null)
	    } else if ("long".equals(numberType)) {
	      prop.getProperty().setValue(number.longValue())
	    } else if ("integer".equals(numberType)) {
	      prop.getProperty().setValue(number.intValue())
	    } else if ("float".equals(numberType)) {
	      prop.getProperty().setValue(number.floatValue())
	    } else if ("double".equals(numberType)) {
	      prop.getProperty().setValue(number.doubleValue())
	    } else {
	      throw new ObjectPopulatorException("Number type " + numberType + " is not supported!")
	    }
	  }
        } else if ("Boolean".equals(type)) {
	  if (!noUpdate || isNullProp(prop)) {
	    Integer val
	    if ("false".equals(stringValue) || "Nej".equalsIgnoreCase(stringValue)) {
	      val = 0
	    } else if ("true".equals(stringValue) || "Ja".equalsIgnoreCase(stringValue)) {
	      val = 1
	    } else {
	      throw new ObjectPopulatorException("Unexpected boolean value '" + stringValue + "'")
	    }
	    prop.getProperty().setValue(val)
	  }
        } else {
	  throw new ObjectPopulatorException("Properties of type " + type + " are not supported!")
        }
    }

  public void close()
  {
    this.objCache.endTransaction()
  }


  public static class Field {

    private final int index

    private String fieldName

    private String title

    private boolean allowNull = false

    private boolean noUpdate = false

    public Field(int index) {
      this.index = index
      this.fieldName = fieldName
      this.title = title
    }

    public String getFieldName() {
      return fieldName
    }

    public String getTitle() {
      return title
    }

    public int getIndex() {
      return index
    }

    public Field setFieldName(String fieldName) {
      this.fieldName = fieldName
      return this
    }

    public Field setTitle(String title) {
      this.title = title
      return this
    }

    public Field setAllowNull(boolean allowNull) {
      this.allowNull = allowNull
      return this
    }

    public boolean allowNull() {
      return allowNull
    }

    public Field setNoUpdate(boolean noUpdate) {
      this.noUpdate = noUpdate;
      return this;
    }

    public boolean noUpdate() {
      return noUpdate;
    }
  }

    public interface NamingScheme {

        ObjectReference getObjectReference(ObjectPopulator populator, Record record, String fieldName)

    }

    public static class ObjectReference {

        private final DocumentReference containingDocumentRef

        private final String className

        private final int objectNumber

        public ObjectReference(DocumentReference containingDocumentRef, String className, int objectNumber)
        {
            this.containingDocumentRef = containingDocumentRef
            this.className = className
            this.objectNumber = objectNumber
        }

        public DocumentReference getContainingDocumentRef() {
            return containingDocumentRef
        }

        public String getClassName() {
            return className
        }

        public int getObjectNumber() {
            return objectNumber
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof ObjectReference &&
                   other.getContainingDocumentRef().equals(containingDocumentRef) &&
                   other.getClassName().equals(className) &&
                   other.getObjectNumber() == objectNumber
        }

        @Override
        public int hashCode() {
            return containingDocumentRef.hashCode() ^ className.hashCode() ^ objectNumber
        }
    }

    public interface Record {

        int getRecordNumber()

        String get(Field field) throws ObjectPopulatorException

    }

    public static class ObjectPopulatorException extends Exception {

        public ObjectPopulatorException(String msg) {
            super(msg);
        }
    }

    public interface ObjCache {

        Object getObject(ObjectReference objRef)

        void endIteration()

        void endTransaction()

    }

    public static class MapObjCache implements ObjCache {

        private Map<String, Object> currentRow = new HashMap<>()

        private List<Map<String, Object>> rows = new LinkedList<>()

        private Closure commit

	private final int commitInterval

        private int currentIteration = 0

        public MapObjCache(int commitInterval) {
             this.commitInterval = commitInterval
        }

        @Override
        Object getObject(ObjectReference objRef) {
             return currentRow
	}

        @Override
        void endIteration() {
	     rows.add(currentRow)
	     currentRow = new HashMap<>()
	     currentIteration++
	     if (currentIteration >= commitInterval) {
	          endTransaction()
		  currentIteration = 0
	     }
	}

        @Override
        void endTransaction() {
	     commit(rows)
	     rows = new LinkedList<>()
	}

      public void setCommit(Closure commit) {
	this.commit = commit;
      }
      
    }

    public static class DefaultObjCache implements ObjCache {

        private Set refWorkingSet = new HashSet()

        protected Map docs = new HashMap()

        private def services

        private def xwiki

        private DocumentReferenceResolver   docRefResolver

        DefaultObjCache(xwiki, services) {
            this.services = services
            this.xwiki = xwiki
            docRefResolver = services.component.getInstance(DocumentReferenceResolver.class, "current")
        }

        protected getDoc(DocumentReference docRef) {
            def doc = docs.get(docRef)
            if (doc == null) {
                doc = xwiki.getDocument(docRef)
                docs.put(docRef, doc)
            }

            return doc
        }

        @Override
        Object getObject(ObjectReference objRef) {

            Set newWorkingSet = getWorkingSet(refWorkingSet, objRef)
            if (!newWorkingSet.equals(refWorkingSet)) {
                endTransaction()
                if (refWorkingSet.size() != 0) {
                    throw new RuntimeException("endTransaction did not clear refWorkingSet! (refWorkingSet class: " + refWorkingSet.getClass().getName() + ")")
                }
                refWorkingSet.addAll(newWorkingSet)
            }

            def doc = getDoc(objRef.getContainingDocumentRef())

            def obj = doc.getObject(objRef.getClassName(), objRef.getObjectNumber())

            if (obj == null) {
                obj = doc.newObject(objRef.getClassName())
                def n = obj.getXWikiObject().getNumber()
                if (n != objRef.getObjectNumber()) {
		  //println(docRefResolver.resolve(objRef.getClassName()))
                    doc.getDocument().getXObjects(docRefResolver.resolve(objRef.getClassName())).set(n, null)
                    doc.getDocument().setXObject(objRef.getObjectNumber(), obj.getXWikiObject())
                }
            }

            return obj
        }

        @Override
        void endIteration() {
        }

        @Override
        void endTransaction() {
            for (doc in docs.values()) {
	      doc.save('ObjectPopulator updated object(s).')
            }
            refWorkingSet.retainAll()
            docs.keySet().retainAll()
        }

        protected Set getWorkingSet(Set set, ObjectReference ref) {
            if (set.size() == 0 || !set.contains(ref)) {
                return Collections.singleton(ref)
            }
            return set
        }

        protected Set getWorkingSet() {
            return refWorkingSet
        }
    }

}

class DBObjectPopulator extends ObjectPopulator {

  private final String tableName;

  private final Map<String, String> columns = new HashMap<>();
  private final Map<String, Integer> columnSqlTypes = new HashMap<>();

  private def beginTransaction

  private def endTransaction

  private static class NullNamingScheme implements ObjectPopulator.NamingScheme {
    public NullNamingScheme() {
    }
    @Override
    ObjectPopulator.ObjectReference getObjectReference(ObjectPopulator populator, ObjectPopulator.Record record, String fieldName) {
      return null;
    }
  }

  public DBObjectPopulator(xwiki, services, xcontext, Locale locale, Character decimalGroupingSeparator, String tableName) {
    super(xwiki, services, locale, decimalGroupingSeparator,  new NullNamingScheme(), new MapObjCache(1024) );
    this.objCache.setCommit({rows -> this.commit(rows) })
    this.tableName = tableName
    def context2 = xcontext.getContext()
    def store = xwiki.getXWiki().getStore().getStore()
    this.beginTransaction = { ->
      store.beginTransaction(context2);
      def session = store.getSession(context2)
      return session.connection()
    }
    this.endTransaction   = { ->  store.endTransaction(context2, true) }

    def connection = this.beginTransaction()
    
    def stmnt = connection.prepareStatement('SELECT * FROM ' + tableName + ' WHERE false');
    try {
      if (stmnt.execute()) {
      } else {
	throw new RuntimeException("Unexpected return value from execute!")
      }

      def metadata = stmnt.getMetaData()

      for (int i = 1; i <= metadata.getColumnCount(); i++) {
	columns.put(metadata.getColumnName(i), metadata.getColumnClassName(i))
	columnSqlTypes.put(metadata.getColumnName(i), metadata.getColumnType(i))
      }
    } finally {
      stmnt.close()
      this.endTransaction()
    }

  }

  @Override
  protected void setPropertyField(obj, Field field, String stringValue) {
    String className = columns.get(field.getFieldName())
    def checkNull = { -> if (stringValue == null || stringValue.equals('')) {
	if (field.allowNull()) {
	  obj.put(field.getFieldName(), null);
	  return true
	}
	return false
      }
    }
    if (Integer.class.getName().equals(className)) {
      if (!checkNull()) {
	Number number = numberFormat.parse(stringValue)
	obj.put(field.getFieldName(), number.intValue())
      }
    } else if (Long.class.getName().equals(className)) {
      if (!checkNull()) {
	Number number = numberFormat.parse(stringValue)
	obj.put(field.getFieldName(), number.longValue())
      }
    } else if (Boolean.class.getName().equals(className)) {
      if (!checkNull()) {
	Boolean b = Boolean.valueOf(stringValue)
	obj.put(field.getFieldName(), b)
      }
    } else {
      throw new RuntimeException("Unsupported type: " + className)
    }
  }

  protected void commit(List<Map<String, Object>> rows) {
    if (rows.size() < 1) {
      return
    }
    String insertSql = "INSERT INTO " + tableName + " ("
    String valuesSql = " VALUES ("
    List<String> columnNames = new LinkedList<>();
    boolean first = true
    for (columnName in rows[0].keySet()) {
      columnNames.add(columnName);
      if (first) {
	first = false
      } else {
	insertSql += ", "
	valuesSql += ", "
      }
      insertSql += columnName
      valuesSql += "?"
    }
    insertSql += ")" + valuesSql + ") ON CONFLICT DO NOTHING"

    def connection = this.beginTransaction()
    
    def stmnt = connection.prepareStatement(insertSql)

    try {
      for (row in rows) {
	int i = 1
	for (columnName in columnNames) {
	  def obj = row.get(columnName)
	  if (obj == null) {
	    stmnt.setNull(i, columnSqlTypes.get(columnName))
	  } else if (obj instanceof Long) {
	    stmnt.setLong(i, obj)
	  } else if (obj instanceof Integer) {
	    stmnt.setInt(i, obj)
	  } else if (obj instanceof Boolean) {
	    stmnt.setBoolean(i, obj)
	  } else {
	    throw new RuntimeException("Unsupported type: " + obj.getClass().getName())
	  }
	  i++
	}
	stmnt.addBatch()
      }
      stmnt.executeBatch()
    } catch (java.sql.BatchUpdateException e) {
	this.exceptions.add(e.getNextException())
    } finally {
      stmnt.close()
      this.endTransaction()
    }
  }
}

class CSVMapper extends CSVProcessor {

    final def script

    final ObjectPopulator objectPopulator

    final def services

    protected List<String> extendedFields = new ArrayList<>()

    protected Integer extendedFieldsLimit

    public CSVMapper(script, xwiki, services, Locale locale, Character decimalGroupingSeparator, ObjectPopulator.NamingScheme namingScheme, ObjectPopulator.ObjCache objCache)
    {
        super(xwiki, locale)
        this.services = services
        this.script = script
        this.extendedFieldsLimit = null
        objectPopulator = new ObjectPopulator(xwiki, services, locale, decimalGroupingSeparator, namingScheme, objCache)
    }

    public CSVMapper(script, xwiki, services, xcontext, Locale locale, Character decimalGroupingSeparator, String tableName)
    {
      super(xwiki, locale)
      this.services = services
      this.script = script
      objectPopulator = new DBObjectPopulator(xwiki, services, xcontext, locale, decimalGroupingSeparator, tableName) 
    }

    public CSVMapper(script, xwiki, services, Locale locale, Character decimalGroupingSeparator, ObjectPopulator.NamingScheme namingScheme)
    {
      this(script, xwiki, services, locale, decimalGroupingSeparator, namingScheme, new ObjectPopulator.DefaultObjCache(xwiki, services))
    }

    public CSVMapper(script, xwiki, services, Locale locale, ObjectPopulator.NamingScheme namingScheme, ObjectPopulator.ObjCache objCache)
    {
        this(script, xwiki, services, locale, null, namingScheme, objCache)
    }

    public CSVMapper(script, xwiki, services, xcontext, Locale locale, String tableName)
    {
      this(script, xwiki, services, xcontext, locale, null, tableName)
    }

    public CSVMapper(script, xwiki, services, Locale locale, ObjectPopulator.NamingScheme namingScheme)
    {
        this(script, xwiki, services, locale, null, namingScheme)
    }

    public CSVMapper(script, xwiki, services, ObjectPopulator.NamingScheme namingScheme)
    {
        this(script, xwiki, services, xwiki.getLocalePreference(), namingScheme)
    }

    public CSVMapper addField(ObjectPopulator.Field field) {
        objectPopulator.addField(field)
        return this
    }

    public CSVProcessor addFieldModifier(ObjectPopulator.Field field, Closure closure) {
        objectPopulator.addFieldModifier(field, closure)
	return this
    }

    public CSVProcessor addExtendedField(String fieldName) {
        extendedFields.add(fieldName)
        return this
    }

    protected newWrappedRecord(final CSVRecord record) {
        WrappedRecord wr = new WrappedRecord(record, extendedFields.size(), extendedFieldsLimit)
	wr = extendWrappedRecord(wr)
        return wr
    }

    @Override
    public void processRecord(final CSVRecord record) {
        WrappedRecord wrappedRecord = newWrappedRecord(record)
        if (wrappedRecord != null && !filterRecord(wrappedRecord)) {
            objectPopulator.populateObject(wrappedRecord)
        }
    }

    protected boolean filterRecord(ObjectPopulator.Record record) {
        return false
    }

    @Override
    protected void initiateProcessing() {
        super.initiateProcessing()

	objectPopulator.setExceptions(this.exceptions)
	
        int max = 0
        for (ObjectPopulator.Field field in objectPopulator.getFields()) {
            if (field.getIndex() > max) {
                max = field.getIndex()
            }
        }

        if (extendedFields.size() > 0) {
            extendedFieldsLimit = max + 1

            int i = extendedFieldsLimit
            for (String fieldName in extendedFields) {
                addField(new ObjectPopulator.Field(i++).setFieldName(fieldName))
            }
        }
    }

    class WrappedRecord implements ObjectPopulator.Record {

        private final CSVRecord csvRecord

        private String [] extendedFields

        private Integer extendedLimit

        public WrappedRecord(CSVRecord csvRecord) {
            this(csvRecord, 0, null)
        }

        public WrappedRecord(CSVRecord csvRecord, int numExtendedFields, Integer extendedLimit) {
            this.csvRecord = csvRecord
            prepareExtendedFields(numExtendedFields, extendedLimit)
        }

        public void prepareExtendedFields(int numExtendedFields, Integer extendedLimit) {
            this.extendedFields = new String[numExtendedFields]
            this.extendedLimit = extendedLimit
        }

        @Override
        public int getRecordNumber() {
            return csvRecord.getRecordNumber()
        }

        @Override
        public String get(ObjectPopulator.Field field) {
            if (extendedLimit != null && field.getIndex() >= extendedLimit) {
                int i = field.getIndex() - extendedLimit
                if (i > extendedFields.length) {
                    throw new ObjectPopulator.ObjectPopulatorException("Wrapped record have to few columns, expected at least " +
                                                               (field.getIndex() - 1) + " but record has only " + csvRecord.size() + " and " + extendedFields.length + " extended fields " + ", extendedLimit is " + extendedLimit)
                }
                return extendedFields[i]
            } else {
                return getFieldValue(script, csvRecord, field)
            }
        }

        public void setExtended(String fieldName, String value) {
            def xf = CSVMapper.this.extendedFields
            for (int i = 0;i < xf.size(); i++) {
                if (fieldName.equals(xf.get(i))) {
                    extendedFields[i] = value
                    return
                }
            }
            throw new RuntimeException("Wrapped record have no extended field named " + fieldName)
        }

        public void setExtended(int index, String value) {
            if (extendedLimit == null) {
                throw new ObjectPopulator.ObjectPopulatorException("Wrapped record have no extended fields.")
            }

            int index0 = index - extendedLimit

            if (index0 >= extendedFields.length) {
                throw new IndexOutOfBoundsException("Wrapped record have " + extendedFields.length + " fields with extended limit " + extendedLimit + ", index " + index  + " is out of bounds.")
            }

            extendedFields[index0] = value
        }

        public String getFieldValue(int index)
        {
	    return csvRecord.get(index)
	}

        @Override
        public String toString() {
            return "WrappedRecord with extendedFields " + extendedFields + " and extended limit " + extendedLimit;
        }

    }

    public ObjectPopulator.Field getField(String name) {
      for (field in objectPopulator.getFields()) {
	if (field.getFieldName().equals(name)) {
	  return field
	}
      }
      return null
    }



    protected static String getFieldValue(script, final CSVRecord csvRecord, final ObjectPopulator.Field field) {
        if (csvRecord.size() <= field.getIndex()) {
            script.println('{{warning}}' + "CSV record " + csvRecord.getRecordNumber() + " have to few columns, expected at least " +
                           (field.getIndex() + 1) + " but record has only " + csvRecord.size() + "{{/warning}}")
            return ""
        }
        return csvRecord.get(field.getIndex())
    }

    @Override
    protected void close() {
        super.close()  
        objectPopulator.close()
    }

    protected WrappedRecord extendWrappedRecord(WrappedRecord wrappedRecord) {
	return wrappedRecord;
    }
}

class CSVMapperDuplicates extends CSVMapper {

    private ObjectPopulator.Field indexField

    private Map<String, ObjectPopulator.Record> records = new TreeMap()

    public CSVMapperDuplicates(script, xwiki, services, Locale locale, ObjectPopulator.NamingScheme namingScheme, ObjectPopulator.ObjCache objCache) {
        super(script, xwiki, services, locale, namingScheme, objCache)
    }

    public CSVMapperDuplicates(script, xwiki, services, Locale locale, ObjectPopulator.NamingScheme namingScheme) {
        super(script, xwiki, services, locale, namingScheme)
    }

    public CSVProcessor setIndexField(ObjectPopulator.Field indexField) {
        this.indexField = indexField
        addField(indexField)
        return this
    }

    public ObjectPopulator.Field getIndexField() {
        return this.indexField
    }

    @Override
    public void processRecord(final CSVRecord csvRecord) {

        String indexValue = getFieldValue(script, csvRecord, indexField)

        ObjectPopulator.Record record = records.get(indexValue)
        if (record != null) {
            validate(record, csvRecord)
            return
        }

        record = newWrappedRecord(csvRecord)

        if (!filterRecord(record)) {
            records.put(indexValue, record)
        }
    }

    private void validate(ObjectPopulator.Record record, CSVRecord csvRecord) {
        for (int i = 0; i < extendedFieldsLimit; i++) {
            ObjectPopulator.Field field = objectPopulator.getFields().get(i)
            String v1 = getFieldValue(script, csvRecord, field)
            String v2 = record.get(field)
            if (! (v1 == null && v2 == null || v1 != null && v1.equals(v2))) {
                script.println("{{warning}}Inconsistent records: record " + csvRecord.getRecordNumber() + " is not consistent with " + record.getRecordNumber() + " on field " + field.getFieldName() + " {{/warning}}")
            }
        }
    }

    @Override
    protected void close() {
        for (k in records.keySet()) {
            objectPopulator.populateObject(records.get(k))
        } 
        super.close()
    }

}


{{/groovy}}
