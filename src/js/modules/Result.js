/* eslint-env browser, amd, es6 */
define([
    "EventBus",
    "jquery",
    "behavior",
    "LazyArray",
    "RelatedInfo",
    "Pagination",
    "History",
    "SelectionPresenter",
    "template!TVPris.Templates.CompareResults".toString()
], (
    EventBus,
    jQuery,
    behavior,
    LazyArray,
    relatedInfo,
    pagination,
    history,
    SelectionPresenter,
    resultTemplate
) => {
    "use strict";
    const register = (
        modelInterface,
        inputVal,
        doCompare,
        formToResultTransition,
        packagesCodeMap,
        measuresCodeMap,
        sortFieldInput,
        sortOrderInput,
        block,
        unblock
    ) => {
        let headerData = null;

        const PAGINATION_SIZE = 10;
        const PRELOAD_PAGE_SIZE = 600;
        const SCROLL_ADJUSTMENT = 95;

        const percentNF = typeof Intl === "object" && isFunc(Intl.NumberFormat)
            ? new Intl.NumberFormat("sv-SE", {
                "style": "percent",
                "maximumFractionDigits": 1,
                "minimumFractionDigits": 1
            })
            : {"format": String};

        const currencyNF = typeof Intl === "object" && isFunc(Intl.NumberFormat)
            ? new Intl.NumberFormat("sv-SE", {
                "style": "currency",
                "currency": "SEK",
                "minimumFractionDigits": 0
            })
            : {"format": String};

        const extractHeaderData = (data) => {
            const ref_prices_texts = data.
                ref_price.map((rp) => currencyNF.format(rp));
            const ci = comparedItems(
                data.ref_items,
                data.is_m,
                ref_prices_texts
            );
            const ref_price = data.ref_price.reduce((a, c) => a + c, 0);
            headerData = {
                "reference_price_text": currencyNF.format(ref_price),
                "reference_price": ref_price,
                "compared_items": ci,
                "compared_items_text": ci.length !== 1 ? ci.length + " åtgärder valda" : ci[0].text,
                "compared_items_more": ci.length !== 1 ? null : ci[0].more,
                "multi_items": ci.length > 1,
                "items": data.ref_items,
                "is_m": data.is_m.some(ID),
                "item_is_m": data.is_m,
                "is_p": data.is_m.some((x) => !x),
                "areas": data.areas,
                "is_replacement": isReplacement()
            };
            EventBus.trigger("new-header-data", [headerData]);
        };

        const processData = (data) => {

            /*
             * We can assume that the data in the model interface is still
             * consistent with the received data, because if it isn"t, the
             * data would have already been discarded in the function
             * appendPage.
             */
            if (Array.isArray(data.r)) {
                const a = data.r;
                const is_m = isJust(headerData) ? !headerData.is_p : false;
                for (let i = 0; i < a.length; i += 1) {
                    const p = a[i];
                    const surgeryViewUrlFull = XWiki.viewUrl("Mottagning." + p.s_id, is_m ? "pm=1" : null);
                    try {
                        const offset = (modelInterface.page() - 1) *
                              modelInterface.limit();
                        p.rank = i + 1 + offset;
                        p.surgery_view = surgeryViewUrlFull;
                        if (modelInterface.isSpecial()) {
                            p.surgery_view += "?special=true";
                        }
                        if (checkMissing(p)) {
                            p.success = false;
                        } else {
                            p.success = true;
                            formatPrices(p);
                        }
                        addComments(p);
                    } catch (e) {
                        console.log("Caught exception: " + e);
                        p.success = false;
                        p.errors = [{"text": "Fel uppstod!"}];
                    }
                    p.error = !p.success;
                }
                return a;
            }
            return [];
        };

        const initDisplayResults = () => {
            let lazyArrayKey = null;
            let lazyArray = null;

            EventBus.on("count-updated", (e, count) => {
                if (lazyArray !== null) {
                    lazyArray.setEnd(count);
                }
            });

            EventBus.on("clear-view", () => {
                lazyArray = null;
                lazyArrayKey = null;
            });

            const resultIsCurrent = (key) => lazyArray !== null &&
                  key === lazyArrayKey;

            const splitIntoGroups = (data) => {
                const GROUP_SIZE = 5;
                let groupCount = 0;
                const groups = [];
                let group = [];
                if (isJust(data)) {
                    for (let i = 0; i < data.length; i += 1) {
                        group.push(data[i]);
                        groupCount += 1;
                        if (groupCount >= GROUP_SIZE) {
                            groupCount = 0;
                            groups.push({"group": group});
                            group = [];
                        }
                    }
                }
                if (group.length > 0) {
                    groups.push({"group": group});
                }
                return groups;
            };

            EventBus.on("set-page", (_, page) => {
                if (isJust(lazyArray)) {
                    lazyArray.setPageSize(PAGINATION_SIZE);
                    lazyArray.getPage(
                        page,
                        (data) => displayPage(lazyArrayKey, data)
                    );
                }
            });

            let displaying = false;
            const displayPage = (key, dataIn) => {
                let data = dataIn;
                if (resultIsCurrent(key)) {

                    /*
                     * Only process this page if the corresponding search
                     * result array still corresponds to the input model.
                     */

                    data = {
                        "r": splitIntoGroups(data),
                        "has_header": false,
                        "is_special": modelInterface.isSpecial(),
                        "is_not_special": !modelInterface.isSpecial()
                    };
                    if (isJust(headerData)) {
                        jQuery.extend(data, headerData);
                    }

                    let html = resultTemplate(data);

                    const restab = document.getElementById("tvpris-search-result-table");
                    restab.innerHTML = html;

                    const html0 = jQuery(restab).children();

                    html = html0.children();
                    html.find(".tvpris-script").
                        each((_index, element) => jQuery(element).removeClass("tvpris-script"));

                    const collapses = html.find(".collapse");
                    behavior.accordion(collapses);
                    collapses.collapse({"toggle": false});
                    collapses.each((index0, element0) => {
                        const sid = jQuery(element0).attr("data-surgery-id");
                        jQuery(element0).on("show.bs.collapse", () => {
                            if (isJust(sid)) {
                                EventBus.trigger("surgery-highlighted", [sid]);
                            }
                        });
                        jQuery(element0).on("hide.bs.collapse", () => {
                            if (isJust(sid)) {
                                EventBus.trigger("surgery-unhighlighted", [sid]);
                            }
                        });
                    });
                    html.find(".focus-within, [tabindex], a").each((_index, element) => behavior.clickFocusElement(element));
                    html.find("div[data-toggle=\"collapse\"]").each((_index, element) => {
                        const collapse = jQuery(element);
                        const body = jQuery(collapse.attr("data-target"));
                        collapse.keydown((event) => {
                            if (event.key === "Enter") {
                                body.collapse("toggle");
                            }
                        });
                    });

                }
            };

            return () => {
                if (displaying || modelInterface.isSynchronized()) {
                    formToResultTransition.to();
                    return;
                }
                try {
                    displaying = true;
                    const key = modelInterface.key();
                    let la = null;
                    if (resultIsCurrent(key)) {
                        la = lazyArray;
                    } else {
                        const fetchData = (opts0) => {
                            block();
                            const done =
                                  (data) => processData(arrayToObjs(data));
                            const promise =
                                  doCompare(
                                      opts0.offset,
                                      opts0.limit
                                  ).done((data) => opts0.callback(done(data))).
                                      always(unblock);
                            if (isFunc(opts0.error)) {
                                promise.fail(opts0.error);
                            }
                        };
                        const opts = {"fetchData": fetchData};
                        la = new LazyArray(opts);
                        lazyArray = la;
                        lazyArrayKey = key;
                        la.setPageSize(PRELOAD_PAGE_SIZE);
                        la.getPage(1, (data) => {
                            EventBus.trigger("surgeries-added", [data]);
                        }, ID);
                        EventBus.trigger("synchronize");
                    }
                } finally {
                    displaying = false;
                }
                formToResultTransition.to();
            };
        };

        const checkMissing = (price) => {
            const d = modelInterface.data().data;
            const toInt = (s) => parseInt(s, 10);
            const missing_p = typeof d.p === "string" ? d.p.split(ARRAY_DELIM).map(toInt) : [];
            const missing_m = typeof d.m === "string" ? d.m.split(ARRAY_DELIM).map(toInt) : [];
            for (let j = 0; j < price.items.length; j += 1) {
                if (price.items[j] !== null) {
                    const ma = headerData.item_is_m[j] ? missing_m : missing_p;
                    const mj = ma.indexOf(price.items[j]);
                    if (mj >= 0) {
                        ma.splice(mj, 1);
                    }
                }
            }
            if (missing_p.length === 0 && missing_m.length === 0) {
                return false;
            }
            if (isJust(headerData)) {
                const active_areas = [];
                const {areas} = headerData;
                for (let j = 0; j < areas.length; j += 1) {
                    if (price.area_active[j]) {
                        if (active_areas.indexOf(areas[j]) < 0) {
                            active_areas.push(areas[j]);
                        }
                    }
                }
            }
            price.errors = [];
            const missingErrors = (m, plur, sing, map) => {
                if (m.length > 0) {
                    for (let i = 0; i < m.length; i += 1) {
                        if (map[m[i]]) {
                            m[i] = map[m[i]];
                        }
                    }
                    price.errors.push({"text": "Kontakta mottagningen för prisuppgift."});
                }
            };
            missingErrors(missing_p, "paketen", "paket", packagesCodeMap);
            missingErrors(missing_m, "åtgärderna", "åtgärden", "#" + measuresCodeMap);
            return true;
        };

        /* eslint-disable */

        const replacements = [
            921,
            922,
            925,
            926,
            928,
            929,
            940,
            941
        ];

        /* eslint-enable */

        const isReplacement = () => {
            if (isJust(headerData)) {
                for (let i = 0; i < headerData.item_is_m.length; i += 1) {
                    if (headerData.item_is_m[i] &&
                        replacements.indexOf(headerData.item[i]) >= 0) {
                        return true;
                    }
                }
            }
            return false;
        };

        const formatPrices = (p) => {
            if (isJust(headerData.reference_price)) {
                const c = p.price_total / headerData.reference_price;
                p.comp_text = percentNF.format(c);
                if (headerData.is_replacement) {
                    p.diff_text = "";
                } else {
                    p.diff_text = currencyNF.
                        format(p.price_total - headerData.reference_price);
                    if (p.price_total > headerData.reference_price) {
                        p.diff_text = "+" + p.diff_text;
                    }
                }
            }
            p.price_text = currencyNF.format(p.price_total);
        };

        const addComments = (price) => {
            price.comment_texts = [];
            price.has_comments = false;
            if (typeof price.comments === "object") {
                for (let i = 0; i < price.comments.length; i += 1) {
                    if (price.comments[i] !== null) {
                        const c = (price.comments.length > 1
                            ? (headerData.item_is_m[i]
                                ? "åtgärd"
                                : "paket") +
                            " " + price.items[i] + ": "
                            : "") + price.comments[i];
                        price.has_comments = true;
                        price.comment_texts.push({"text": c});
                    }
                }
            }
        };

        const comparedItems = (items, is_m, ref_prices_texts) => {
            const t = [];
            for (let i = 0; i < items.length; i += 1) {
                let s = null;
                if (is_m[i]) {
                    s = measuresCodeMap.get(String(items[i]));
                } else {
                    s = packagesCodeMap.get(String(items[i]));
                }
                t.push({
                    "code": items[i],
                    "text": s.text,
                    "more": isEmpty(s.more) ? null : s.more,
                    "ref_price_text": ref_prices_texts[i]
                });
            }
            return t;
        };

        const isHeaderField = (fn) => ["ref_price", "ref_items", "is_m", "areas"].indexOf(fn) >= 0;

        const arrayToObjs = (data) => {
            const res = [];
            const header = {};
            const f = data.fields;
            for (let i = 0; i < data.records.length; i += 1) {
                const obj = {};
                for (let j = 0; j < f.length; j += 1) {
                    if (isHeaderField(f[j].name)) {
                        if (i === 0) {
                            header[f[j].name] = data.records[i][j];
                        }
                    } else {
                        obj[f[j].name] = data.records[i][j];
                    }
                }
                res.push(obj);
            }
            if (isNothing(headerData) && data.records.length > 0) {
                extractHeaderData(header);
            }
            return {"r": res};
        };

        const countWidget = jQuery("#price-query-results-counter");

        EventBus.on("count-updated", (_, count) => {
            if (isJust(count)) {
                countWidget.removeClass("unknown");
                countWidget.find(".counter").text(String(count));
            } else {
                countWidget.addClass("unknown");
            }
        });

        EventBus.on("becomes-unsynchronized", () => {
            headerData = null;
            EventBus.trigger("new-header-data", [null]);
        });

        new SelectionPresenter({
            "container": jQuery(".query-item-presenter"),
            "events": "new-header-data",
            "htmlSource": true,
            "source": () => headerData === null ? [] : [headerData.compared_items_text]
        });

        new SelectionPresenter({
            "container": jQuery(".query-item-presenter-more"),
            "events": "new-header-data",
            "htmlSource": true,
            "source": () => headerData === null ? [] : [headerData.compared_items_more]
        });

        new SelectionPresenter({
            "container": jQuery(".reference-price"),
            "events": "new-header-data",
            "htmlSource": true,
            "source": () => headerData === null ? [] : [headerData.reference_price_text]
        });

        new SelectionPresenter({
            "container": jQuery("#measures-list-presenter"),
            "events": "new-header-data",
            "htmlSource": true,
            "source": () => {
                if (headerData === null) {
                    return [];
                }
                return [];
            }
        });

        const displayRelatedInfo = (element) => {
            if (isJust(headerData)) {
                const ps = [];
                const ms = [];
                const mareas = [];
                const {items} = headerData;
                for (let i = 0; i < items.length; i += 1) {
                    if (headerData.item_is_m[i]) {
                        ms.push(items[i]);
                    } else {
                        ps.push(items[i]);
                    }
                }
                const {areas} = headerData;
                for (let i = 0; i < areas.length; i += 1) {
                    if (!isEmpty(areas[i])) {
                        mareas.push(areas[i]);
                    }
                }
                relatedInfo(
                    headerData,
                    ps,
                    ms,
                    headerData.areas,
                    element
                );
            }
        };

        EventBus.on(
            "becomes-unsynchronized",
            () => jQuery("#tpk-query-presenter-body").collapse("hide")
        );
        jQuery("#tpk-query-presenter-body").collapse({"toggle": false});
        jQuery("#tpk-query-presenter-body").on("show.bs.collapse", () => {
            displayRelatedInfo(document.getElementById("measures-list-presenter"));
        });
        jQuery("#search-control-link").dropdown();
        EventBus.on("change:" + sortOrderInput.id + " change:" + sortFieldInput.id, () => {
            const order = inputVal(sortOrderInput);
            const field = inputVal(sortFieldInput);
            jQuery("#search-control-menu .dropdown-menu li").each((index_, li) => {
                if (jQuery(li).find("[data-field]").attr("data-field") === field &&
                    jQuery(li).find("[data-order]").attr("data-order") === order) {
                    li.classList.add("current");
                } else {
                    li.classList.remove("current");
                }
            });
        });
        jQuery("#search-control-menu .dropdown-menu a").click((event) => {
            const field = event.target.getAttribute("data-field");
            const order = event.target.getAttribute("data-order");
            inputVal(sortOrderInput, order);
            inputVal(sortFieldInput, field);
            jQuery("#search-control-link").dropdown("toggle");
            return false;
        });

        const pag = pagination("tpk-pagination", PAGINATION_SIZE);

        history.register({
            "load": (page) => {
                if (Number.isInteger(page)) {
                    pag.setPage(page);
                }
            },
            "save": () => {
                const page = modelInterface.page();
                return page === 1 ? null : page;
            },
            "parse": Number,
            "serialize": String,
            "name": "page",
            "order": 200
        });
        history.addPushEvents("page-change");
        EventBus.on("page-change", (_, page) => {
            const rt = document.getElementById("search-control-menu");
            const {top} = rt.getBoundingClientRect();
            if (top < 0) {
                const offset = isJust(window.scrollY)
                    ? window.scrollY
                    : window.pageYOffset;
                window.scrollTo(0, top + offset - SCROLL_ADJUSTMENT);
            }
            if (page === 1) {
                jQuery("#average-price-info").show();
            } else {
                jQuery("#average-price-info").hide();
            }
        });

        return initDisplayResults();
    };

    return register;
});

