{{groovy}}


if (request.getMethod() == "POST" && xwiki.hasAdminRights() && services.csrf.getToken() == request.get('form_token') ) {
   if (request.get('inject_custom_mapping') == "on") {
       injectCustomMapping(request.get('custom_mapping_classname'), request.get('custom_mapping'));
   }
   if (request.get('view_custom_mapping') == "on") {
       viewCustomMapping(request.get('custom_mapping_classname'));
   }
   if (request.get('delete_custom_mapping') == "on") {
       deleteCustomMapping(request.get('custom_mapping_classname'));
   }
   if (request.get('set_to_internal_mapping') == "on") {
       setToInternalMapping(request.get('custom_mapping_classname'));
   }
}
outputForm();

void outputForm()
{
    println("((({{html}}")
    println('<form action="' + xwiki.getURL(doc.fullName) + '" method="POST" >')
    println('<input type="hidden" name="form_token" value="' + services.csrf.getToken() + '" />')
    println('<dl>')
    println('<dd><input type="checkbox" name="view_custom_mapping" id="view_custom_mapping" /><label for="view_custom_mapping">Visa custom mapping.</label></dd>')
    println('<dd><input type="checkbox" name="inject_custom_mapping" id="inject_custom_mapping" /><label for="inject_custom_mapping">Injicera custom mapping.</label></dd>')
    println('<dd><input type="checkbox" name="set_to_internal_mapping" id="set_to_internal_mapping" /><label for="set_to_internal_mapping">Sätt till "internal".</label></dd>')
    println('<dd><input type="checkbox" name="delete_custom_mapping" id="delete_custom_mapping" /><label for="delete_custom_mapping">Ta bort custom mapping.</label></dd>')

    outputClassSelect(request.get('custom_mapping_classname'))
    println('<dd><label for="custom_mapping">Hbm-definition:</label></dd>')
    println('<dd><textarea name="custom_mapping" id="custom_mapping" rows="20" cols="100">' + (request.get('custom_mapping') != null ? new org.xwiki.velocity.tools.EscapeTool().xml(request.get('custom_mapping')) : '') + '</textarea></dd>')
    println('</dl>')
    println('<span class="buttonwrapper"><input type="submit" class="button" name="submit" value="Verkställ" /></span>');
    println('</form>')
    println("{{/html}})))")
}


void viewCustomMapping(final String classname)
{
    println('== Visar custom mapping för ' + classname + ' ==')

    def classDoc = xwiki.getDocument(classname)
    //get some objects.
    def xc = xcontext.getContext();
    def wiki = xc.getWiki();
    def thisClass = classDoc.getDocument().getxWikiClass()

    println("{{code}}");
    println(thisClass.getCustomMapping());
    println("{{/code}}");
}
void injectCustomMapping(final String classname, final String mapping)
{

    println('== Injicerar custom mapping för ' + classname + ' ==')

    def classDoc = xwiki.getDocument(classname)
    //get some objects.
    def xc = xcontext.getContext();
    def wiki = xc.getWiki();
    def thisClass = classDoc.getDocument().getxWikiClass()
    //set the mapping
    thisClass.setCustomMapping(mapping);

    //XWiki provides us with a way to test our custom mapping!
    if (thisClass.isCustomMappingValid(xc)) {
        //We must save the document with the new custom mapping set.
        classDoc.save();
        //This will make Hibernate create the new database table.
        wiki.getHibernateStore().updateSchema(thisClass, xc);
        println("Done");
    } else {
        println("The mapping is not valid, consult your error log to find the problem.")
    }
}

void deleteCustomMapping(final String classname)
{

    println('== Injicerar custom mapping för ' + classname + ' ==')

    def classDoc = xwiki.getDocument(classname)
    //get some objects.
    def xc = xcontext.getContext();
    def wiki = xc.getWiki();
    def thisClass = classDoc.getDocument().getxWikiClass()
    //set the mapping
    thisClass.setCustomMapping(null);

    //XWiki provides us with a way to test our custom mapping!
    if (thisClass.isCustomMappingValid(xc)) {
        //We must save the document with the new custom mapping set.
        classDoc.save();
        //This will make Hibernate create the new database table.
        wiki.getHibernateStore().updateSchema(thisClass, xc);
        println("Done");
    } else {
        println("The mapping is not valid, consult your error log to find the problem.")
    }
}

void setToInternalMapping(final String classname)
{

    println('== Sätter custom mapping för ' + classname + ' till internal ==')

    def classDoc = xwiki.getDocument(classname).getDocument()
    //get some objects.
    def xc = xcontext.getContext();
    def wiki = xc.getWiki();
    def thisClass = classDoc.getxWikiClass()
    //set the mapping
    thisClass.setCustomMapping("internal");

    //We must save the document with the new custom mapping set.
    xcontext.getContext().getWiki().saveDocument(classDoc, xcontext.getContext());
    if ("internal".equals(classDoc.getxWikiClass().getCustomMapping())) {
        println("Success.");
    } else {
        println("Failed to alter the custom mapping field.");
    }
}


void outputClassSelect(final String activeclass) {
  println('<dd><label for="custom_mapping_classname">Klassnamn</label><select name="custom_mapping_classname" id="custom_mapping_classname">')
  println('<option value=""></option>')
  for (classname in services.query.hql('SELECT DISTINCT className FROM BaseObject AS o ORDER BY className').execute()) {
    println('<option value="' + classname + '"' + (classname == activeclass ? ' selected' : '') + '>' + classname + '</option>')
  }
  println('</select></dd>')
}

{{/groovy}}
