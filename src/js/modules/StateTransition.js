/* eslint-env browser, amd, es6 */
define(() => {
    "use strict";

    function StateTransition (to, from, noInit, valid) {
        this.atInitial = true;
        this.to = (...a) => {
            if (isFunc(valid) && !valid()) {
                if (!this.atInitial) {
                    from(...a);
                }
            } else if (this.atInitial) {
                this.atInitial = false;
                to(...a);
            }
        };
        this.from = (...a) => {
            if (!this.atInitial) {
                this.atInitial = true;
                from(...a);
            }
        };
        this.toggle =
            (...a) => this.atInitial ? this.to(...a) : this.from(...a);
        if (!(typeof noInit !== "undefined" && noInit)) {
            from();
        }
    }

    StateTransition.prototype.compose = function compose (st) {
        return new StateTransition((...a) => {
            this.to(...a);
            st.to(...a);
        }, (...a) => {
            this.from(...a);
            st.from(...a);
        });
    };

    StateTransition.prototype.mutex = function mutex (st) {
        const thisTo = this.to;
        const thatTo = st.to;
        this.to = (...a) => {
            st.from(...a);
            thisTo(...a);
        };
        st.to = (...a) => {
            this.from(...a);
            thatTo(...a);
        };
    };

    StateTransition.prototype.history = function history (supportUrl) {
        const obj = {
            "save": () => this.atInitial ? 0 : 1,
            "load": (s) => s === 1 ? this.to() : this.from()
        };
        if (supportUrl) {
            obj.serialize = () => this.atInitial ? null : 1;
            obj.parse = (s) => isJust(s) ? Number(s) : null;
        }
        return obj;
    };

    return StateTransition;
});
