/* eslint-env browser, amd, es6 */
define([
    "jquery",
    "Notifyer",
    "EventBus",
    "Infobox"
], (jQuery, notifyer, EventBus, infoBox) => {
    "use strict";

    const isDefined = (x) => typeof x !== "undefined";
    const isUndefined = (x) => !isDefined(x);
    const isNothing = (x) => isUndefined(x) || x === null;
    const isJust = (x) => !isNothing(x);

    let relatedInfoboxesFetched = false;
    EventBus.on("parameters-updated", () => {
        relatedInfoboxesFetched = false;
        return 1;
    });


    const displayRelatedInfoDialog = (
        event,
        ctx
    ) => {
        event.preventDefault();
        event.stopPropagation();

        const container = jQuery("#related-infotext-2");

        ctx.initialInfoboxes = [];

        require(["template!TVPris.Templates.RelatedInfotext2"], (rit) => {
            if (container.find(".modal-body").length === 0) {
                const html = jQuery(rit(ctx));
                container.append(html);

                html.find("[data-infobox]").each((index_, element) => infoBox(
                    jQuery(element),
                    null
                ));

                container.click((e) => {
                    if (!(/(input)|(button)|(a)/i).test(e.target.tagName)) {
                        container.modal("hide");
                        return false;
                    }
                    return true;
                });
            }

            container.modal("show");
        });
    };

    const packageMeasureExtra = (p, mm) => {
        const extra = [];
        if (isJust(p.packageMeasureCodes) &&
            isJust(p.packageMeasureCount) &&
            p.packageMeasureCodes.length === p.packageMeasureCount.length) {
            let s = "";
            let first = true;
            for (let i = 0; i < p.packageMeasureCodes.length; i += 1) {
                const m = mm.get(String(p.packageMeasureCodes[i]));
                if (isJust(m)) {
                    if (first) {
                        first = false;
                        s += "$services.localization.render('tpk.relatedinfo-included-measures') ";
                    } else {
                        s += ", ";
                    }

                    if (p.packageMeasureCount[i] > 1) {
                        s += " (" + p.packageMeasureCount[i] + " st)";
                    }
                    s += m.code;
                }
            }
            extra.push({"text": s});
        }
        return extra;
    };

    const addToListMap = (map, k, v) => {
        if (isNothing(map.get(k))) {
            map.set(k, [v]);
        } else {
            map.get(k).push(v);
        }
    };

    let relatedInfotextTemplate = null;

    const price_text = (item, data) => {
        for (let i = 0; i < data.compared_items.length; i += 1) {
            const it = data.compared_items[i];
            if (it.code === item.code) {
                return it.ref_price_text;
            }
        }
        return "";
    };

    const prepareContext = (
        data,
        ps,
        ms,
        mareas,
        container,
        infoboxes
    ) => {
        const areas = [];

        const multiple =
              (isJust(infoboxes.measures)
                  ? Object.values(infoboxes.measures).length
                  : 0) +
              (isJust(infoboxes.packages)
                  ? Object.values(infoboxes.packages).length
                  : 0) >
              1;

        let type = {};

        const am = new Map();
        const mm = new Map();
        if (isJust(infoboxes.measures)) {

            const es = Object.entries(infoboxes.measures);
            for (let i = 0; i < es.length; i += 1) {
                const [code, m] = es[i];
                addToListMap(am, m.area, m);
                mm.set(code, m);
            }
        }

        if (isJust(infoboxes.packages)) {
            if (multiple) {
                type.type_text = "Behandlingspaket";
            }
            type.items = [];
            const vs = Object.values(infoboxes.packages);
            for (let i = 0; i < vs.length; i += 1) {
                const p = vs[i];
                const extra = packageMeasureExtra(p, mm);

                const ibs = [];
                for (let j = 0; j < p.infoboxes.length; j += 1) {
                    const ib = p.infoboxes[j];
                    ibs.push({
                        "title": ib.title,
                        "text": ib.text
                    });
                }
                type.items.push({
                    "title": p.title,
                    "code": p.code,
                    "description": p.description,
                    "extra": extra,
                    "related_info": ibs,
                    "price_text": price_text(p, data),
                    "is_p": true,
                    "is_m": false
                });
            }
        }

        areas.push({
            "infoboxes": [],
            "types": type
        });

        const es = am.entries();
        for (let i = 0; i < es.lenght; i += 1) {
            const [a, measures] = es[i];
            const items = [];
            const ibs = [];
            const entries = Object.entries(infoboxes.mareas).
                filter(([a0, _ma]) => a.code === a0);
            for (let k = 0; k < entries.length; k += 1) {
                const [_a, ma] = entries[k];
                ibs.push({
                    "title": ma.title,
                    "text": ma.text
                });
            }
            for (let k = 0; k < measures.length; k += 1) {
                const m = measures[k];
                const mibs = [];
                for (let j = 0; j < m.infoboxes.lenght; j += 1) {
                    const mib = m.infoboxes[j];
                    mibs.push({
                        "title": mib.title,
                        "text": mib.text
                    });
                }
                items.push({
                    "title": m.title,
                    "code": m.code,
                    "description": m.description,
                    "extra": [],
                    "related_info": ibs,
                    "price_text": price_text(m, data),
                    "is_m": true,
                    "is_p": false
                });
            }
            type = {
                "type_text": "Behandlingsåtgärder i sökningen",
                "items": items
            };
            areas.push({
                "area_text": {
                    "name": a,
                    "infoboxes": ibs
                },
                "types": [type]
            });
        }

        const moreLink = [{"href": "#"}];

        const initialInfoboxes = isJust(infoboxes.infoboxes)
            ? infoboxes.infoboxes
            : [];

        return {
            "infoboxes": initialInfoboxes,
            "areas": areas,
            "more_info_link": moreLink,
            "multiple": multiple,
            "has_measures": ms.length > 0,
            "has_packages": ps.length > 0,
            "has_no_packages": ps.length <= 0
        };

    };


    const displayRelatedInfoboxes_ = (
        data,
        ps,
        ms,
        mareas,
        container,
        infoboxes
    ) => {
        if (relatedInfoboxesFetched) {
            return;
        }
        relatedInfoboxesFetched = true;


        require(["template!TVPris.Templates.RelatedInfotext1"], (rit) => {
            relatedInfotextTemplate = rit;

            const ctx = prepareContext(
                data,
                ps,
                ms,
                mareas,
                container,
                infoboxes
            );

            const html = jQuery(relatedInfotextTemplate(ctx));

            html.find("[data-infobox]").each((index_, element) => infoBox(
                jQuery(element),
                null
            ));

            const h = (event) => displayRelatedInfoDialog(event, ctx);
            const link = html.find("#tpk-related-info-link");
            link.click(h);
            link.keydown((event) => event.stopPropagation());


            jQuery(container).append(html);

            /*
             * The modal cannot be inside the container because we
             * don't want input events to propagate to the container.
             */
            const modal = jQuery("#related-infotext-2");
            modal.remove();
            jQuery("body").append(modal);
        });

    };

    const queryInfoBoxes = (data, ps, ms, mareas, container) => {
        while (container.firstChild) {
            container.removeChild(container.firstChild);
        }
        const ibs = [];
        if (ps.length > 0) {
            ibs.push("B10");
        }
        if (ms.length > 0) {
            ibs.push("B11");
        }
        jQuery.ajax({
            "url": "/rest/tpk-infobox",
            "data": {
                "infoboxes": ibs,
                "measures": ms,
                "packages": ps,
                "mareas": mareas
            }
        }).always(notifyer.progress("$services.localization.render('tpk.fetching-infotext')")).
            fail((jqXHR, textStatus, errorThrown) => notifyer.
                error("$services.localization.render('tpk.fetching-infotext-failed') " + textStatus + " " + errorThrown)).
            done((infoboxes) => {
                const {measures, packages} = infoboxes;
                const list = jQuery("<ul />");
                const codes = Object.keys(measures);
                for (let i = 0; i < codes.length; i += 1) {
                    const c = codes[i];
                    const m = measures[c];
                    const code = jQuery("<div class=\"code\"/>");
                    code.text(m.code);
                    const title = jQuery("<div class=\"title\"/>");
                    title.text(m.title);
                    const li = jQuery("<li/>");
                    li.append(code);
                    li.append(title);
                    list.append(li);
                }
                jQuery(container).append(list);
                const pcodes = Object.keys(packages);
                for (let i = 0; i < pcodes.length; i += 1) {
                    const c = pcodes[i];
                    const html = packages[c].description;
                    const div = jQuery("<div/>");
                    div.html(html);
                    jQuery(container).append(div);
                }
            });
    };

    return queryInfoBoxes;
});
