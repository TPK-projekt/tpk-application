/*
 *  Copyright (C) 2017  Andreas Jonsson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* eslint-env browser, amd, es6 */
define(["jquery"], (jQuery) => {
    "use strict";

    const defaults = {
        "className": "sort-control",
        "stateAsc": "state-asc",
        "stateDesc": "state-desc",
        "eventBus": null,
        "state": null
    };

    const isDefined = (x) => typeof x !== "undefined";
    const isUndefined = (x) => !isDefined(x);

    function SortControl (options) {
        const o = (n) => {
            if (isUndefined(options[n])) {
                if (isUndefined(defaults[n])) {
                    throw Error("Missing mandatory option: " + n);
                }
                this[n] = defaults[n];
            } else {
                this[n] = options[n];
            }
        };
        o("className");
        o("stateAsc");
        o("stateDesc");
        o("selector");
        o("state");
        o("eventBus");

        if (this.eventBus === null) {
            this.eventBus = jQuery(this);
        }

        this.on();
    }

    const p = SortControl.prototype;

    p.each = function each (f) {
        jQuery(this.selector).find("." + this.className).each(f);
    };

    p.setState = function setState (field, order) {
        this.each((_index, element) => {
            const el = jQuery(element);
            el.removeClass(this.stateAsc + " " + this.stateDesc);
            if (el.attr("data-field") === field) {
                if (order === "asc") {
                    el.addClass(this.stateAsc);
                } else if (order === "desc") {
                    el.addClass(this.stateDesc);
                }
            }
        });
        this.state = [field, order];
        this.eventBus.trigger("sort-order-change", this.state);
    };

    p.toggle = function toggle (field) {
        if (this.state === null || this.state[0] !== field) {
            this.state = [field, null];
        }
        this.setState(field, this.state[1] === "desc" ? "asc" : "desc");
    };

    p.off = function off () {
        this.each((_index, element) => {
            jQuery(element).off("click");
        });
    };

    p.on = function on () {
        if (this.state !== null) {
            this.setState(this.state[0], this.state[1]);
        }
        this.each((_index, element) => {
            jQuery(element).click(() => {
                const field = jQuery(element).attr("data-field");
                this.toggle(field);
            });
        });
    };

    return SortControl;
});
