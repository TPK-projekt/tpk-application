/**
 *  Copyright (C) 2017  Andreas Jonsson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* eslint-env browser, amd, es6 */
define(["jquery", "EventBus"], (jQuery, EventBus) => {
    "use strict";

    const isObj = (x) => isJust(x) && typeof x === "object";

    const defaults = {
        "sourceContext": null,
        "placeHolder": null,
        "itemClass": "selection-presenter-item",
        "placeHolderClasses": [],
        "itemEventHandlers": [],
        "delimiter": null,
        "htmlSource": false,
        "removeAriaLabel": "Ta bort"
    };

    const eb = jQuery(EventBus);

    function SelectionPresenter (options) {

        const o = (n) => {
            if (isUndefined(options[n])) {
                if (isUndefined(defaults[n])) {
                    throw new Error("Missing mandatory option: " + n);
                }
                this[n] = defaults[n];
            } else {
                this[n] = options[n];
            }
        };

        o("container");
        o("events");
        o("source");
        o("sourceContext");
        o("itemClass");
        o("placeHolder");
        o("placeHolderClasses");
        o("itemEventHandlers");
        o("delimiter");
        o("htmlSource");
        o("removeAriaLabel");

        if (this.placeHolderClasses.length === 0) {
            this.placeHolderClasses.push("selection-placeholder");
        }

        this.container = jQuery(this.container);

        eb.on(this.events, this.update.bind(this));
        this.update();
    }

    const p = SelectionPresenter.prototype;

    p.update = function update () {
        this.empty();
        this.render();
    };

    p.empty = function empty () {
        if (this.containerIsInput()) {
            if (this.placeHolder === null) {
                this.container.val("");
            } else {
                this.container.attr("placeholder", "");
            }
        } else {
            this.container.empty();
        }
    };

    p.render = function render () {
        const items = this.source.call(this.sourceContext);
        if ((isNothing(items) || items.length === 0) &&
            isJust(this.placeHolder)) {
            this.renderItem(this.placeHolder, this.placeHolderClasses);
        }
        for (let i = 0; isJust(items) && i < items.length; i += 1) {
            if (this.delimiter !== null && i !== 0) {
                this.container.append(this.delimiter);
            }
            this.renderItem(items[i], []);
        }
    };

    p.itemElement = function itemElement (text) {
        const element = jQuery("<span/>");
        if (this.htmlSource) {
            element.html(text);
        } else {
            element.text(text);
        }
        return element;
    };

    p.renderItem = function renderItem (item, classes) {
        if (this.containerIsInput()) {
            if (this.placeHolder === null) {
                let s = this.container.val();
                if (isNothing(s)) {
                    s = "";
                }
                if (s !== "" && this.delimiter) {
                    s += this.delimiter;
                }
                s += item;
                this.container.val(s);
            } else {
                let ph = this.container.attr("placeholder");
                if (ph && this.delimiter) {
                    ph += this.delimiter;
                }
                this.container.attr("placeholder", ph + item);
                this.container.val("");
            }
        } else {
            let elem = null;
            if (isObj(item)) {
                elem = this.itemElement(item.text);
                if (isFunc(item.remove)) {
                    const close = jQuery('<button class="fa fa-close selection-presenter-remove"></button>');
                    close.attr("aria-label", (isJust(item.removeAriaLabel) ? item : this).removeAriaLabel);
                    elem.prepend(close);
                    close.click(item.remove);
                }
            } else {
                elem = this.itemElement(item);
            }
            elem.addClass([this.itemClass].concat(classes).join(" "));
            for (const ev in this.itemEventHandlers) {
                if (Object.prototype.hasOwnProperty.
                    call(this.itemEventHandlers, ev)) {
                    elem.on(ev, this.itemEventHandlers[ev]);
                }
            }
            this.container.append(elem);
        }
    };

    p.containerIsInput = function containerIsInput () {
        return this.container.length === 1 && this.container.get(0).tagName.toLowerCase() === "input";
    };

    return SelectionPresenter;
});
