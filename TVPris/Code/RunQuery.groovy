{{groovy output="none"}}
import java.sql.Types
import java.sql.Array

class QueryRunner {

    private final List<Closure> parameterSetters = new LinkedList<>()

    public final def runQuery

    public final def addParameterSetter

    private final def xwiki

    private final def xcontext

    public QueryRunner(String mode, xwiki, xcontext) {
        this.xwiki = xwiki
        this.xcontext = xcontext
        if ("native".equals(mode)) {
            this.runQuery = runQueryNative
            this.addParameterSetter = addParameterSetterNative
        }
        if ("hql".equals(mode)) {
            this.runQuery = runQueryHql
            this.addParameterSetter = addParameterSetterHql
        }
    }

    private def runQueryNative = {queryStr, lim, off ->
        if (lim != null) {
            queryStr += ' LIMIT ' + lim
        }
        if (off != null) {
            queryStr += ' OFFSET ' + off
        }
        def context2 = xcontext.getContext()
        def store = xwiki.getXWiki().getStore().getStore()
        store.beginTransaction(context2)
        def session = store.getSession(context2)
        def connection = session.connection()
        def list = new ArrayList()
        def stmt = connection.prepareStatement(queryStr)
        try {
            for (ps in parameterSetters) {
                ps(stmt, connection)
            }
            def resultset = stmt.executeQuery()
            def mdata = resultset.getMetaData()
            def nbcols = mdata.getColumnCount()
            while (resultset.next()) {
                if (nbcols > 1) {
                    def row = new ArrayList()
                    for (int i = 0; i < nbcols; i++) {
                        row.add(resultset.getObject(i + 1))
                    }
                    list.add(row)
                } else {
                    list.add(resultset.getObject(1))
                }
            }
        } finally {
            stmt.close()
            store.endTransaction(context2, true)
        }
        return list
    }

    private def runQueryHql = {queryStr, lim, off ->
        def query = services.query.hql(queryStr)
        if (lim != null) {
            query.setLimit(limit)
        }
        if (off != null) {
            query.setOffset(offset)
        }
        for (ps in parameterSetters) {
            ps(query)
        }
        return query.execute()
    }

    def addParameterSetterNative = { x ->
        int index = parameterSetters.size() + 1
        parameterSetters.add({ preparedStatement, connection ->
            if (x instanceof Integer) {
                preparedStatement.setInt(index, x)
            } else if (x instanceof Long) {
		preparedStatement.setLong(index, x)
	    } else if (x instanceof Collection) {
                Object [] array = x.toArray()
                Array jdbcArray
                if (array.length > 0 && array[0] instanceof Integer) {
                    jdbcArray = connection.createArrayOf("integer", array);
                } else {
                    jdbcArray = connection.createArrayOf("text", array);
                }
                preparedStatement.setArray(index, jdbcArray)
            } else if (x instanceof String) {
                preparedStatement.setString(index, x)
	    } else if (x instanceof Boolean) {
		preparedStatement.setBoolean(index, x)
            } else {
                throw new RuntimeException("Unsupported parameter type: " + x.getClass().getName())
            }
        })
        return this
    }

    def addParameterSetterHql = { x ->
        int index = parameterSetters.size() + 1
        parameterSetters.add({ query -> query.bindValue((int) index, x) })
        return this
    }

    public QueryRunner reset() {
        parameterSetters.clear()
        return this
    }

}
{{/groovy}}
