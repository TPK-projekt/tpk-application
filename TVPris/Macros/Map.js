require(['jquery', 'openlayers', 'EventBus'],
        function ($, ol, EventBus) {

            var eb = $(EventBus);
            var sweden = ol.proj.fromLonLat([16.8490, 63]);
            var swedenBoundingBox = [55.1331192,69.0599699,10.592077,24.1776819];
            var markerIconUrl = new XWiki.Document(XWiki.Model.resolve('TVPris.Icons', XWiki.EntityType.DOCUMENT)).getURL('download') + '/map_marker.svg';
            console.log(markerIconUrl);

            var inSwedenBoundingBox = function (lon, lat) {
                return lat >= swedenBoundingBox[0] && lat <= swedenBoundingBox[1] &&
                    lon >= swedenBoundingBox[2] && lon <= swedenBoundingBox[3];
            };

            var initMap = function() {

                $('.tvpris-map').each(function ( index, element ) {
                    var vectorLayer;
                    var map
                    var markersVisible = false;
                    var countiesVisible = false;
                    var municipalitiesVisible = false;
                    var markersCutoffResolution = 1000;
                    var vectorSource = null;
                    var municipalitySource = new ol.source.Vector();
                    var countySource = new ol.source.Vector();
                    var editableSurgery = null;
                    var editableSurgeryMarker = null;
                    var surgeries = null;
                    var activeSurgeries;
                    var center = sweden;
                    var initialZoom = ($(this).height() ? Math.log2($(this).height()) * 0.45 : 4);

                    try {
                        var s = $(element).attr('data-surgeries');
                        if (typeof(s) === 'string') {
                            surgeries = new Map();

                            var addSurgeries = function(ss) {
                                for (var i = 0; i < ss.length; i++) {
                                    surgeries.set(ss[i].id, ss[i]);
                                }
                            }

                            eb.on('surgeries-added', addSurgeries);

                            addSurgeries(surgeries, JSON.parse(s))
                        }

                        s = $(element).attr('data-active-surgeries');
                        if (typeof(s) === 'string') {
                            activeSurgeries = new Set(JSON.parse(s))
                        }
                    } catch (e) {
                    }


                    try {
                        editableSurgery = JSON.parse($(element).attr('data-editable-surgery').replace(/(&#123;)|(&#34;)/g, function (s) {
                            switch (s) {
                            case '&#123;': return '{';
                            case '&#34;': return '"';
                            }
                        } ));
                        center = ol.proj.fromLonLat([editableSurgery.lat, editableSurgery.lon]);
                        initialZoom = 8;
                    } catch (e) {
                    }

                    var view = new ol.View({
                        center: center,
                        zoom: initialZoom
                    });


                    var countyLayer = new ol.layer.Vector({
                        source: countySource
                    });

                    var municipalityLayer = new ol.layer.Vector({
                        source: municipalitySource
                    });

                    var selectFromPixel = function (pixel) {
                        if (countyLayer !== null) {
                            map.forEachFeatureAtPixel(pixel, function (feature) {
                                eb.trigger('county-selected', [feature.get('name')]);
                            },
                                                      { layerFilter: function (layer) { return layer === countyLayer }});
                        }
                        if (municipalityLayer !== null) {
                            map.forEachFeatureAtPixel(pixel, function (feature) {
                                eb.trigger('municipality-selected', [feature.get('name')]);
                            },
                                                      { layerFilter: function (layer) { return layer === municipalityLayer }});
                        }
                        if (editableSurgeryMarker !== null) {
                            var c = map.getCoordinateFromPixel(pixel);
                            editableSurgeryMarker.setGeometry(new ol.geom.Point(c));
                            eb.trigger('editable-surgery-updated', [ol.proj.transform(c, 'EPSG:3857', 'EPSG:4326')]);
                        }
                    };

                    var selectPosition = function(lon, lat) {
                        if (typeof(lon) !== 'undefined' && typeof(lat) !== 'undefined' && inSwedenBoundingBox(Number(lon), Number(lat))) {
                            var c = ol.proj.transform([Number(lon), Number(lat)], 'EPSG:4326', 'EPSG:3857');
			    alert('coordinate: ' + JSON.stringify(c));
                            view.setCenter(c);
                            view.setZoom(10);
			    setTimeout(function() { selectFromPixel(map.getPixelFromCoordinate(c)); }, 50);
                        }
                    };

                    eb.on('query-location', function (event, q) {
                        $.ajax({
                            url: 'https://nominatim.openstreetmap.org/search.php',
                            data: {
                                q: q,
                                format: 'json',
                                limit: 1
                            },
                            success: function(data) {
                                if (data.size() > 0) {
                                    var p = data[0];
                                    selectPosition(p.lon, p.lat);
                                }
                            }
                        }).fail(function (jqXHR, textStatus, errorThrown ) {
                            new XWiki.widgets.Notification("$services.localization.render('map_failed_to_fetch_location'): " + textStatus + ' ' + errorThrown, 'error');
                        });
                    })

                    var borderStyle = new ol.style.Style({
                        fill: new ol.style.Fill({color: [0, 0, 0, 0]}),
                        stroke: new ol.style.Stroke({
                            color: [0, 0, 0, 0.04],
                            width: 0,
                            opacity: 0.1
                        })
                    });

                    var setMarkersVisible = function () {
                        markersVisible = true;
                        map.addLayer(vectorLayer);
                    }

                    var setMarkersInvisible = function () {
                        markersVisible = false;
                        map.removeLayer(vectorLayer);
                    }

                    var setCountiesVisible = function () {
                        countiesVisible = true;
                        map.addLayer(countyLayer);
                    }

                    var setCountiesInvisible = function () {
                        countiesVisible = false;
                        map.removeLayer(countyLayer);
                    }

                    var setMunicipalitiesVisible = function () {
                        municipalitiesVisible = true;
                        map.addLayer(municipalityLayer);
                    }

                    var setMunicipalitiesInvisible = function () {
                        countiesVisible = false;
                        map.removeLayer(municipalityLayer);
                    }

                    var createMarker = function(surgery) {
                        var marker = new ol.Feature({
                            geometry: new ol.geom.Point(ol.proj.transform([surgery.lat, surgery.lon], 'EPSG:4326', 'EPSG:3857')),
                            labelPoint: new ol.geom.Point(ol.proj.transform([surgery.lat, surgery.lon], 'EPSG:4326', 'EPSG:3857')),
                            name: surgery.name,
                            surgeryId: surgery.id
                        });

                        if (editableSurgery !== null && editableSurgery.id == surgery.id) {
                            console.log('Setting editable surgery marker for ' + surgery.id + ' (' + JSON.stringify(surgery) + ')');
                            editableSurgeryMarker = marker;
                        }

                        marker.setStyle([iconStyle,
                                         new ol.style.Style({
                                             text: new ol.style.Text({
                                                 text: surgery.name,
                                                 offsetY: -60,
                                                 fill: new ol.style.Fill({
                                                     color: '#333'
                                                 }),
                                                 stroke: new ol.style.Stroke({
                                                     color: '#fff',
                                                     width: 4
                                                 })
                                             })
                                         })
                                        ]);

                        return marker;
                    };

                    var addMarkers = function(markers) {
                        console.log('adding markers');
                        var iconFeatures=[];

                        for (var i = 0; i < markers.length; i++) {
                            var surgery = markers[i];
                            var marker = createMarker(surgery);

                            iconFeatures.push(marker);
                        }
                        if (vectorSource === null) {
                            console.log('vector source was null');
                            vectorSource = new ol.source.Vector({
                                features: iconFeatures //add an array of features
                            });

                            vectorLayer = new ol.layer.Vector({
                                source: vectorSource
                            });
                        } else {
                            vectorSource.addFeatures(iconFeatures);
                        }

                        if (surgeries !== null)   {
                            var extent = vectorSource.getExtent();
                            var zoomToExtent = new ol.control.ZoomToExtent({ extent: extent });
                            map.addControl(zoomToExtent);
                            map.getView().fit(extent, {padding: [5, 5, 5, 5]});
                        }

                    }

                    var elem = this
                    map = new ol.Map({
                        target: elem,
                        layers: [
                            new ol.layer.Tile({
                                preload: 4,
                                source: new ol.source.XYZ({ url: '/osm_tiles/{z}/{x}/{y}.png' })
                            })
                        ],
                        controls: new ol.Collection([
                            new ol.control.Zoom(),
                            new ol.control.Attribution()
                        ]),
                        // Improve user experience by loading tiles while animating. Will make
                        // animations stutter on mobile or slow devices.
                        loadTilesWhileAnimating: true,
                        view: view
                    });

                    /*
                      var timeoutId = null;

                      var updateVisible = function()  {
                      timeoutId = null;
                      if (markersVisible) {
                      var y = $(elem).height();
                      var x = $(elem).width();
                      var c1 = map.getCoordinateFromPixel([0, y]);
                      var c2 = map.getCoordinateFromPixel([x, 0]);

                      var visibleSurgeries = []
                      vectorSource.forEachFeatureInExtent(c1.concat(c2),
                      function (feature) {
                      visibleSurgeries.push(feature.get('surgeryId'));
                      });
                      $('#surgeries-input').val(visibleSurgeries.join(';'));
                      } else {
                      $('#surgeries-input').val('');
                      }
                      }

                      map.on('postrender', function () {
                      if (timeoutId != null) {
                      clearTimeout(timeoutId);
                      }
                      timeoutId = setTimeout(updateVisible, 200)
                      });
                    */

                    if (surgeries === null && editableSurgery === null) {
                        map.getView().on('propertychange', function (e) {
                            switch (e.key) {
                            case 'resolution':
                                var r = e.target.get(e.key);
                                if (markersVisible && r >= markersCutoffResolution) {
                                    setMarkersInvisible();
                                }
                                if (!markersVisible && r < markersCutoffResolution) {
                                    setMarkersVisible();
                                }
                                break;
                            }
                        });
                    }

                    var iconStyle =
                        new ol.style.Style({
                            image: new ol.style.Icon({
                                anchor: [0.5, 40],
                                anchorXUnits: 'fraction',
                                anchorYUnits: 'pixels',
                                opacity: 0.75,
                                src: markerIconUrl
                            })
                        });

                    data = {};
                    if (surgeries !== null) {
                        data.s = surgeries.join('|');
                    }
                    if (surgeries !== null || editableSurgery === null) {
                        $.ajax({
                            url: "$xwiki.getURL('TVPris.Services.Surgeries')?xpage=plain&outputSyntax=plain",
                            data: data,
                            success: addMarkers
                        }).fail(function (jqXHR, textStatus, errorThrown ) {
                            new XWiki.widgets.Notification("$services.localization.render('map_failed_to_fetch_surgeries'): " + textStatus + ' ' + errorThrown, 'error');
                        });
                    }
                    if (editableSurgery !== null) {
                        addMarkers([editableSurgery]);
                        setMarkersVisible();
                    }
                    map.on('click', function(event) {
                        selectFromPixel(event.pixel);
                    });

                    var decompressMPolygon = function(mp) {
                        var m0 = [];
                        for (var i = 0; i < mp.length; i++) {
                            var m1 = [];
                            for (var j = 0; j < mp[i].length; j++) {
                                var m2 = [];
                                var p = null;
                                for (var k = 0; k < mp[i][j].length; k++) {
                                    var c = mp[i][j][k];
                                    if (p !== null) {
                                        c[0] = p[0] + c[0];
                                        c[1] = p[1] + c[1];
                                    }
                                    p = c;
                                    m2.push(c);
                                }
                                m1.push(m2);
                            }
                            m0.push(m1);
                        }
                        return m0;
                    };

                    var mps = [];
                    var addFeature = function (index, element) {
                        var mpolygon = $(element).attr('data-border');
                        var name = $(element).attr('name');
                        var p = decompressMPolygon(JSON.parse(mpolygon))
                        var mp = new ol.Feature({
                            geometry: new ol.geom.MultiPolygon(p),
                            name: name
                        });
                        mp.setStyle([borderStyle]);
                        mps.push(mp);
                    };

                    $('#tvpris-county-input option[data-border]').each(addFeature);
                    countySource.addFeatures(mps);
                    setCountiesVisible();
                    mps = [];
                    $('#tvpris-municipality-input option[data-border]').each(addFeature);
                    municipalitySource.addFeatures(mps);
                    setMunicipalitiesVisible();
                    mps = null;

                    eb.on('current-position', function(e, curPos) {
                        if (typeof(curPos.latitude) === 'number' && typeof(curPos.longitude) === 'number') {
                            selectPosition(curPos.longitude, curPos.latitude);
                        }
                    });
                });

            };

            EventBus.onInit('application-init', initMap);
        });
