/* eslint-env browser, amd, es6 */
require(["jquery"], (jQuery) => {
    "use strict";

    const e = jQuery("<li><a href=\"#\" id=\"tpk-break-cache\" title=\"Bryt cache\"><span class=\"application-img\"><span class=\"fa fa-shower\"></span></span><span class=\"application-label\">Rensa cachen</span></a></li>");
    const panel = jQuery("ul.applicationsPanel:not(.applicationsPanelMoreList)");
    panel.prepend(e);
    const link = jQuery("#tpk-break-cache");
    link.click(() => {
        const cacheVersion = window.TPK.cache_version;
        const note = new XWiki.widgets.Notification("$services.localization.render('tpk.updating-cache')", "inprogress");
        jQuery.ajax({
            "url": "$xwiki.getURL('TVPris.Admin.BreakCache', 'view', 'xpage=plain&outputSyntax=plain')",
            "data": {
                "cache_version": cacheVersion,
                "form_token": jQuery("html").attr("data-xwiki-form-token")
            }
        }).
            fail((jqXHR, textStatus, errorThrown) => {
                if (typeof jqXHR.responseJSON !== "undefined" && typeof jqXHR.responseJSON.error !== "undefined") {
                    new XWiki.widgets.Notification("$services.localization.render('tpk.updating-cache-failure'): " + jqXHR.responseJSON.error, "error");
                } else {
                    new XWiki.widgets.Notification("$services.localization.render('tpk.updating-cache-failure'): " + textStatus + " " + errorThrown, "error");
                }
            }).
            always(() => note.hide()).
            done((data) => {
                window.TPK.cache_version = data.new_cache_version;
                new XWiki.widgets.Notification("$services.localization.render('tpk.updating-cache-success'): " + data.new_cache_version, "success");
            });
    });
});
