{{include reference="TVPris.Code.RunQuery"/}}
{{velocity}}#includeInContext('TVPris.Model.Declaration'){{/velocity}}{{groovy}}
import org.xwiki.velocity.VelocityManager

def debug = false

def retVal = new HashMap<String, Object>()
VelocityManager velocityManager = services.component.getInstance(VelocityManager.class)
def vcontext = velocityManager.getVelocityContext()

def getParameter(id) {
  return request.getParameter(tpk_model.get(id).name)
}

List<Closure> parameterSetters = new LinkedList<>()

List<String> errorList = new LinkedList<>()


Iterable<Integer> getIntListParam(String id, List<String> errorList) {
  Set<Integer> result = new TreeSet<>()
  def param = getParameter(id)
  if (param != null) {
    for (tp in param.split(";")) {
      try {
	int x = Integer.parseInt(tp.trim())
	result.add(x)
      } catch (NumberFormatException e) {
	errorList.add('Invalid value fragment in ' + tpk_model.get(id).name + ' parameter: ' + tp)
      }
    }
  }
  return result
}

if (vcontext.get('doCompare') == null || vcontext.get('doCompare')) {

  Iterable<Integer> surgeries = getIntListParam('surgeries-input', errorList)
  Iterable<Integer> measures = getIntListParam('measures-input', errorList)
  Iterable<Integer> packages = getIntListParam('package-input-select', errorList)

  if (packages.size() == 0 && measures.size() == 0) {
    // Do standard comparison.  For now: use package 1.
    packages.add(1);
  }

  Integer limit = null
  Integer offset = null
  Integer itemCount = null
  boolean countOnly = false
  boolean eveningHours = false
  boolean weekendHours = false
  String county = getParameter('tvpris-county-input')
  Set counties = new TreeSet()
  if (county != null && county != '') {
    for (c in county.split(ARRAY_DELIM)) {
      counties.add(c)
    }
  }
  String municipality = getParameter('tvpris-municipality-input')
  Set municipalities = new TreeSet()
  if (municipality != null && municipality != '') {
    for (c in municipality.split(ARRAY_DELIM)) {
      municipalities.add(c)
    }
  }
  Set categories = new TreeSet()
  String surgeryCategory = getParameter('surgery-category-input')
  if (surgeryCategory != null && surgeryCategory != '') {
    for (c in surgeryCategory.split(ARRAY_DELIM)) {
      categories.add(c)
    }
  }
  if (getParameter('tvpris-limit')) {
    try {
      limit = Integer.parseInt(getParameter('tvpris-limit'))
    } catch (NumberFormatException e) {
    }
  }
  if (limit == null) {
    limit = 20
  }
  if (request.get('offset')) {
    try {
      offset = Integer.parseInt(request.get('offset'))
    } catch (NumberFormatException e) {
    }
  }
  if (request.get('count')) {
    try {
      itemCount = Integer.parseInt(request.get('count'))
    } catch (NumberFormatException e) {
    }
  }
  if (request.get('co')) {
    countOnly = true
  }
  if (getParameter('tvpris-evening-hours')) {
    eveningHours = true
  }
  if (getParameter('tvpris-weekend-hours')) {
    weekendHours = true
  }
  String sort_field = 'price_total'
  boolean wantDeviation = false
  String reqSortField = getParameter('tvpris-sort-field')
  String reqSortOrder = getParameter('tvpris-sort-order')
  if (reqSortField == 'sn' ) {
    sort_field = 'tvps_name'
  }
  if (reqSortField == 'deviation') {
    sort_field = 'deviation'
    wantDeviation = !countOnly
  }
  if (reqSortField == 'price') {
    sort_field = 'price_total'
  }

  String sort_order = 'ASC'
  if (reqSortOrder == 'desc') {
    sort_order = 'DESC NULLS LAST'
  }

  def columns = [
    ['surgery_id',              'tvps_surgery_id'],
    ['surgery_name',            'tvps_name'],
    ['surgery_county',          'tvps_county'],
    ['surgery_municipality',    'tvps_municipality'],
    ['surgery_category',        'tvps_category'],
    ['surgery_address',         'tvpse_address'],
    ['surgery_price_comment',   'tvpse_price_comment'],
    ['surgery_lat',             'tvpse_latitude'],
    ['surgery_lon',             'tvpse_longitude'],
    ['price_total',             'sum(tvpip_price) AS price_total'],
    ['reference_price',         'sum(tvpi_price) AS reference_price_total'],
    ['reference_price_special', 'sum(tvpi_price_special) AS reference_price_special_total'],
    ['items',                   "array_agg(tvpip_item_id) as items"],
    ['is_m',                    "array_agg(is_measure) as is_m"],
    ['comment',                 'array_agg(tvppc_text) as comments'],
    ['n',                       "count(tvpip_price) AS n"]
  ]

  boolean wantEditable = eveningHours || weekendHours || !countOnly
  boolean wantComment = !countOnly
  QueryRunner queryRunner = new QueryRunner('native', xwiki, xcontext)
  StringBuilder qb = new StringBuilder("WITH version (version) AS (SELECT tpkv_version  FROM tvpris_versionclass JOIN xwikiobjects USING (xwo_id) WHERE xwo_name = ?) SELECT ")
  queryRunner.addParameterSetter(services.tpkCompare.VERSION_DOCUMENT)
  def itemJoinCondition = { initialAnd, isMeasureField, isMeasureInt, itemIdField ->
    boolean requireRParen = false
    String isMeasure;
    String isNotMeasure;

    if (isMeasureInt) {
      isMeasure    = isMeasureField + ' != 0'
      isNotMeasure = isMeasureField +  ' = 0'
    } else {
      isMeasure    = isMeasureField
      isNotMeasure = 'NOT ' + isMeasureField
    }

    if (measures.size() > 0) {
      qb.append(initialAnd ? ' AND (' : ' (')
      requireRParen = true
      qb.append(isMeasure).append(' AND ').append(itemIdField).append(' = ANY (?)')
      queryRunner.addParameterSetter(measures)
    }
    if (packages.size() > 0) {
      if (requireRParen) {
	qb.append(initialAnd ? ' OR ' : ' ')
      } else {
	qb.append(initialAnd ? ' AND (' : ' (')
      }
      requireRParen = true
      qb.append(isNotMeasure).append(' AND ').append(itemIdField).append(' = ANY (?)')
      queryRunner.addParameterSetter(packages)
    }
    if (requireRParen) {
      qb.append(')')
    }
  }

  if (countOnly) {
    qb.append('count(distinct tvps_surgery_id) as count')
  } else {
    boolean first = true
    for (c in columns) {
      if (first) {
	first = false
      } else {
	qb.append(', ')
      }
      qb.append(c[1])
    }
  }
  if (wantDeviation) {
    qb.append(', cast(abs(price_total - reference_price) as double precision)/ reference_price as deviation')
  }
  qb.append(' FROM version JOIN tvpris_surgery ON true')
  if (wantEditable) {
    qb.append(' JOIN tvpris_surgeryedit ON tvps_surgery_editable_id = tvpris_surgeryedit.xwo_id')
  }
  qb.append(' JOIN tvpris_item ON tvpi_version = version')
  itemJoinCondition(true, 'is_measure', false, 'tvpi_code')
  qb.append(' LEFT OUTER JOIN tpk_itemprice    ON tvpip_version = version AND tvpip_surgery_id = tvps_surgery_id AND')
  qb.append(' ((tvpip_type = 0 OR tvpip_type = 1) AND tvpip_is_measure = is_measure AND tvpip_item_id = tvpi_code)')
  if (wantComment) {
    qb.append(' LEFT OUTER JOIN tvpris_publicpricecomment ON    tvppc_surgery_id = tvps_surgery_id AND tvpi_code = tvppc_item_id AND is_measure = (tvppc_is_measure != 0) AND (tvppc_version IS NULL OR tvppc_version = version)')
  }

  boolean whereAdded = false
  def conjunction = { c, s, p ->
    if (c) {
      if (!whereAdded) {
	qb.append(' WHERE')
	whereAdded = true
      } else {
	qb.append(' AND')
      }
      qb.append(s)
      if (p != null) {
	queryRunner.addParameterSetter(p)
      }
    }
  };
  if (surgeries.size() > 0) {
    whereAdded = true
    qb.append(' WHERE tvps_surgery_id = ANY (?)')
    queryRunner.addParameterSetter(surgeries)
  }
  conjunction(weekendHours, ' tvpse_weekend_hours != 0', null)
  conjunction(eveningHours, ' tvpse_evening_hours != 0', null)
  conjunction(counties.size() > 0, ' tvps_county = ANY (?)', counties)
  conjunction(municipalities.size() > 0, ' tvps_municipality = ANY (?)', municipalities)
  conjunction(categories.size() > 0, ' tvps_category = ANY (?)', categories)

  if (!countOnly) {
    qb.append(' GROUP BY tvps_surgery_id, tvps_name, tvps_county, tvps_municipality, tvps_category, tvpse_address, tvpse_latitude, tvpse_longitude')
    if (wantEditable) {
      qb.append(', tvpse_price_comment');
    }
    qb.append(' ORDER BY ')
    if (sort_field == 'price_total') {
      qb.append('n DESC, ')
    }
    qb.append(sort_field).append(' ').append(sort_order)
  }

  def query = qb.toString()

  if (debug) {
    println('{{{')
    println(query)
    println('}}}')
  }

  def result = queryRunner.runQuery(query, limit, offset)

  if (countOnly) {
    if (result.size() == 0) {
      retVal.put('error', 'No result in count!')
      response.setStatus(response.SC_INTERNAL_SERVER_ERROR)
    } else {
      if (result.size() > 1) {
	retVal.put('warning', 'More than one count in result!')
      }
      retVal.put('count', result.get(0))
    }
  } else {
    def results = new ArrayList(result.size())

    Double referencePrice = null
    Double referencePriceSpecial = null

    for (row in result) {
      def map = new HashMap()
      int i = 0
      for (c in columns) {
	if (c[0] == 'items' || c[0] == 'is_m' || c[0] == 'comment') {
	  def a = row[i].getArray()
	  if (c[0] == 'comment') {
	    a = a.collect({ x ->
	      if (x != null) {
		def xdom = services.rendering.parse(x, 'xwiki/2.1')
		return services.rendering.render(xdom, 'xhtml/1.0')
	      }
	      return null
			  })
	  }
	  map.put(c[0], a)
	} else if (c[0] == 'reference_price') {
	  if (referencePrice == null) {
	    referencePrice = row[i]
	  } else if (referencePrice != row[i]) {
	    errorList.add('Row ' + i + ' has different reference price (' + referencePrice + ') reference price: ' + referencePrice)
	  }
	} else if (c[0] == 'reference_price_special') {
	  if (referencePriceSpecial == null) {
	    referencePriceSpecial = row[i]
	  } else if (referencePriceSpecial != row[i]) {
	    errorList.add('Row ' + i + ' has different reference price special (' + referencePriceSpecial + ') reference price special: ' + referencePriceSpecial)
	  }
	} else {
	  def v = row[i]
	  if (c[0] == 'surgery_price_comment' && v != null) {
	    def xdom = services.rendering.parse(v, 'xwiki/2.1')
	    v = services.rendering.render(xdom, 'xhtml/1.0')
	  }
	  map.put(c[0], v)
	}
	i++
      }
      results.add(map)
    }

    retVal.put('reference_price', referencePrice)
    retVal.put('reference_price_special', referencePriceSpecial)
    retVal.put('r', results)
    retVal.put('n', packages.size() + measures.size())
    if (itemCount != null) {
      retVal.put('count', itemCount)
    }
  }

  if (errorList.size() > 0) {
    retVal.put('errors', errorList)
  }

  vcontext.put('tpkCompareResult', retVal)
  vcontext.put('tpkCompareSurgeries', surgeries)
  vcontext.put('tpkCompareMeasures', measures)
  vcontext.put('tpkComparePackages', packages)
  vcontext.put('tpkOffset', offset)
  vcontext.put('tpkLimit', limit)
}
{{/groovy}}
