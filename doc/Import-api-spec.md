
API för import av data till Tandpriskollen
==========================================

Datat i tjänsten består av 4 olika typer av objekt:

* Åtgärder
* Behandlingspaket
* Mottagningar
* Prisuppgifter

Åtgärderna, behandlingspaketen och prisuppgifterna associeras med ett gemensamt versionsnummer som särskiljer olika omgångar av data.  Mottagningarna berörs ej av detta versionsnummer.

Uppdatering av datat sker enligt dessa steg:

1. Importera åtgärder
2. Importera behandlingspaket
3. Importera mottagningar (uppdatering av mottagningar)
4. Importera priser
5. Öka versionsnummret med 1.
6. Ta bort gamla mottagningar.
7. Ta bort gammal data.

Varje steg genomförs genom att tabellformaterad data laddas upp till tjänstens API enligt den metod som beskrivs i detta dokument.

## Generell information

### Autenticering

Autenticering till API:et sker med användarnamn och lösenord via http basic authentication.  Användaren måste ha administrativa rättigheter till systemet.

### CSV-data

Vad gäller data på CSV-format skall den ha den teckenkodning som specificieras i Content-Type-fältet som anges i http-meddelandets huvud.  Om ingen teckenkodning anges förutsätts utf-8.  Rekommenderad teckenkodning är utf-8.

Formatet är anpassat för att matcha en export från Excel med Svenska som språkinställning med kommatecken som kolumnseparator.

Det måste finnas en rad med rubriker.

Kolumner skall separeras med komma-tecken.

Rader skall separeras av nyradstecken, alternativt carrige return tecken följt av nyradstecken.

Dataceller får inneslutas i dubbelcitationstecken ".  Citationstecken får dubbleras som escape-sekvens för att låta citationstecknet ingå i datacellen.

Tal formateras enligt svensk standard med kommatecken som decimalseparator.  Siffror får grupperas med hårt mellanslag (&nbsp;  &#160; U+00A0). 
 Negativa tal med minustecken (&#8722; U+2212). Obs! Ej bindestreck.

I detta dokument numreras kolumnerna från vänster med start 0.

### API

Api:et nås via anrop över http.  Bas-url:en är https:// + domännamnet.  Vid lyckat anrop returneras ett tomt JSON-object.  Detta JSON-objekt kan innehålla attributet exceptions vilket är en lista med meddelanden som indikerar icke-fatala problem med indatafilen.

## Åtgärder

### Format

Formatet på tabellen för åtgärdsdata skall vara följande

Rubrikraden används endast för eventuell felsökning.

Varje övrig rad motsvarar en åtgärd.

* Kolumn 0 skall vara åtgärdens 3-siffriga kod.
* Kolumn 1 skall vara åtgärdens rubrik.
* Kolumn 2 skall vara åtgärdens referenspris för allmäntandvård.
* Kolumn 3 skall vara åtgärdens referenspris för specialisttandvård.
* Kolumn 4 skall vara det behandlingsområde åtgärden ingår i för allmäntandvård.
* Kolumn 5 skall vara det behandlingsområde åtgärden ingår i för specialisttandvård.

### API

URL: Basurl + "/rest/tpk-import-measures"
Metod: PUT
Mime-type: text/csv

CSV-dokumentet skickas som innehållet i http-förfrågan.

#### Exempel med curl:

    curl -u <användarnamn>:<lösenord> --header 'Content-Type: text/csv; charset=utf-8' -X PUT --data-binary '@measures.csv' https://tpk-dev.kreablo.se/rest/tpk-import-measures

## Behandlingspaket

### Format

Formatet på tabellen för behandlingspaket skall vara följande.

Rubrikraden används endast för eventuell felsökning.

Varje övrig rad motsvarar ett behandlingspaket.

* Kolumn 0 skall vara behandlingsgruppskoden bestående av en versal bokstav.  (Denna ignoreras för närvarande av tjänsten.)
* Kolumn 1 skall vara namnet på behandlingsgruppen.
* Kolumn 2 skall vara behandlingspaketets textkod bestående av en versal följt av ett nummer och eventuellt en gemen.
* Kolumn 3 skall vara behandlingspaketets titel.
* Kolumn 4 skall vara en beskrivande text för behandlingen.
* Kolumn 5 skall innehålla en ytterligare beskrivande text (beskrivande text nivå två).
* Kolumn 6 skall innehålla en lista av 3-siffriga koder som anger åtgärderna.  Kolumnen kan även innehålla annan text.
* Kolumn 7 skall innehålla behandlingspaketets referenspris.

### API

URL: Basurl + "/rest/tpk-import-packages"
Metod: PUT
Mime-type: text/csv

CSV-dokumentet skickas som innehållet i http-förfrågan.

#### Exempel med curl:

    curl -u <användarnamn>:<lösenord> --header 'Content-Type: text/csv; charset=utf-8' -X PUT --data-binary '@packages.csv' https://tpk-dev.kreablo.se/rest/tpk-import-packages


## Mottagningar

Notera att uppgifter om mottagningarnas plats fylls i genom att sökning på address sker med hjälp av Google Maps.  Därför måste systemet ha konfigurerats med giltig API-nyckel för detta ändamål.  Denna skall anges med konfigurationsnyckeln tpk.googleAPIKeys.maps i /etc/xwiki/xwiki.properties.

Returvärdet från API-anropet är ett JSON-object som kan innehålla attributet warnings med en lista med varningsmeddelanden från de geografiska slagningarna.  Eftersom det är av stor vikt att mottagningarnas geografiska placering är korrekt bör denna lista granskas noggrant och placeringarna manuellt åtgärdas.

### Format uppladdning

Formatet på tabell för mottagningsdata skall vara följande.

Rubrikraden används endast för eventuell felsökning.

Varje övrig rad motsvarar en mottagnings uppgifter.

Klinik,"Vårdgivare TPK","Mottagningsnamn","Besöksadress",Post nr,Ort,"E-post",Webbsida,"Telefonnummer"

* Kolumn 0 skall vara en kod bestående av endast siffrorna 0-9 som unikt identifierar mottagningen.
* Kolumn 1 skall innehålla en textkod som antingen är PRIVAT eller LANDSTING.
* Kolumn 2 skall innehålla län.  (Notera att tjänsten i första hand använder det län som tas fram genom en geografisk sökning på addressen.)
* Kolumn 3 skall innehålla namn på vårdgivare.
* Kolumn 4 skall innehålla namn på mottagningen.
* Kolumn 5 skall innehålla gatuadressen från mottagningens besöksadress.
* Kolumn 6 skall innehålla postnummer från mottagninges besöksadress.
* Kolumn 7 skall innehålla ortsnamn från mottagningens besöksadress.
* Kolumn 8 skall innehålla mottagningens epostadress.
* Kolumn 9 skall innehålla webbadress till mottagningens webbplats.
* Kolumn 10 skall innehålla mottagningens telefonnummer.

### API uppladdning

Befintlinga mottagningar uppdateras om ändrade uppgifter ingår i det uppladdade dokumentet.

URL: Basurl + "/rest/tpk-import-surgeries"
Metod: PUT
Mime-type: text/csv

CSV-dokumentet skickas som innehållet i http-förfrågan.

#### Exempel med curl:

    curl -u <användarnamn>:<lösenord> --header 'Content-Type: text/csv; charset=utf-8' -X PUT --data-binary '@surgeries.csv' https://tpk-dev.kreablo.se/rest/tpk-import-surgeries

### Format borttagning

* Kolumn 0 skall vara en kod bestående av endast siffrorna 0-9 som unikt identifierar mottagningen.

### API borttagning

URL: Basurl + "/rest/tpk-import-surgeries"
Metod: DELETE
Mime-type: text/csv

CSV-dokumentet skickas som innehållet i http-förfrågan.

#### Exempel med curl:

    curl -u <användarnamn>:<lösenord> --header 'Content-Type: text/csv; charset=utf-8' -X DELETE --data-binary '@deleted-surgeries.csv' https://tpk-dev.kreablo.se/rest/tpk-import-surgeries

## Prisuppgifter

Prisuppgifter accepteras endast om motsvarande åtgärd eller behandlingspaket och mottagning redan finns inladdade i systemet.  Prisuppgifter för åtgärder och behandlingspaket får inkluderas i samma dokument eller delas upp i flera separata dokument.  Vad gäller åtgärder så måste priser för specialisttandvård och allmän tandvård laddas upp i separata dokument.

### Format

Rubrikraden skall för varje priskolumn bestå av antingen en 3-siffrig åtgärdskod eller en behandlingspaketkod bestående av versal, nummer och eventuellt en gemen.  Motsvarande kolumn motsvarar således prisupgifter för den åtgärd eller det behandlingspaket som anges av rubriken.  Kolumner som ej rubriceras av en giltig kod ignoreras, bortsett från kolumn 0.

Varje övrig rad anger prisuppgifterna för en specifik mottagning.

* Kolumn 0 skall vara en kod bestående av endast siffrorna 0-9 som unikt identifierar mottagningen.
* Övriga kolumner skall vara priskolumner rubricerade enligt ovan och alltså innehålla prisuppgifter (medianpriser från försäkringskassan).  Dataceller får lämnas tomma för att ange att prisuppgift saknas.


### API uppladdning

URL: Basurl + "/rest/tpk-import-prices"
Metod: PUT
Mime-type: text/csv
Query-parameter: special=true för att ange att åtgärdspriser är för specialisttandvård.

CSV-dokumentet skickas som innehållet i http-förfrågan.

#### Exempel med curl:

    curl -u <användarnamn>:<lösenord> --header 'Content-Type: text/csv; charset=utf-8' -X PUT --data-binary '@prices.csv' https://tpk-dev.kreablo.se/rest/tpk-import-surgeries

## Versionsnummer

Uppladdning av åtgärder, behandlingspaket och prisuppgifter sker till tjänstens nästkommande versionsnummer.  För att datat skall bli aktuellt måste versionsnummret ökas med 1.

### API hämta nuvarande version

URL: Basurl + "/rest/tpk-version"
Metod: GET
Resultat-Mime: application/json

Resultatet är ett object med attributet "version"

#### Exempel med curl:

    curl  https://tpk-dev.kreablo.se/rest/tpk-version

	{ "version":7 }

### API sätt ny version

URL: Basurl + "/rest/tpk-version/<versionsnummer>"
Metod: PUT
Resultat-Mime: application/json

Där versionsnummret skall vara ett heltal.

#### Exempel med curl:

    curl -u <användarnamn>:<lösenord> -X PUT https://tpk-dev.kreablo.se/rest/tpk-import-surgeries/8

### API radera alla objekt tillhörande en specifik version

URL: Basurl + "/rest/tpk-version/<versionsnummer>"
Metod: DELETE
Resultat-Mime: application/json

Där versionsnummret skall vara ett heltal.

Notera att systemet hindrar att aktuell version (den som fås med GET-metoden) tas bort.

#### Exempel med curl:

    curl -u <användarnamn>:<lösenord> -X DELETE https://tpk-dev.kreablo.se/rest/tpk-import-surgeries/6

