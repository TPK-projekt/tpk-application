/* eslint-env browser, amd, es6 */
define([
    "jquery",
    "$xwiki.getURL('TVPris.JQuery.Toastmessage', 'jsx')".toString()
], (jQuery) => {
    "use strict";

    const PROGRESS_DELAY = 500;

    function Notifyer () {
    }

    const p = Notifyer.prototype;

    const toast = (type, msg) => {
        jQuery().toastmessage("showToast", {
            "text": msg,
            "position": "top-center",
            "type": type
        });
    };

    p.error = (msg) => toast("error", msg);
    p.warning = (msg) => toast("warning", msg);
    p.notice = (msg) => toast("notice", msg);
    p.success = (msg) => toast("success", msg);

    p.progress = (msg) => {
        const hide_ = () => clearTimeout(th);

        let hide = hide_;

        const showtoast = () => {
            const note = jQuery().toastmessage("showToast", {
                "text": msg,
                "position": "middle-center",
                "type": "progress",
                "sticky": true,
                "closeText": ""
            });

            const hide0 = () => jQuery().toastmessage("removeToast", note);

            hide = hide0;
        };

        const th = setTimeout(showtoast, PROGRESS_DELAY);

        return () => {
            hide();
        };
    };

    return new Notifyer();
});
