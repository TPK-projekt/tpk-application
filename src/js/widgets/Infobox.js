/* eslint-env browser, amd, es6 */
define([
    "jquery",
    "Notifyer"
], (jQuery, notifyer) => {
    "use strict";

    const url = "/rest/tpk-infobox";
    const isDefined = (x) => typeof x !== "undefined";

    const SCROLL_TRESHOLD = 200;

    function Infobox (code, jqElement) {
        this.code = code;
        this.content = null;
        jqElement.each((_index, element) => {
            let enabled = false;
            let active = false;
            jQuery(element).click((event) => {
                if (!enabled) {
                    jQuery(element).on("shown.bs.popover", () => {
                        active = true;
                    });
                    jQuery(element).on("hidden.bs.popover", () => {
                        active = false;
                    });
                    this.enable(element);
                    enabled = true;
                }
                if (active) {
                    element.blur();
                }
                event.preventDefault();
                event.stopPropagation();
            });
        });
    }

    Infobox.prototype.getContent = function getContent () {
        const d = jQuery.Deferred();
        if (this.content !== null) {
            d.resolve(this.content);
        } else {
            jQuery.ajax({
                "url": url,
                "dataType": "json",
                "data": {"infoboxes[]": [this.code]}
            }).done((data) => {
                if (data.infoboxes.length > 0) {
                    const [cont] = data.infoboxes;
                    this.content = cont;
                    d.resolve(cont);
                } else if (isDefined(data.errors)) {
                    d.reject("$services.localization.render('infobox-failed-to-fetch-infobox'): " + data.errors);
                } else {
                    d.reject("$services.localization.render('infobox-failed-to-fetch-infobox')");
                }
            }).fail(() => d.reject("$services.localization.render('infobox-failed-to-fetch-infobox')"));
        }
        return d.promise();
    };

    Infobox.prototype.enable = function enable (element) {
        this.getContent().fail((err) => notifyer.error(err)).
            done((content) => {
                const jqe = jQuery(element);
                jqe.popover({
                    "title": "<button type=\"button\" class=\"close\" data-dismiss=\"popover\" aria-label=\"Stäng\"><span aria-hidden=\"true\">x</span></button> " + content.title,
                    "html": true,
                    "content": content.text,
                    "placement": () => {
                        const {top} = element.getBoundingClientRect();
                        if (top < SCROLL_TRESHOLD) {
                            return "auto bottom";
                        }
                        return "auto top";
                    }
                });
                jqe.popover("show");
                const popoverid = jqe.attr("aria-describedby");
                if (isJust(popoverid)) {
                    jQuery("#" + popoverid).find("button").click(() => {
                        jqe.popover("hide");
                        return false;
                    });
                }
            });
    };

    const infoBox = (jq, code) => {
        if (typeof code === "string") {
            const _ib = new Infobox(jq, code);
        } else {
            const m = new Map();
            jq.each((_index, element) => {
                const c = element.getAttribute("data-infobox");
                if (isDefined(c) && c !== null && c !== "") {
                    let codes = m.get(c);
                    if (!isDefined(codes)) {
                        codes = [];
                        m.set(c, codes);
                    }
                    codes.push(element);
                }
            });
            m.forEach((t, c) => new Infobox(c, jQuery(t)));
        }
    };

    return infoBox;
});
