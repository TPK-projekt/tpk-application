require(['jquery', "$xwiki.getURL('TVPris.Javascript.Expandable', 'jsx')", 'EventBus'], function(jQuery, Expandable, EventBus) {
    EventBus.triggerInit('application-init-complete');
    var eb = jQuery(EventBus);
    jQuery('.package-type-expandable').each(function(index, element) {
	new Expandable({
	    toggleElement: jQuery(element).find('.expandable-toggle'),
	    bodyElement: jQuery(element).find('.expandable-body'),
	    startOpened: false,
	    eventBus: eb,
	    closeEvent: 'package-accordion'
	});
    });
    jQuery('.measure-type-expandable').each(function(index, element) {
	new Expandable({
	    toggleElement: jQuery(element).find('.expandable-toggle'),
	    bodyElement: jQuery(element).find('.expandable-body'),
	    startOpened: false,
	    eventBus: eb,
	    closeEvent: 'measure-accordion'
	});
    });

});
