{{groovy}}
import org.xwiki.velocity.tools.JSONTool
import org.xwiki.model.reference.DocumentReference
import org.xwiki.model.reference.SpaceReference
import org.apache.commons.lang.exception.ExceptionUtils

def plainMode = request.get('outputSyntax') == 'plain' && request.get('xpage') == 'plain'
def debug = false

if (plainMode) {
  response.setContentType("application/json")
}

final Map retVal = new HashMap<String, Object>()

int getIntParam(String name) {
    String s = request.getParameter(name)
    if (s == null) {
        throw new IllegalArgumentException("Missing parameter: '" + name + "'")
    }
    try {
      return Integer.parseInt(s)
    } catch (Exception e) {
      throw new IllegalArgumentException("Not integer value of parameter: '" + name + "', value was '" + s + "'")
    }
}

void copyObjectProperties(srcObj, dstObj) {
  for (prop in srcObj.getxWikiClass().getPropertyNames()) {
    def value = srcObj.getProperty(prop) != null ? srcObj.getProperty(prop).getValue() : null
    dstObj.set(prop, value)
  }
}

try {

  if (services.csrf.getToken() != request.get('form_token')) {
    retVal.put("error", "Formulärskydd felaktig!  Ladda om sidan och försök publicera igen!")
    response.setStatus(response.SC_FORBIDDEN)
  }

  final int sid = getIntParam("sid");
  final boolean unpublish = "true".equalsIgnoreCase(request.get("unpublish"))

  final SpaceReference surgeryViewRootSpaceRef = services.model.resolveSpace(services.tpkCompare.SURGERY_VIEW_SPACE);
  final SpaceReference surgeryEditRootSpaceRef = services.model.resolveSpace(services.tpkCompare.SURGERY_EDIT_SPACE);

  final SpaceReference surgeryViewSpaceRef = new SpaceReference("" + sid, surgeryViewRootSpaceRef);
  final SpaceReference surgeryEditSpaceRef = new SpaceReference("" + sid, surgeryEditRootSpaceRef);

  final DocumentReference viewRef = new DocumentReference("WebHome", surgeryViewSpaceRef);
  final DocumentReference editRef = new DocumentReference("WebHome", surgeryEditSpaceRef);

  final def viewDoc = xwiki.getDocument(viewRef)
  final def editDoc = xwiki.getDocument(editRef)

  if (viewDoc.isNew()) {
    throw new RuntimeException("Document " + viewRef + " does not exist!");
  }
  if (editDoc.isNew()) {
    throw new RuntimeException("Document " + editRef + " does not exist!");
  }
  
  final def viewObj = viewDoc.getObject("TVPris.Classes.SurgeryEditableClass");
  final def editObj = editDoc.getObject("TVPris.Classes.SurgeryPreviewClass");

  if (viewObj == null) {
    throw new RuntimeException("Document " + viewRef + " has no object of class TVPris.Classes.SurgeryEditableClass");
  }
  if (editObj == null) {
    throw new RuntimeException("Document " + editRef + " has no object of class TVPris.Classes.SurgeryPreviewClass");
  }

  if (unpublish) {
    copyObjectProperties(viewObj, editObj);
    editDoc.save("Discarded changes");
  } else {
    copyObjectProperties(editObj, viewObj);
    viewDoc.saveAsAuthor("Published changes");
  }
  
} catch (Exception e) {
    retVal.put('error', e.getMessage());
    retVal.put('exception', services.localization.render('tvpris.surgerycomment_caught_exception', [e.getMessage() + ExceptionUtils.getFullStackTrace(e)]))
    response.setStatus(response.SC_INTERNAL_SERVER_ERROR)
}


println('{{{')
println(new JSONTool().serialize(retVal))
println('}}}')
{{/groovy}}
