{{include reference="TVPris.Code.RunQuery"/}}{{groovy}}
import org.xwiki.velocity.tools.JSONTool

def plainMode = request.get('outputSyntax') == 'plain' && request.get('xpage') == 'plain'
def debug = false

if (plainMode) {
  response.setContentType("application/json")
}

Map result = new HashMap();

double getDoubleParam(String name, Map result)
{
  try {
    return Double.parseDouble(request.get(name))
  } catch (Exception e) {
    if (request.get('lat') == null) {
      result.put('error', "Missing parameter '" + name + "'")
    } else {
      result.put('error', "Invalid value for parameter '" + name + "'")
    }
    response.setStatus(response.SC_INTERNAL_SERVER_ERROR)
    throw e
  }
}

try {
  Double lon = getDoubleParam('lon', result)
  Double lat = getDoubleParam('lat', result)
  int srid = 3857;
  if (request.get('srid')) {
    srid = Integer.parseInt(request.get('srid'))
  }
  
  String region_level = request.get('region_level')

  if (debug) {
    println("lon: " + lon.toString() + ", lat: " + lat.toString())
  }
  
  String queryString = "SELECT borders.name, types.name FROM gis.region_borders AS borders JOIN gis.region_types as types ON type = types.id WHERE gis.ST_Contains(border, gis.ST_Transform(gis.ST_GeometryFromText(CONCAT('POINT(', ?, ' ', ?, ')'), ?), 3857))"

  def qr = new QueryRunner('native', xwiki, xcontext)
  qr.addParameterSetter(lon.toString())
  qr.addParameterSetter(lat.toString())
  qr.addParameterSetter(srid);

  if (region_level != null) {
    queryString += ' AND types.name = ?'
    qr.addParameterSetter(region_level.toUpperCase())
  }
  
  if (debug) {
    println("query: '" + queryString + "'")
  }

  def r = qr.runQuery(queryString, null, null)

  List regions = new ArrayList();

  for (row in r) {
    regions.add([
		  'name': row[0],
		  'type': row[1]
		])
  }
  result.put('regions', regions)
} catch (Exception e) {
  if (result.get('error') == null) {
    result.put('error', "Uncaught exception: " + e)
    response.setStatus(response.SC_INTERNAL_SERVER_ERROR)
  }
}

println('{{{')
println(new JSONTool().serialize(result))
println('}}}')

{{/groovy}}
