/* eslint-env browser, amd, es6 */
define(["EventBus"], (EventBus) => {
    "use strict";

    const enable = (e) => e.parentElement.classList.remove("disabled");
    const disable = (e) => e.parentElement.classList.add("disabled");
    const deactivate = (e) => e.parentElement.classList.remove("active");
    const activate = (e) => e.parentElement.classList.add("active");

    const stopEvent = (e, f, el) => {
        e.stopPropagation();
        e.preventDefault();
        return f(e, el);
    };

    const setAnchorPage = (a, page) => {
        a.forEach((a0) => {
            if (a0.parentElement.classList.contains("link")) {
                a0.innerHTML = page;
            }
            a0.setAttribute("data-page", page);
            enable(a0);
        });
    };

    function Pagination (id, pageSize) {
        this.pageSize = pageSize;
        const e = document.getElementById(id);

        const anchors = (className, tagName = "a") => {
            const c = e.getElementsByClassName(className);
            const as = [];
            for (let i = 0; i < c.length; i += 1) {
                as.push(Array.prototype.slice.
                    call(c[i].getElementsByTagName(tagName)));
            }
            return as;
        };

        this.element = e;
        this.count = null;
        this.srOnlys = [];
        this.current = 0;
        this.numPages = 0;
        this.triggering = false;
        this.trigger = true;
        this.triggerp = true;
        this.pending = true;
        this.pendingSetPage = null;

        if (isNothing(e)) {
            return;
        }

        this.prev = anchors("prev");
        this.next = anchors("next");
        this.links = anchors("link");
        this.first = anchors("first");
        this.last = anchors("last");
        this.moreb = anchors("more-before", "span");
        this.morea = anchors("more-after", "span");

        const click = (el, f) => {
            for (let i = 0; i < el.length; i += 1) {
                el[i].forEach((e0) => e0.addEventListener("click", (ev) => stopEvent(ev, f, e0)));
            }
        };

        click(this.prev, () => {
            if (this.current > 0) {
                this.setPage(this.current - 1);
            }
        });

        click(this.next, () => {
            if (this.current < this.numPages) {
                this.setPage(this.current + 1);
            }
        });

        click(this.links.concat(this.first).concat(this.last), (event, el) => {
            const n = Number(el.getAttribute("data-page"));
            if (Number.isInteger(n)) {
                this.setPage(n);
            }
        });

        const onCountUpdated = (_, count) => {
            this.count = count;
            if (count !== null) {
                const div = Math.floor(count / this.pageSize);
                const mod = count % this.pageSize !== 0 ? 1 : 0;
                this.numPages = div + mod;
            } else {
                this.numPages = 0;
            }
            if (count === null || count <= pageSize) {
                disable(this.element);
                if (!this.pending) {
                    this.triggerSetPage(1);
                }
            } else if (!this.pending) {
                enable(this.element);
                this.triggerp = false;
                try {
                    this.setPage(1);
                } finally {
                    this.triggerp = true;
                }
            }
        };

        const onSetPage = (_, page) => {
            if (!this.triggering) {
                try {
                    this.trigger = false;
                    this.setPage(page);
                } finally {
                    this.trigger = true;
                }
            }
        };

        const reset = () => {
            this.current = null;
            this.setPending(1);
        };
        EventBus.on("count-updated", onCountUpdated);
        EventBus.on("set-page", onSetPage);
        EventBus.on("page-change", onSetPage);
        EventBus.on("view-updated", reset);
        EventBus.on("parameters-updated", reset);
        EventBus.on("block", () => {
            this.pending = true;
        });
        EventBus.on("unblock", () => {
            this.pending = false;
            if (this.pendingSetPage !== null) {
                this.setPage(this.pendingSetPage);
                this.pendingSetPage = null;
            }
        });

        reset();
    }

    const p = Pagination.prototype;

    p.clear = function clear () {
        for (let i = 0; i < this.links.length; i += 1) {
            this.links[i].forEach((l) => {
                deactivate(l);
                disable(l);
            });
        }

        for (let i = 0; i < this.srOnlys.length; i += 1) {
            const e = this.srOnlys[i];
            e.parentElement.removeChild(e);
        }
        this.srOnlys = [];
    };

    p.setCurrentAnchor = function setCurrentAnchor (idx) {
        this.links[idx].forEach((a) => {
            const srOnly = document.createElement("span");
            srOnly.className = "sr-only";
            srOnly.innnerHTML = " (nuvarande sida)";
            a.appendChild(srOnly);
            activate(a);
            this.srOnlys.push(srOnly);
        });
    };

    p.allocate = function allocate () {
        this.clear();
        if (this.numPages < 2) {
            return;
        }

        for (let i = 0; i < this.first.length; i += 1) {
            setAnchorPage(this.first[i], 1);
        }
        for (let i = 0; i < this.last.length; i += 1) {
            setAnchorPage(this.last[i], this.numPages);
        }

        let nb = 0;
        let na = 0;
        const n = this.links.length - 1;
        let c = 0;

        nb = this.current - 1;
        na = this.numPages - this.current;
        const ab = Math.floor(n / 2);
        const aa = Math.ceil(n / 2);
        const ub = Math.min(nb, ab);
        const ua = Math.min(na, aa);
        const rb = ab + Math.max(0, aa - ua);
        const ra = aa + Math.max(0, ab - ub);
        nb = Math.min(nb, rb);
        na = Math.min(na, ra);
        c = nb;
        setAnchorPage(this.links[c], this.current);

        this.setCurrentAnchor(c);

        const min = c - nb;
        const max = c + na;

        for (let i = 0; i < nb; i += 1) {
            setAnchorPage(this.links[c - i - 1], this.current - i - 1);
        }
        for (let i = 0; i < na; i += 1) {
            setAnchorPage(this.links[c + i + 1], this.current + i + 1);
        }
        for (let i = 0; i < min; i += 1) {
            this.links[i].forEach(disable);
        }
        for (let i = max + 1; i < this.links.length; i += 1) {
            this.links[i].forEach(disable);
        }

        if (this.current - nb > 1) {
            this.moreb.forEach((e) => e.forEach(enable));
        }
        if (this.current + na < this.numPages) {
            this.morea.forEach((e) => e.forEach(enable));
        }
    };

    p.triggerSetPage = function triggerSetPage (page) {
        if (this.trigger) {
            this.triggering = true;
            try {
                EventBus.trigger("set-page", [page]);
            } finally {
                this.triggering = false;
            }
        }
    };

    p.triggerPageChange = function triggerPageChange (page) {
        if (this.triggerp) {
            this.triggering = true;
            try {
                EventBus.trigger("page-change", [page]);
            } finally {
                this.triggering = false;
            }
        }
    };

    p.setPage = function setPage (page) {
        const wasReset = this.current === null;
        if (this.pending) {
            this.pendingSetPage = page;
            return;
        }
        if (page === this.current) {
            return;
        }
        this.current = page;
        this.moreb.forEach((e) => e.forEach(disable));
        this.morea.forEach((e) => e.forEach(disable));
        this.allocate();
        if (page === 1) {
            this.prev.forEach((e) => e.forEach(disable));
            this.first.forEach((e) => e.forEach(disable));
        } else {
            this.prev.forEach((e) => e.forEach(enable));
            this.first.forEach((e) => e.forEach(enable));
        }
        if (page === this.numPages) {
            this.next.forEach((e) => e.forEach(disable));
            this.last.forEach((e) => e.forEach(disable));
        } else {
            this.next.forEach((e) => e.forEach(enable));
            this.last.forEach((e) => e.forEach(enable));
        }
        this.triggerSetPage(page);
        if (!wasReset) {
            this.triggerPageChange(page);
        }
    };

    p.setPending = function setPending (page) {
        this.pending = true;
        this.setPage(page);
    };

    return (id, pageSize) => new Pagination(id, pageSize);

});
