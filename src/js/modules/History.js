/**
 *  Copyright (C) 2018  Andreas Jonsson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* eslint-env browser, amd, es6 */
define(["EventBus"], (EventBus) => {
    "use strict";

    const DEFAULT_ORDER = 50;

    const isDefined = (x) => typeof x !== "undefined";
    const isUndefined = (x) => !isDefined(x);
    const isNothing = (x) => isUndefined(x) || x === null;
    const isJust = (x) => !isNothing(x);
    const isFunc = (x) => typeof x === "function";
    const isEmpty = (x) => isNothing(x) || x === "";

    const nullFunc = () => null;


    const pl = /\+/g;
    const search = /([^&=]+)=?([^&]*)/g;
    const decode = (s) => decodeURIComponent(s.replace(pl, " "));

    const urlParams = () => {
        const query = document.location.search.substring(1);
        let match = search.exec(query);
        const up = new Map();
        while (match) {
            up.set(decode(match[1]), decode(match[2]));
            match = search.exec(query);
        }
        return up;
    };

    function History () {
        this.orders = [];
        this.objects = new Map();
        this.preserveParams = new Set();
        this.initated = false;

        this.pushEvents = [];
        this.replaceEvents = [];
        this.enabled = false;

        const disable = () => {
            this.enabled = false;
        };

        const enable = () => {
            this.enabled = true;
        };

        const parse = (up, id, obj) => {
            const d = obj.parse(up.get(id));
            if (isJust(d)) {
                obj.load(d);
            }
        };

        const loadStateFromObject = (state, up) => {
            for (let i = 0; i < this.orders.length; i += 1) {
                const n = this.orders[i];
                this.objects.get(n).forEach((obj, id) => {
                    if (isJust(state.get(id))) {
                        obj.load(state.get(id));
                    } else if (isJust(up.get(id)) && isFunc(obj.parse)) {
                        parse(up, id, obj);
                    }
                });
            }
        };

        const loadStateFromUrl = (up) => {
            for (let i = 0; i < this.orders.length; i += 1) {
                const n = this.orders[i];
                this.objects.get(n).forEach((obj, id) => {
                    if (isJust(up.get(id)) && isFunc(obj.parse)) {
                        parse(up, id, obj);
                    }
                });
            }
        };

        const loadState = (state) => {
            try {
                disable();

                const up = urlParams();

                if (isJust(state)) {
                    loadStateFromObject(state, up);
                } else {
                    loadStateFromUrl(up);
                }
            } finally {
                enable();
            }
            return false;
        };

        const saveState = (save) => {
            const state = new Map();
            const query = [];
            const up = urlParams();
            this.preserveParams.forEach((param) => {
                if (isJust(up.get(param))) {
                    query.push(encodeURIComponent(param) + "=" + encodeURIComponent(up.get(param)));
                }
            });
            for (let j = this.orders.length - 1; j >= 0; j -= 1) {
                this.objects.get(this.orders[j]).forEach((obj, id) => {
                    let v = null;
                    if (isFunc(obj.save)) {
                        v = obj.save();
                        if (isJust(v)) {
                            state.set(id, v);
                        }
                    }
                    if (isFunc(obj.serialize)) {
                        const vs = isJust(v)
                            ? obj.serialize(v)
                            : obj.serialize();
                        if (!isEmpty(vs)) {
                            query.push(encodeURIComponent(id) +
                                       "=" + encodeURIComponent(vs));
                        }
                    }
                });
            }
            const qs = query.length > 0 ? "?" + query.join("&") : "";

            const url = document.createElement("a");
            url.href = document.location.href;
            url.search = qs;

            let u = url.href;

            if (u[u.length - 1] === "#") {
                u = u.substring(0, u.length - 1);
            }

            save(state, window.title, u);
        };

        if (typeof window.history === "object" &&
            typeof window.history.pushState === "function") {
            window.history.scrollRestoration = "auto";
            window.onpopstate = (event) => loadState(event.state);
        } else {
            this.push = nullFunc;
            this.replace = nullFunc;
        }

        const addEvents = (array, callback, events) => {
            for (let i = 0; i < events.length; i += 1) {
                const e = events[i];
                if (array.indexOf(e) < 0) {
                    array.push(e);
                    EventBus.on(e, () => {
                        if (this.enabled) {
                            return callback();
                        }
                        return null;
                    });
                }
            }
        };

        const replace = () => saveState(window.history.
            replaceState.bind(window.history));
        const push = () => saveState(window.history.
            pushState.bind(window.history));

        this.replace = replace;
        this.push = push;

        /**
         * Add replace event names.
         *
         * The history manager will listen to these events on the event bus
         * and issue a call to window.history.replaceState.  The events will
         * be disabled during loadState.
         *
         * @param {array} events array The event list.
         * @returns {undefined}
         */
        this.addReplaceEvents =
            (...events) => addEvents(this.replaceEvents, replace, events);

        /**
         * Add push event names.
         *
         * The history manager will listen to these events on the event bus
         * and issue a call to window.history.pushState.  The events will
         * be disabled during loadState.
         *
         * @param {array} events The event list.
         * @returns {undefined}
         */
        this.addPushEvents =
            (...events) => addEvents(this.pushEvents, push, events);

        /**
         * Initiate history managed objects by loading from state URL.
         * @returns {undefined}
         */
        this.init = function init () {
            loadState(null);
            this.initated = true;
            this.enabled = true;
            EventBus.triggerInit("history-loaded");
        };
    }

    const p = History.prototype;

    let id = 0;
    const getId = () => {
        id += 1;
        return id;
    };

    /**
     * Register a history managed object.
     *
     * The object can be managed with either load/save callbacks or
     * serialize/parse callbacks or both.  For saving the state in
     * the URL, serialize/parse must be provided.  For saving the
     * state in the pushState state object, load/save must be provided.
     *
     * @param {object}   opts Object containing the option parameters.
     * @param {string}   opts.name An optional name for the parameter.
     * @param {function} opts.serialize  A serializer function.  When
     *                   combined with load/save the saved object to
     *                   serialize is passed as parameter.
     * @param {function} opts.parse Function for parsing query string value.
     *                   If the parsed object is returned when combined
     *                   with load/save it will be passed as
     *                   parameter to load.
     * @param {function} opts.save A function that must return the object
     *                   to save.  If null or undefined is returned, nothing
     *                   is saved.
     * @param {function} opts.load A callback function to obtain a stored
     *                   value on popstate.
     * @param {number}   opts.order Parameter to determine the order at which
     *                   to load the object.  Default is 50.
     * @returns {undefined}
     */
    p.register = function register (opts) {
        if (this.initiated) {
            throw new Error("Register after init!");
        }
        const obj = {
            "id": isDefined(opts.name) ? opts.name : getId(),
            "load": nullFunc,
            "save": nullFunc
        };
        if (isJust(opts.serialize) && isJust(opts.parse)) {
            obj.serialize = opts.serialize;
            obj.parse = opts.parse;
        } else {
            if (isJust(opts.serialize)) {
                throw Error("serialize requires parse!");
            }
            if (isJust(opts.parse)) {
                throw Error("parse requries serialize!");
            }
        }

        if (isJust(opts.load) && isJust(opts.save)) {
            obj.load = opts.load;
            obj.save = opts.save;
        } else {
            if (isJust(opts.load)) {
                throw Error("load requires save!");
            }
            if (isJust(opts.save)) {
                throw Error("save requires load!");
            }
        }
        const order = isJust(opts.order) ? opts.order : DEFAULT_ORDER;

        if (isUndefined(this.objects.get(order))) {
            this.orders.push(order);
            this.orders.sort((a, b) => a - b);
            this.objects.set(order, new Map());
        }
        this.objects.get(order).set(obj.id, obj);
    };

    p.preserveParameters = function preserveParameters (...parameters) {
        for (let i = 0; i < parameters.length; i += 1) {
            this.preserveParams.add(parameters[i]);
        }
    };

    const history = new History();

    return history;
});
