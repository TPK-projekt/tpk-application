{{include reference="TVPris.Code.Compare"/}}{{groovy}}
import org.xwiki.velocity.tools.JSONTool

def plainMode = request.get('outputSyntax') == 'plain' && request.get('xpage') == 'plain'

def debug = false

response.setHeader('Cache-Control', 'no-cache')
if (plainMode) {
  response.setContentType("application/json")
}

println('{{{')
println(new JSONTool().serialize(tpkCompareResult))
println('}}}')

{{/groovy}}
